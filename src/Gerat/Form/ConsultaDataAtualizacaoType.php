<?php
/*
 * This file is part of the Gerat Bundle.
 *
 * (c) Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 *
 */
namespace Gerat\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ConsultaDataAtualizacaoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dataAtualizacao = new \DateTime('today');

        $builder
            ->add('dataAtualizacao', 'date', array(
                'label'    => 'Data de atualização*',
                'required' => true,
                'widget'   => 'single_text',
                'format'   => 'dd/MM/yyyy',
                'attr'     => array(
                    'class' => 'form-control datepicker',
                    'value' => $dataAtualizacao->format('d/m/Y')
                    ),
                'constraints' => array(
                    new Assert\NotBlank(),
                    ),
                ))
            ->add('submit', 'submit', array(
                'attr'     => array(
                    'class'=> 'btn btn-default'
                    )
                ))
            ;
    }

    /**
     * @var DateTime
     * Data de processamento da rotina
     * @Assert\NotBlank()
     * @Assert\Type("\DateTime")
     */
    protected $dataAtualizacao;

    /**
     * FormularioConsultaDataAtualizacao::getDataAtualizacao()
     *
     * @param void
     *
     * @return FormularioConsultaDataAtualizacao
     */
    public function getDataAtualizacao()
    {
        return $this->dataAtualizacao;
    }

    /**
     * FormularioConsultaDataAtualizacao::setDataAtualizacao()
     *
     * @param DateTime
     *
     * @return void
     */
    public function setDataAtualizacao(\DateTime $dataAtualizacao)
    {
        $this->dataAtualizacao = $dataAtualizacao;
    }

    public function getName()
    {
        return "formDataAtualizacao";
    }
}
