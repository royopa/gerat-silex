<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Gerat\User;
use Gerat\UserManager;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Gerat\Form\ConsultaDataAtualizacaoType;
use Viter\Gerat\GeratBundle\Model\RotinaDiaria;
use Viter\Gerat\GeratBundle\Model\ListaLiquidezDiaria;


/**
 * Página Inicial - Raiz
 *
 */
$app->get('/', function (Request $request) use ($app) {

    //var_dump($request);

    //verifica usuário logado
    $token = $app['security']->getToken();

    if (null !== $token) {
        //$user = $token->getUser();
        $user = $token;
    }

    return $app['twig']->render('index.html.twig', array(
        'listaTopMais'   => 'listaTopMais',
        'graficoTopMais' => 'graficoTopMais',
        'dataReferencia' => new \DateTime('now'),
        'produtos'       => 'produtos',
        'user'           => $user,
    ));
})
->bind('homepage')
;

/**
 * Página de exibição de erros
 *
 */
$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html',
        'errors/'.substr($code, 0, 2).'x.html',
        'errors/'.substr($code, 0, 1).'xx.html',
        'errors/default.html',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

/**
 * Lista de fundos
 *
 */
$app->get('/fundos', function (Request $request) use ($app) {

    $connection = $app['db'];

    $sql = "
        SELECT
            P.CO_PRD,
            P.NO_PRD,
            C.NO_CUSTOD
        FROM
            PRODUTO P
        INNER JOIN
            CUSTODIANTE C
        ON
            P.CO_CUSTOD = C.CO_CUSTOD
        WHERE
            P.IC_DESAT = '0'
        ";

    $statement = $connection->prepare($sql);

    $statement->execute();

    $produtos = $statement->fetchAll();

    return $app['twig']->render('Fundos/lista_fundos.html.twig', array(
        'title'    => 'GERAT | Lista de fundos',
        'produtos' => $produtos,
    ));

})
->bind('fundos');
;

/**
 * Login
 *
 */
$app->get('/login', function () use ($app) {

    //$conn = $app['db']
    //pega a matrícula do usuário logado
    $matricula = $app['request']->server->get('REMOTE_USER');
    //formata para pegar somente a matrícula
    $matricula = 'CORPCAIXA\C090762';
    $matricula = strtolower(substr($matricula, -7));

    //se não conseguiu pegar o usuário logado, retorna uma exception
    if ('' === $matricula) {
        $page = '500.html';
        $code = '500';

        return new Response($app['twig']->render($page, array('code' => $code)), $code);
    }

    //pega o usuário no banco de dados para preencher a sessão
    $userManager = new UserManager($app['db'], $app);
    $user = $userManager->loadUserByUsername($matricula);

    //se não encontrou o usuário na base dados, redireciona para página não encontrada
    if (!$user) {
        $page = '404.html';
        $code = '404';

        return new Response($app['twig']->render($page, array('code' => $code)), $code);
    }

    $token = null;
    //seta o usuário na sessão
    $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
    $app['security']->setToken($token);

    $app['session']->getFlashBag()->add('success', 'You are logged!');
    $referer = $app['request']->headers->get('referer');

    return $app->redirect($referer != null ? $referer : $app['url_generator']->generate('homepage'));
})
->bind('login');
;

/**
 * Formulário com a data de atualização
 *
 */
$app->match('/consulta_xml_nao_processados', function (Request $request) use ($app) {

    $form = new ConsultaDataAtualizacaoType();

    $form = $app["form.factory"]->create(new ConsultaDataAtualizacaoType());

    $form->handleRequest($request);

    if ($form->isValid()) {

        $data = $form->getData();

        $rotinaDiaria = new RotinaDiaria($app['db']);
        $listaXmlNaoProcessados = $rotinaDiaria->getXmlNaoProcessados($data['dataAtualizacao']);
        $listaDiferencasXml     = $rotinaDiaria->getDiferencasXml($data['dataAtualizacao']);

        return $app['twig']->render('Fundos/lista_fundos_xml_nao_processados.html.twig', array(
            'title'           => 'XML não processados - Data de Atualização: ' . $data['dataAtualizacao']->format('d/m/Y'),
            'lista'           => $listaXmlNaoProcessados,
            'listaDiferencas' => $listaDiferencasXml,
            )
        );
    }

    // display the form
    return $app['twig']->render('Form/form_consulta_data_atualizacao.html.twig', array(
        'title'    => 'Consulta XML não processados',
        'subtitle' => 'Utilize o formulário abaixo para consultar os XMLs não processados.',
        'method'   => 'POST',
        'action'   => 'consulta_xml_nao_processados',
        'form'     => $form->createView(),
        ));
})
->bind('consulta_xml_nao_processados');
;

/**
 * Relatório de atribuição de performance
 *
 */
$app->get('/relatorio_atribuicao_performance', function (Request $request) use ($app) {

    $conn = $app['db'];

    return $app['twig']->render('Fundos/lista_fundos.html.twig', array(
        'title'    => 'GERAT | ',
        //'produtos' => $produtos,
    ));

})
->bind('relatorio_atribuicao_performance');
;

/**
 * Consulta de liquidez diária
 *
 */

$app->match('/consulta_liquidez', function (Request $request) use ($app) {

    $form = new ConsultaDataAtualizacaoType();

    $form = $app["form.factory"]->create(new ConsultaDataAtualizacaoType());

    $form->handleRequest($request);

    if ($form->isValid()) {

        return new Response('Fazer controller!');

        $data = $form->getData();

        $conn = $app['db'];
        $listaLiquidezDiaria = new ListaLiquidezDiaria($data['dataAtualizacao'], $conn);

        return $app['twig']->render('Fundos/lista_fundos_xml_nao_processados.html.twig', array(
            'title'           => 'Liquidez Diária: ' . $data['dataAtualizacao']->format('d/m/Y'),
            'listaLiquidezDiaria' => $listaLiquidezDiaria,
            )
        );
    }

    // display the form
    return $app['twig']->render('Form/form_consulta_data_atualizacao.html.twig', array(
        'action'   => 'consulta_liquidez',
        'title'    => 'Consulta Liquidez Diária dos Fundos',
        'subtitle' => 'Utilize o formulário abaixo para consultar a liquidez dos fundos/carteiras na data solicitada.',
        'form'     => $form->createView(),
        'method'   => 'POST',
        ));
})
->bind('consulta_liquidez');
;

/**
 * Conferência de liquidez - auditoria
 *
 */
$app->match('/conferencia_liquidez', function (Request $request) use ($app) {
    
    $form = new ConsultaDataAtualizacaoType();

    $form = $app["form.factory"]->create(new ConsultaDataAtualizacaoType());

    $form->handleRequest($request);

    if ($form->isValid()) {

        $data = $form->getData();

        $conn = $app['db'];
        $listaLiquidezDiaria = new ListaLiquidezDiaria($data['dataAtualizacao'], $conn);

        //faz o processamento de tempos em tempos
        $i = 0;
        
        foreach ($listaLiquidezDiaria as $liquidezDiaria) {
            # code...
            $url_base = 'http://10.4.45.109/gerat/index.php/RiscoLiquidez/detalheLiquidezDiaria?';
            $url = 'co_prd=';
            $url = $url . $liquidezDiaria->getCnpj();
            $url = $url . '&dt_ref=';
            $url = $url . $liquidezDiaria->getDataReferencia()->format('Y-m-d');
            $url = $url . '&dt_atu=';
            $url = $url . $liquidezDiaria->getDataAtualizacao()->format('Y-m-d');

            //$page = file_get_contents($url_base.urlencode($url));
            $curl_handle=curl_init();
            curl_setopt($curl_handle, CURLOPT_URL,$url_base.urlencode($url));
            curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Your application name');
            $query = curl_exec($curl_handle);
            curl_close($curl_handle);
            
            $i++;
        }
        
        
        return $app['twig']->render('Fundos/lista_fundos_liquidez.html.twig', array(
            'title'    => 'Conferência de liquidez - Auditoria - Data de Atualização: ' . $data['dataAtualizacao']->format('d/m/Y'),
            'subtitle' => 'Processo terminado, consulte a liquidez do dia solicitado.',
            'listaLiquidezDiaria' => $listaLiquidezDiaria,
            )
        );
    }

    // display the form
    return $app['twig']->render('Form/form_consulta_data_atualizacao.html.twig', array(
        'title'    => 'Conferência de liquidez - Auditoria',
        'subtitle' => 'Utilize o formulário abaixo para consultar a liquidez dos fundos/carteiras na data solicitada.',
        'method'   => 'POST',
        'action'   => 'conferencia_liquidez',
        'form'     => $form->createView(),
        ));
})
->bind('conferencia_liquidez');
;

/**
 * Consulta XML com diferenças
 *
 */
$app->match('/consulta_diferencas_xml', function (Request $request) use ($app) {
    return new Response('Fazer controller!');

    $form = new ConsultaDataAtualizacaoType();

    $form = $app["form.factory"]->create(new ConsultaDataAtualizacaoType());

    $form->handleRequest($request);

    if ($form->isValid()) {

        $data = $form->getData();

        $conn = $app['db'];
        $rotinaDiaria = new RotinaDiaria($app['db']);
        $listaXmlNaoProcessados = $rotinaDiaria->getXmlNaoProcessados($data['dataAtualizacao']);
        $listaDiferencasXml     = $rotinaDiaria->getDiferencasXml($data['dataAtualizacao']);

        return $app['twig']->render('Fundos/lista_fundos_xml_nao_processados.html.twig', array(
            'title'           => 'XML não processados - Data de Atualização: ' . $data['dataAtualizacao']->format('d/m/Y'),
            'lista'           => $listaXmlNaoProcessados,
            'listaDiferencas' => $listaDiferencasXml,
            )
        );
    }

    // display the form
    return $app['twig']->render('Form/form_consulta_data_atualizacao.html.twig', array(
        'title'    => 'Consulta XML não processados',
        'subtitle' => 'Utilize o formulário abaixo para consultar os XMLs não processados.',
        'method'   => 'POST',
        'action'   => 'consulta_xml_nao_processados',
        'form'     => $form->createView(),
        ));
})
->bind('consulta_diferencas_xml');
;

/**
 * Consultar valor de benchmark
 *
 */
$app->match('/consulta_benchmark', function (Request $request) use ($app) {
    return new Response('Fazer controller!');

    $form = new ConsultaDataAtualizacaoType();

    $form = $app["form.factory"]->create(new ConsultaDataAtualizacaoType());

    $form->handleRequest($request);

    if ($form->isValid()) {

        $data = $form->getData();

        $conn = $app['db'];
        $rotinaDiaria = new RotinaDiaria($app['db']);
        $listaXmlNaoProcessados = $rotinaDiaria->getXmlNaoProcessados($data['dataAtualizacao']);
        $listaDiferencasXml     = $rotinaDiaria->getDiferencasXml($data['dataAtualizacao']);

        return $app['twig']->render('Fundos/lista_fundos_xml_nao_processados.html.twig', array(
            'title'           => 'XML não processados - Data de Atualização: ' . $data['dataAtualizacao']->format('d/m/Y'),
            'lista'           => $listaXmlNaoProcessados,
            'listaDiferencas' => $listaDiferencasXml,
            )
        );
    }

    // display the form
    return $app['twig']->render('Form/form_consulta_data_atualizacao.html.twig', array(
        'title'    => 'Consulta XML não processados',
        'subtitle' => 'Utilize o formulário abaixo para consultar os XMLs não processados.',
        'method'   => 'POST',
        'action'   => 'consulta_xml_nao_processados',
        'form'     => $form->createView(),
        ));
})
->bind('consulta_benchmark');
;

/**
 * Relatório de atribuição de performance
 *
 */
$app->match('/simular_dpge', function (Request $request) use ($app) {
    return new Response('Fazer controller!');

    $form = new ConsultaDataAtualizacaoType();

    $form = $app["form.factory"]->create(new ConsultaDataAtualizacaoType());

    $form->handleRequest($request);

    if ($form->isValid()) {

        $data = $form->getData();

        $conn = $app['db'];
        $rotinaDiaria = new RotinaDiaria($app['db']);
        $listaXmlNaoProcessados = $rotinaDiaria->getXmlNaoProcessados($data['dataAtualizacao']);
        $listaDiferencasXml     = $rotinaDiaria->getDiferencasXml($data['dataAtualizacao']);

        return $app['twig']->render('Fundos/lista_fundos_xml_nao_processados.html.twig', array(
            'title'           => 'XML não processados - Data de Atualização: ' . $data['dataAtualizacao']->format('d/m/Y'),
            'lista'           => $listaXmlNaoProcessados,
            'listaDiferencas' => $listaDiferencasXml,
            )
        );
    }

    // display the form
    return $app['twig']->render('Form/form_consulta_data_atualizacao.html.twig', array(
        'title'    => 'Consulta XML não processados',
        'subtitle' => 'Utilize o formulário abaixo para consultar os XMLs não processados.',
        'method'   => 'POST',
        'action'   => 'consulta_xml_nao_processados',
        'form'     => $form->createView(),
        ));
})
->bind('simular_dpge');
;

/**
 * Relatório de atribuição de performance
 *
 */
$app->get('/consulta_monitoramento', function (Request $request) use ($app) {
    return new Response('Fazer controller!');
})
->bind('consulta_monitoramento');
;

$app->get('/consulta_pastas_xml', function (Request $request) use ($app) {
    return new Response('Fazer controller!');
})
->bind('consulta_pastas_xml');
;

$app->get('/renomear_xml_santander', function (Request $request) use ($app) {
    return new Response('Fazer controller!');
})
->bind('renomear_xml_santander');
;

$app->get('/processar_xml_itau', function (Request $request) use ($app) {
    return new Response('Fazer controller!');
})
->bind('processar_xml_itau');
;

$app->get('/lista_usuarios', function (Request $request) use ($app) {
    return new Response('Fazer controller!');
})
->bind('lista_usuarios');
;

$app->get('/lista_ativos_mercado', function (Request $request) use ($app) {
    return new Response('Fazer controller!');
})
->bind('lista_ativos_mercado');
;

$app->get('/lista_grupos_ativos', function (Request $request) use ($app) {
    return new Response('Fazer controller!');
})
->bind('lista_grupos_ativos');
;

$app->get('/consulta_ativos_por_emissor', function (Request $request) use ($app) {
    return new Response('Fazer controller!');
})
->bind('consulta_ativos_por_emissor');
;

$app->get('/consulta_rating', function (Request $request) use ($app) {
    return new Response('Fazer controller!');
    return $app['twig']->render('index.html.twig', array(
        'listaTopMais'   => 'listaTopMais',
        'graficoTopMais' => 'graficoTopMais',
        'dataReferencia' => new \DateTime('now'),
        'produtos'       => 'produtos',
        'user'           => $user,
    ));
})
->bind('consulta_rating');
;
