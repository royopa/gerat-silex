<?php
/**
* AtivoRiscoMercado File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * AtivoRiscoMercado Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

class AtivoRiscoMercado
{
    /**
     * @var string
     * O id do ativo
     */
    private $id;

    /**
     * @var string
     * O nome do ativo
     */
    private $nome;

    /**
     * @var string
     * O nome do ativo legível
     */
    private $nomeLegivel;

    /**
     * @var string
     * O id do tipo ativo
     */
    private $codigoTipo;

    /**
     * @var string
     * O tipo do ativo
     */
    private $tipo;

    /**
     * @var string
     * O grupo do ativo
     */
    private $grupo;

    public function __construct($conn = null)
    {
        $this->conn = $conn;
    }

    /**
     * AtivoRiscoMercado::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return AtivoRiscoMercado
     */
    public function create($row)
    {
        $this->setId($row['CO_ATIVO']);
        $this->setNome(utf8_encode($row['NO_ATIVO']));
        $this->setNomeLegivel(utf8_encode($row['NO_ATIVO_LEGIVEL']));
        $this->setCodigoTipo($row['CO_TP_ATI']);
        $this->setGrupo(utf8_encode($row['NO_GR_ATIVO']));
        $this->setTipo(utf8_encode($row['NO_TP_ATI']));

        return $this;
    }

    /**
     * AtivoRiscoMercado::get()
     *
     * @param int $id O id do ativo
     *
     * @return AtivoRiscoMercado
     *
     * Pega o elementos do banco de dados e popula o objeto
     */
    public function get($id)
    {
        $sql
            = "
            SELECT
                a.CO_ATIVO,
                a.NO_ATIVO,
                a.NO_GR_ATIVO,
                a.NO_ATIVO_LEGIVEL,
                t.CO_TP_ATI,
                AT.NO_TP_ATI
            FROM
                Produto_Dia_Expo_Ativo_x_Fator_Ativo a
            LEFT JOIN
                Produto_Dia_Expo_Ativo_x_Fator_Tipo_Ativo t
            ON a.CO_ATIVo = t.CO_ATIVO
            LEFT JOIN
                Ativo_Tipo AT
            ON AT.CO_TP_ATI=T.CO_TP_ATI
            WHERE
                a.CO_ATIVO = :id
            ";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindValue(
            'id',
            $id,
            'integer'
            );

        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $this->create($row);

            return $this;
        }

        return $this;
    }

    /**
     * Gets the O id do ativo.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the O id do ativo.
     *
     * @param string $id the id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets the O nome do ativo.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the O nome do ativo.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the O nome do ativo legível.
     *
     * @return string
     */
    public function getNomeLegivel()
    {
        return $this->nomeLegivel;
    }

    /**
     * Sets the O nome do ativo legível.
     *
     * @param string $nomeLegivel the nome legivel
     *
     * @return self
     */
    public function setNomeLegivel($nomeLegivel)
    {
        $this->nomeLegivel = $nomeLegivel;

        return $this;
    }

    /**
     * Gets the O id do tipo ativo.
     *
     * @return string
     */
    public function getCodigoTipo()
    {
        return $this->codigoTipo;
    }

    /**
     * Sets the O id do tipo ativo.
     *
     * @param string $codigoTipo the codigo tipo
     *
     * @return self
     */
    public function setCodigoTipo($codigoTipo)
    {
        $this->codigoTipo = $codigoTipo;

        return $this;
    }

    /**
     * Gets the O tipo do ativo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Sets the O tipo do ativo.
     *
     * @param string $tipo the tipo
     *
     * @return self
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Gets the O grupo do ativo.
     *
     * @return string
     */
    public function getGrupo()
    {
        return $this->grupo;
    }

    /**
     * Sets the O grupo do ativo.
     *
     * @param string $grupo the grupo
     *
     * @return self
     */
    public function setGrupo($grupo)
    {
        $this->grupo = $grupo;

        return $this;
    }
}
