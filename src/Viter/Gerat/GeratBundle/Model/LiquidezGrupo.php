<?php
/**
* LiquidezGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * LiquidezGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LiquidezGrupo extends \ArrayIterator
{
    /**
     * @var FundoDiario
     * O fundo diário do ativo
     */
    private $fundoDiario;

    /**
     * @var float
     * A soma total dos volumes líquidos
     */
    private $valorMercadoTotal;

    /**
     * @var float
     * A soma total dos volumes líquidos
     */
    private $volumeLiquidoTotal;

    /**
     * @var float
     * A soma total dos percentuais em relação ao PL dos valores alocados
     */
    private $percentualLiquidezTotal;

    /**
     * @var float
     * A soma total dos percentuais em relação ao PL dos valores alocados
     */
    private $resgateProjetado;

    /**
     * @var float
     * Valor em garantia
     */
    private $valorEmGarantia;

    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->conn = $conn;

        if ($fundoDiario instanceof FundoDiario) {
            $this->fundoDiario     = $fundoDiario;
            $this->valorEmGarantia = $fundoDiario->getValorEmGarantia();
            $this->fetchAll($fundoDiario);
            $this->preencheTotalizadores();
        }
    }
    /**
     * Gets the A soma total dos percentuais em relação ao PL dos valores alocados.
     *
     * @return float
     */
    public function getResgateProjetado()
    {
        return $this->resgateProjetado;
    }

    /**
     * Sets the A soma total dos percentuais em relação ao PL dos valores alocados.
     *
     * @param float $resgateProjetado the resgate projetado
     *
     * @return self
     */
    public function setResgateProjetado($resgateProjetado)
    {
        $this->resgateProjetado = $resgateProjetado;

        return $this;
    }

    /**
     * Gets the Valor em garantia.
     *
     * @return float
     */
    public function getValorEmGarantia()
    {
        return $this->valorEmGarantia;
    }

    /**
     * Sets the Valor em garantia.
     *
     * @param float $valorEmGarantia the valor garantia
     *
     * @return self
     */
    public function setValorEmGarantia($valorEmGarantia)
    {
        $this->valorEmGarantia = $valorEmGarantia;

        return $this;
    }

    /**
     * Gets the A soma total dos volumes líquidos.
     *
     * @return float
     */
    public function getValorMercadoTotal()
    {
        return $this->valorMercadoTotal;
    }

    /**
     * Sets the A soma total dos volumes líquidos.
     *
     * @param float $valorMercadoTotal the valor mercado total
     *
     * @return self
     */
    public function setValorMercadoTotal($valorMercadoTotal)
    {
        $this->valorMercadoTotal = $valorMercadoTotal;

        return $this;
    }

    /**
     * Gets the A soma total dos volumes líquidos.
     *
     * @return float
     */
    public function getVolumeLiquidoTotal()
    {
        return $this->volumeLiquidoTotal;
    }

    /**
     * Sets the A soma total dos volumes líquidos.
     *
     * @param float $volumeLiquidoTotal the volume liquido total
     *
     * @return self
     */
    public function setVolumeLiquidoTotal($volumeLiquidoTotal)
    {
        $this->volumeLiquidoTotal = $volumeLiquidoTotal;

        return $this;
    }

    /**
     * Gets the soma total dos percentuais em relação ao PL dos valores alocados.
     *
     * @return float
     */
    public function getPercentualLiquidezTotal()
    {
        return $this->percentualLiquidezTotal;
    }

    /**
     * Sets the soma total dos percentuais em relação ao PL dos valores alocados.
     *
     * @param float $percentualLiquidezTotal the percentual liquidez total
     *
     * @return self
     */
    public function setPercentualLiquidezTotal($percentualLiquidezTotal)
    {
        $this->percentualLiquidezTotal = $percentualLiquidezTotal * 100;

        return $this;
    }

    /**
     * Gets the O fundo diário do ativo.
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * Sets the O fundo diário do ativo.
     *
     * @param FundoDiario $fundoDiario the fundo diario
     *
     * @return self
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;

        return $this;
    }

    /**
     * LiquidezGrupo::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return LiquidezGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                NO_MTP_ATI,
                SUM(VR_VOLUME_1D) VR_VOLUME_1D,
                SUM(VR_MERC) VR_MERC
            FROM
                CR245002_RL_F_12
            WHERE
                IC_CENARIO = 'N' AND
                DT_ATU = :dataAtualizacao AND
                CO_PRD = :cnpj
            GROUP BY
                NO_MTP_ATI
              ";

        $stmt = $conn->prepare($sql);

        $stmt->bindValue(
            'cnpj',
            $fundoDiario->getFundo()->getCnpj(),
            'integer'
            );
        $stmt->bindValue(
            'dataAtualizacao',
            $fundoDiario->getDataAtualizacao(),
            'datetime'
            );
        $stmt->execute();

        while ($row = $stmt->fetch()) {

            $liquidez = new Liquidez();
            $this->append($liquidez->create($row));
        }

        return $this;
    }

    /**
     * LiquidezGrupo::preencheTotalizadores()
     *
     * @return LiquidezGrupo
     *
     * Preenche os totalizadores de valor e percentual total de alocação
     */
    public function preencheTotalizadores()
    {
        foreach ($this as $liquidez) {

            $this->valorMercadoTotal
                = $this->valorMercadoTotal + $liquidez->getValorMercado();

            $this->volumeLiquidoTotal
                = $this->volumeLiquidoTotal + $liquidez->getVolumeLiquido();

            $this->percentualLiquidezTotal
                = ($this->volumeLiquidoTotal / $this->valorMercadoTotal) * 100;
        }

        return $this;
    }
}
