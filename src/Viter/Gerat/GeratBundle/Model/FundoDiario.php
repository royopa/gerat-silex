<?php
/*
 * This file is part of the Gerat package.
 *
 * (c) Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

class FundoDiario
{
    /**
     * @var Fundo
     * O fundo ao qual as informações diárias pertencem
     */
    private $fundo;

    /**
     * @var DateTime
     * Data do arquivo XML processado na rotina
     */
    private $dataReferencia;

    /**
     * @var DateTime
     * Data de processamento da rotina
     */
    private $dataAtualizacao;

    /**
     * @var float
     * Patrimônio líquido do fundo
     */
    private $patrimonioLiquido;

    /**
     * @var float
     * Patrimônio líquido stress
     */
    private $patrimonioLiquidoStress;

    /**
     * @var float
     * Valor da cota
     */
    private $valorCota;

    /**
     * @var float
     * Valor que foi aplicado no fundo
     */
    private $valorAplicadoD0;

    /**
     * @var float
     * Valor que foi resgatado no fundo
     */
    private $valorResgatadoD0;

    /**
     * @var float
     * Resgate projetado
     */
    private $resgateProjetado;

    /**
     * @var TermometroLiquidez
     * O termometro de risco de liquidez - CR245
     */
    private $termometroLiquidez;

    /**
     * @var float
     * O valor do maior resgate ocorrido nos últimos 3 anos
     */
    private $maiorResgateUltimos3Anos;

    /**
     * @var float
     * A rentabilidade no dia
     */
    private $rentabilidadeDia;

    /**
     * @var float
     * A rentabilidade no mês
     */
    private $rentabilidadeMes;

    /**
     * @var float
     * A rentabilidade no ano
     */
    private $rentabilidadeAno;

    /**
     * @var float
     * Valor que está em garantia
     */
    private $valorEmGarantia;

    public function __construct(Fundo $fundo = null, \DateTime $dataAtualizacao = null, $conn)
    {
        $this->conn = $conn;

        if ($fundo instanceof Fundo) {
            $this->fundo           = $fundo;
            $this->dataAtualizacao = $dataAtualizacao;
            $this->fetchAll();
        }
    }

    /**
     * Sets the Patrimônio líquido stress.
     *
     * @param float $patrimonioLiquidoStress the patrimonio liquido stress
     *
     * @return self
     */
    public function setPatrimonioLiquidoStress($patrimonioLiquidoStress)
    {
        $this->patrimonioLiquidoStress = (float) $patrimonioLiquidoStress;

        return $this;
    }

    /**
     * Gets the Valor da cota.
     *
     * @return float
     */
    public function getValorCota()
    {
        return (float) $this->valorCota;
    }

    /**
     * Sets the Valor da cota.
     *
     * @param float $valorCota the valor cota
     *
     * @return self
     */
    public function setValorCota($valorCota)
    {
        $this->valorCota = (float) $valorCota;

        return $this;
    }

    /**
     * Gets the Valor que foi aplicado no fundo.
     *
     * @return float
     */
    public function getValorAplicadoD0()
    {
        return (float) $this->valorAplicadoD0;
    }

    /**
     * Sets the Valor que foi aplicado no fundo.
     *
     * @param float $valorAplicadoD0 the valor aplicado d0
     *
     * @return self
     */
    public function setValorAplicadoD0($valorAplicadoD0)
    {
        $this->valorAplicadoD0 = (float) $valorAplicadoD0;

        return $this;
    }

    /**
     * Gets the Valor que foi resgatado no fundo.
     *
     * @return float
     */
    public function getValorResgatadoD0()
    {
        return (float) $this->valorResgatadoD0;
    }

    /**
     * Sets the Valor que foi resgatado no fundo.
     *
     * @param float $valorResgatadoD0 the valor resgatado d0
     *
     * @return self
     */
    public function setValorResgatadoD0($valorResgatadoD0)
    {
        $this->valorResgatadoD0 = (float) $valorResgatadoD0;

        return $this;
    }

    /**
     * Gets the Valor que está em garantia.
     *
     * @return float
     */
    public function getValorEmGarantia()
    {
        return (float) $this->valorEmGarantia;
    }

    /**
     * Sets the Valor que está em garantia.
     *
     * @param float $valorEmGarantia the valor em garantia
     *
     * @return self
     */
    public function setValorEmGarantia($valorEmGarantia)
    {
        $this->valorEmGarantia = (float) $valorEmGarantia;

        return $this;
    }

    /**
     * FundoDiario::getFundo()
     *
     * @param void
     *
     * @return FundoDiario
     */
    public function getFundo()
    {
        return $this->fundo;
    }

    /**
     * FundoDiario::getDataReferencia()
     *
     * @param void
     *
     * @return FundoDiario
     */
    public function getDataReferencia()
    {
        return $this->dataReferencia;
    }

    /**
     * FundoDiario::getDataAtualizacao()
     *
     * @param void
     *
     * @return FundoDiario
     */
    public function getDataAtualizacao()
    {
        return $this->dataAtualizacao;
    }

    /**
     * Fundo::getPatrimonioLiquido()
     *
     * @param void
     *
     * @return float
     */
    public function getPatrimonioLiquido()
    {
        return (float) $this->patrimonioLiquido;
    }

    /**
     * Fundo::getPatrimonioLiquidoStress()
     *
     * @param void
     *
     * @return float
     */
    public function getPatrimonioLiquidoStress()
    {
        return (float) ($this->patrimonioLiquido - $this->maiorResgateUltimos3Anos);
    }

    /**
     * Fundo::getResgateProjetado()
     *
     * @param void
     *
     * @return float
     */
    public function getResgateProjetado()
    {
        return $this->resgateProjetado;
    }

    /**
     * Fundo::getMaiorResgateUltimos3Anos()
     *
     * @param void
     *
     * @return float
     */
    public function getMaiorResgateUltimos3Anos()
    {
        return (float) $this->maiorResgateUltimos3Anos;
    }

    /**
     * Fundo::setFundo()
     *
     * @param Fundo
     *
     * @return Fundo
     */
    public function setFundo(Fundo $fundo)
    {
        $this->fundo = $fundo;

        return $this;
    }

    /**
     * FundoDiario::setDataReferencia()
     *
     * @param DateTime
     *
     * @return FundoDiario
     */
    public function setDataReferencia(DateTime $dataReferencia)
    {
        $this->dataReferencia = new DateTime($dataReferencia);

        return $this;
    }

    /**
     * FundoDiario::setDataAtualizacao()
     *
     * @param DateTime
     *
     * @return FundoDiario
     */
    public function setDataAtualizacao(DateTime $dataAtualizacao)
    {
        $this->dataAtualizacao = new DateTime($dataAtualizacao);

        return $this;
    }

    /**
     * Fundo::setPatrimonioLiquido()
     *
     * @param float
     *
     * @return FundoDiario
     */
    public function setPatrimonioLiquido($patrimonioLiquido)
    {
        $this->patrimonioLiquido = (float) $patrimonioLiquido;

        return $this;
    }

    /**
     * Fundo::setResgateProjetado()
     *
     * @param float
     *
     * @return FundoDiario
     */
    public function setResgateProjetado($resgateProjetado)
    {
        $this->resgateProjetado = (float) $resgateProjetado;

        return $this;
    }

    /**
     * Fundo::setMaiorResgateUltimos3Anos()
     *
     * @param float
     *
     * @return FundoDiario
     */
    public function setMaiorResgateUltimos3Anos($maiorResgateUltimos3Anos)
    {
        $this->maiorResgateUltimos3Anos = (float) $maiorResgateUltimos3Anos;

        return $this;
    }

    /**
     * Fundo::processa()
     * Faz a busca no banco de dados e preenche o objeto
     *
     * @return FundoDiario
     */
    public function fetchAll()
    {
        $sql
            = "
                SELECT DISTINCT
                    pd.CO_PRD,
                    pd.DT_REF,
                    pd.DT_ATU,
                    pd.VR_PL,
                    pd.VR_COTA,
                    pd.VR_APL_D0,
                    pd.VR_RES_D0,
                    pd.VR_RESG_PROJ,
                    pd.VR_RESG_MAX_3A,
                    pd.VR_EM_GAR
                    ,fd.rentdia
                    ,fd.rentmes
                    ,fd.rentano
                FROM
                    [SIRAT].[dbo].[Produto_Dia] pd
                INNER JOIN
                    [SIRAT].[dbo].[Produto] p
                ON
                    p.CO_PRD = pd.CO_PRD
                INNER JOIN
                    [ANBID2].[dbo].[fundos] f
                ON
                    cast(f.cnpj as bigint) = p.CO_PRD
                INNER JOIN
                    [ANBID2].[dbo].[fundos_dia] fd
                ON
                    fd.codfundo = f.codfundo AND
                    fd.data = pd.DT_REF
                WHERE
                    pd.CO_PRD = :cnpj AND
                    pd.DT_ATU = :dataAtualizacao
            ";

            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(
                'cnpj',
                $this->getFundo()->getCnpj(),
                'integer'
                );
            $stmt->bindValue(
                'dataAtualizacao',
                $this->getDataAtualizacao(),
                'datetime'
                );
            $stmt->execute();

            while ($row = $stmt->fetch()) {
                $this->dataReferencia           = new \DateTime($row['DT_REF']);
                $this->dataAtualizacao          = new \DateTime($row['DT_ATU']);
                $this->patrimonioLiquido        = (float) ($row['VR_PL']);
                $this->valorCota                = (float) ($row['VR_COTA']);
                $this->valorAplicadoD0          = (float) ($row['VR_APL_D0']);
                $this->valorResgatadoD0         = (float) ($row['VR_RES_D0']);
                $this->resgateProjetado         = (float) ($row['VR_RESG_PROJ']);
                $this->maiorResgateUltimos3Anos = (float) ($row['VR_RESG_MAX_3A']);
                $this->valorEmGarantia          = (float) ($row['VR_EM_GAR']);
                $this->rentabilidadeDia         = (float) ($row['rentdia']);
                $this->rentabilidadeMes         = (float) ($row['rentmes']);
                $this->rentabilidadeAno         = (float) ($row['rentano']);
            }

        return $this;
    }

    /**
     * Gets the O termometro de risco de liquidez - CR245.
     *
     * @return TermometroLiquidez
     */
    public function getTermometroLiquidez()
    {
        return $this->termometroLiquidez;
    }

    /**
     * Sets the O termometro de risco de liquidez - CR245.
     *
     * @param TermometroLiquidez $termometroLiquidez the termometro liquidez
     *
     * @return self
     */
    public function setTermometroLiquidez(TermometroLiquidez $termometroLiquidez)
    {
        $this->termometroLiquidez = $termometroLiquidez;

        return $this;
    }

    /**
     * Gets the A rentabilidade no dia.
     *
     * @return float
     */
    public function getRentabilidadeDia()
    {
        return $this->rentabilidadeDia;
    }

    /**
     * Sets the A rentabilidade no dia.
     *
     * @param float $rentabilidadeDia the rentabilidade dia
     *
     * @return self
     */
    public function setRentabilidadeDia($rentabilidadeDia)
    {
        $this->rentabilidadeDia = $rentabilidadeDia;

        return $this;
    }

    /**
     * Gets the A rentabilidade no mês.
     *
     * @return float
     */
    public function getRentabilidadeMes()
    {
        return $this->rentabilidadeMes;
    }

    /**
     * Sets the A rentabilidade no mês.
     *
     * @param float $rentabilidadeMes the rentabilidade mes
     *
     * @return self
     */
    public function setRentabilidadeMes($rentabilidadeMes)
    {
        $this->rentabilidadeMes = $rentabilidadeMes;

        return $this;
    }

    /**
     * Gets the A rentabilidade no ano.
     *
     * @return float
     */
    public function getRentabilidadeAno()
    {
        return $this->rentabilidadeAno;
    }

    /**
     * Sets the A rentabilidade no ano.
     *
     * @param float $rentabilidadeAno the rentabilidade ano
     *
     * @return self
     */
    public function setRentabilidadeAno($rentabilidadeAno)
    {
        $this->rentabilidadeAno = $rentabilidadeAno;

        return $this;
    }
}
