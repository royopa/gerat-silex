<?php
/**
* Dbal File Doc Comment
*
* @category Library
* @package  Libraries
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;

/**
 * Dbal Class Doc Comment
 *
 * @category Library
 * @package  Libraries
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 * http://docs.doctrine-project.org/en/2.0.x/cookbook/integrating-with-codeigniter.* * html
 */
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

class Dbal
{
    public $connDbal  = null;
    private $conn      = null;
    private $connRisco = null;
    private $connRiscoLocalhost = null;

    public function __construct($connectionParams = null)
    {
        if (is_null($connectionParams)) {

            //dev
            $connectionParams = array(
                'dbname'   => 'model',
                'user'     => 'sa',
                'password' => 'sa',
                'host'     => '127.0.0.1',
                'driver'   => 'pdo_sqlsrv2000',
            );

            //prd
            $connectionParams = array(
                'dbname'   => 'SIRAT',
                'user'     => 'sirat',
                'password' => 'sirat',
                'host'     => '10.6.9.47',
                'driver'   => 'pdo_sqlsrv2000',
            );
        }

        //prd
        $connectionParamsRisco = array(
            'dbname'   => 'RISCO',
            'user'     => 'systemls',
            'password' => 'systemls',
            'host'     => '10.6.9.47',
            'driver'   => 'pdo_sqlsrv2000',
        );

        //127.0.0.1 test
        $connectionParamsRiscoLocalhost = array(
            'dbname'   => 'RISCO',
            'user'     => 'sa',
            'password' => 'sa',
            'host'     => '127.0.0.1',
            'driver'   => 'pdo_sqlsrv2000',
        );

        $rootDir = str_replace('src\\Viter\\Gerat\\GeratBundle\\Model', '', __DIR__);
        $loader = require $rootDir.'app\\autoload.php';

        $config = new Configuration();

        $this->connDbal = DriverManager::getConnection($connectionParams, $config);

        $this->conn = $this->connDbal;

        $this->connRisco = DriverManager::getConnection($connectionParamsRisco, $config);

        $this->connRiscoLocalhost = DriverManager::getConnection($connectionParamsRiscoLocalhost, $config);
    }

    /**
     * Gets the value of connDbal.
     *
     * @return mixed
     */
    public function getConnDbal()
    {
        return $this->connDbal;
    }

    /**
     * Sets the value of connDbal.
     *
     * @param mixed $connDbal the conn dbal
     *
     * @return self
     */
    public function setConnDbal($connDbal)
    {
        $this->connDbal = $connDbal;

        return $this;
    }

    /**
     * Gets the value of conn.
     *
     * @return mixed
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * Sets the value of conn.
     *
     * @param mixed $conn the conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }

    /**
     * Gets the value of connRisco.
     *
     * @return mixed
     */
    public function getConnRisco()
    {
        return $this->connRisco;
    }

    /**
     * Sets the value of connRisco.
     *
     * @param mixed $connRisco the conn risco
     *
     * @return self
     */
    public function setConnRisco($connRisco)
    {
        $this->connRisco = $connRisco;

        return $this;
    }

    /**
     * Gets the value of connRiscoLocalhost.
     *
     * @return mixed
     */
    public function getconnRiscoLocalhost()
    {
        return $this->connRiscoLocalhost;
    }

    /**
     * Sets the value of connRiscoLocalhost.
     *
     * @param mixed $connRiscoLocalhost the conn risco 127.0.0.1
     *
     * @return self
     */
    public function setconnRiscoLocalhost($connRiscoLocalhost)
    {
        $this->connRiscoLocalhost = $connRiscoLocalhost;

        return $this;
    }
}
