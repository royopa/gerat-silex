<?php
/**
* FatorRisco File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * FatorRisco Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class FatorRisco extends \ArrayIterator
{
    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->fundoDiario = $fundoDiario;
        $this->conn = $conn;
    }

    /**
     * FatorRisco::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return FatorRisco
     *
     * Pega o maior fator de risco para aquele grupo de ativos
     */
    public function getMaiorFator(FundoDiario $fundoDiario = null, $nomeGrupoAtivo)
    {
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $cnpj = $fundoDiario->getFundo()->getCnpj();
        $dataAtualizacao = $fundoDiario->getDataAtualizacao()->format('Y-m-d');

        $sql
            = "
                SELECT TOP 1
                    A.NO_ATIVO,
                    C.NO_FATOR,
                    O.NO_GR_ATIVO,
                    B.VR_MTM
                FROM
                    [CR245002_RM_F_16.A] O
                INNER JOIN
                    Produto_Dia_Expo_Ativo_x_Fator_Ativo A
                ON
                    O.CO_ATIVO=A.CO_ATIVO
                INNER JOIN
                    [Produto_Dia_Expo_Ativo_x_Fator] B
                ON
                    B.CO_ATIVO=A.CO_ATIVO AND
                    B.CO_PRD=O.CO_PRD AND
                    B.DT_ATU=O.DT_ATU
                INNER JOIN
                    [Produto_Dia_Expo_Ativo_x_Fator_Fator] C
                ON
                    C.CO_FATOR=B.CO_FATOR
                WHERE
                    O.CO_PRD = '$cnpj' AND
                    O.DT_ATU = '$dataAtualizacao' AND
                    O.NO_GR_ATIVO = '$nomeGrupoAtivo'
                ORDER BY
                    B.VR_MTM DESC
                ";

        $stmt = $conn->prepare($sql);

        /*
        $stmt->bindValue(
            'cnpj',
            $fundoDiario->getFundo()->getCnpj(),
            'integer'
            );

        $stmt->bindValue(
            'dataAtualizacao',
            $fundoDiario->getDataAtualizacao(),
            'datetime'
            );

        $stmt->bindValue(
            'nomeGrupoAtivo',
            $nomeGrupoAtivo,
            'string'
            );
        */

        $stmt->execute();

        while ($row = $stmt->fetch()) {
            return $row['NO_FATOR'];
        }

    return $this;
    }
}
