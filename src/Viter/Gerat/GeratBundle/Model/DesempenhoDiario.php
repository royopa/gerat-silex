<?php
/**
* DesempenhoDiario File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * DesempenhoDiario Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

class DesempenhoDiario
{
    /**
     * @var DateTime
     * Dia Mês e ano de rentabilidades
     */
    private $data;

    /**
     * @var float
     * A rentabilidade do fundo no dia
     */
    private $rentabilidadeFundo;

    /**
     * @var float
     * A rentabilidade do benchmark no dia
     */
    private $rentabilidadeBenchmark;

    /**
     * @var float
     * O percentual das rentabilidades do fundo sobre o benchmark
     */
    private $percentual;

    /**
     * Gets the Dia Mês e ano de rentabilidades.
     *
     * @return DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Sets the Mês e ano de rentabilidades.
     *
     * @param DateTime $data the data
     *
     * @return self
     */
    public function setData(\DateTime $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Gets the A rentabilidade do fundo no dia.
     *
     * @return float
     */
    public function getRentabilidadeFundo()
    {
        return $this->rentabilidadeFundo;
    }

    /**
     * Sets the A rentabilidade do fundo no dia.
     *
     * @param float $rentabilidadeFundo the rentabilidade fundo
     *
     * @return self
     */
    public function setRentabilidadeFundo($rentabilidadeFundo)
    {
        $this->rentabilidadeFundo = $rentabilidadeFundo;

        return $this;
    }

    /**
     * Gets the A rentabilidade do benchmark no dia.
     *
     * @return float
     */
    public function getRentabilidadeBenchmark()
    {
        return $this->rentabilidadeBenchmark;
    }

    /**
     * Sets the A rentabilidade do benchmark no dia.
     *
     * @param float $rentabilidadeBenchmark the rentabilidade benchmark
     *
     * @return self
     */
    public function setRentabilidadeBenchmark($rentabilidadeBenchmark)
    {
        $this->rentabilidadeBenchmark = $rentabilidadeBenchmark;

        return $this;
    }

    /**
     * Gets the O percentual das rentabilidades do fundo sobre o benchmark.
     *
     * @return float
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * Sets the O percentual das rentabilidades do fundo sobre o benchmark.
     *
     * @param float $percentual the percentual
     *
     * @return self
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;

        return $this;
    }

    /**
     * DesempenhoDiario::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return DesempenhoDiario
     */
    public function create($row, FundoDiario $fundoDiario = null)
    {
        if (count($row) > 0) {

            $this->setData(new \DateTime($row['DT_REF']));
            $this->setRentabilidadeFundo(
                (float) round($row['VR_RET_DIA'], 7)
                );

            $benchmark = $fundoDiario->getFundo()->getBenchmark();

            $retornoDia = $benchmark->getRetornoDia($this->getData());

            $this->setRentabilidadeBenchmark($retornoDia);

            $this->setPercentual(
                round(( $this->getRentabilidadeFundo() / $this->getRentabilidadeBenchmark()) * 100, 2)
                );

        }

        return $this;
    }
}
