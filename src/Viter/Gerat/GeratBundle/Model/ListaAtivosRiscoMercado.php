<?php
/**
* ListaAtivosRiscoMercado File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*
*/
namespace Viter\Gerat\GeratBundle\Model;

/**
 * ListaAtivosRiscoMercado Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ListaAtivosRiscoMercado extends \ArrayIterator
{
    /**
     * @var string
     * Nome do Ativo
     */
    private $nome;

    public function __construct(FundoDiario $fundoDiario = null,  $conn)
    {
        $this->conn = $conn;
        $this->fundoDiario = $fundoDiario;
        $this->fetchAll($fundoDiario);
    }

    /**
     * ListaAtivosRiscoMercado::fetchAll()
     *
     * @param FundoDiario $fundoDiario O objeto fundo diário
     *
     * @return ListaAtivosRiscoMercado
     *
     * Pega todos os ativos e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        $dataReferencia = $fundoDiario->getDataReferencia()->format('Y-m-d');
        $cnpj = $fundoDiario->getFundo()->getCnpj();

        $sql
            = "
            SELECT
                f.DT_REF,
                f.CO_PRD,
                fa.CO_ATIVO,
                fa.NO_ATIVO,
                fa.NO_ATIVO_LEGIVEL,
                fa.NO_GR_ATIVO,
                ff.CO_FATOR,
                ff.NO_FATOR,
                ff.NO_GR_FATOR,
                f.VR_MTM,
                f.VR_VAR,
                f.VR_CVAR,
                f.VR_INC_VAR,
                f.VR_VAR_S,
                f.VR_CVAR_S,
                f.VR_INC_VAR_S
            FROM
                Produto_Dia_Expo_Ativo_x_Fator f
            INNER JOIN
                Produto_Dia_Expo_Ativo_x_Fator_Ativo fa
            ON
                f.CO_ATIVO = fa.CO_ATIVO
            INNER JOIN
                Produto_Dia_Expo_Ativo_x_Fator_Fator ff
            ON
                f.CO_FATOR = ff.CO_FATOR
            WHERE
                (CO_PRD = '$cnpj' AND DT_REF = '$dataReferencia' AND VR_MTM <> 0) OR
                (CO_PRD = '$cnpj' AND DT_REF = '$dataReferencia' AND VR_VAR <> 0) OR
                (CO_PRD = '$cnpj' AND DT_REF = '$dataReferencia' AND VR_CVAR <> 0)
            ";

        $stmt = $this->conn->prepare($sql);

        $stmt->execute();

        $gruposAtivo = array();
        $fatoresRisco = array();
        $ativoFator = array();

        while ($row = $stmt->fetch()) {

            if ( ! in_array($row['NO_GR_ATIVO'], $gruposAtivo)) {
                $gruposAtivo[] = $row['NO_GR_ATIVO'];
            }

            if ( ! in_array($row['NO_FATOR'], $fatoresRisco)) {
                $fatoresRisco[] = $row['NO_FATOR'];
            }

            $ativoFator[$row['NO_ATIVO']] = $row['NO_FATOR'];

            $this->append($row);
        }

        return array($gruposAtivo, $fatoresRisco, $ativoFator);

        var_dump($gruposAtivo);
        var_dump($fatoresRisco);
        var_dump($ativoFator);

        return $this;
    }

    /**
     * Gets the Nome do Ativo.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the Nome do Ativo.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }
}
