<?php
/**
* DesempenhoDiarioGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * DesempenhoDiarioGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Highcharts\Highchart;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartJsExpr;

class DesempenhoDiarioGrupo extends \ArrayIterator
{
    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * @var DesempenhoMensal
     * O desempenho do fundo no mês anterior
     */
    private $desempenhoMesAnterior;

    /**
     * Gets the O gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the O gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }

    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->conn = $conn;

        if ($fundoDiario instanceof FundoDiario) {
            //pega todas as datas de referência do mês de referência
            $datas = $this->getDatasReferenciaMes($fundoDiario);
            $this->fetchAll($fundoDiario, $datas);
            $this->montaGrafico();

            $desempenhoMesAnterior = new DesempenhoMensal();
        }
    }

    /**
    * Monta um array com a série da composição da carteira de crédito que
    * será usada para montagem do gráfico de coluna da carteira de crédito
    *
    * @return  mixed[] $dados_serie Um array com os arrays que serão usados
    */
    private function montaSerieDados()
    {
        $dadosSerie        = array();
        $datas             = array();
        $retornosFundo     = array();
        $retornosBenchmark = array();

        foreach ($this as $desempenhoMensal) {

            $datas[] = $desempenhoMensal->getData()->format('d/m/Y');

            $retornosFundo[]
                = $desempenhoMensal->getRentabilidadeFundo();

            $retornosBenchmark[]
                = $desempenhoMensal->getRentabilidadeBenchmark();
        }

        $dadosSerie[0] = $datas;
        $dadosSerie[1] = $retornosFundo;
        $dadosSerie[2] = $retornosBenchmark;

        return $dadosSerie;
    }

    /**
     * DesempenhoMensalGrupo::montaGrafico()
     *
     * @return DesempenhoMensalGrupo
     *
     * Monta o gráfico de colunas dos retornos fundo/benchmark
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();

        $this->grafico =
            $this->getDesempenhoDiarioChart(
                    $this->montaSerieDados(),
                    'chart_desempenho_diario',
                    null,
                    'Rentabilidade do Fundo x Benchmark'
                );

        return $this;
    }

    /**
     * DesempenhoDiarioGrupo::getDatasReferenciaMes()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return DesempenhoDiarioGrupo
     *
     * Pega todas as datas de referência do mês de referência até a data
     * de referência
     */
    public function getDatasReferenciaMes(FundoDiario $fundoDiario = null)
    {
            $dataReferencia = $fundoDiario->getDataReferencia()->format('Y-m-d');

            $sql
                = "
                SELECT
                    DISTINCT(produto_dia.DT_REF)
                FROM
                    produto_dia
                INNER JOIN
                    produto_dia_2
                ON
                    produto_dia.co_prd = produto_dia_2.co_prd AND
                    produto_dia.dt_ref = produto_dia_2.dt_ref
                WHERE
                    produto_dia.co_prd = :cnpj AND
                    YEAR(produto_dia.DT_REF) = YEAR('$dataReferencia') AND
                    MONTH(produto_dia.DT_REF) = MONTH('$dataReferencia') AND
                    produto_dia.DT_REF <= '$dataReferencia'
                ";

            $stmt = $this->conn->prepare($sql);

            $stmt->bindValue(
                'cnpj',
                $fundoDiario->getFundo()->getCnpj(),
                'integer'
                );

            $stmt->execute();

            $datas = array();

            while ($row = $stmt->fetch()) {
              $datas[] = $row['DT_REF'];
        }

        return $datas;
    }

    public function fetchAll(FundoDiario $fundoDiario = null, $datas = null)
    {
        if ($fundoDiario instanceof FundoDiario) {

            $stmt = $this->conn->executeQuery(
                '
                SELECT
                    DT_REF,
                    VR_RET_DIA
                FROM
                    produto_dia_2
                WHERE
                    dt_ref
                IN
                    (?) AND
                CO_PRD = ?
                ORDER BY
                    DT_REF ASC
                ',
                array($datas, $fundoDiario->getFundo()->getCnpj()),
                array(\Doctrine\DBAL\Connection::PARAM_STR_ARRAY)
            );

            while ($row = $stmt->fetch()) {
                $desempenho = new DesempenhoDiario();
                $desempenho->create($row, $fundoDiario);
                $this->append($desempenho);
            }
        }

        return $this;
    }

    /**
    * Monta o gráfico Highchart
    *
    * @access  public
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getDesempenhoDiarioChart($serieDados = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();

        $chart->chart->renderTo = $render_div;
        $chart->chart->type     = "column";
        $chart->title->text     = $title;

        $chart->xAxis->categories = $serieDados[0];
        $chart->xAxis->labels->rotation = - 45;
        $chart->xAxis->labels->style->font = "normal 13px Verdana, sans-serif";

        $chart->yAxis->title->text = "Rentabilidade (%)";
        $chart->legend->layout = "vertical";
        $chart->legend->backgroundColor = "#FFFFFF";
        $chart->legend->align = "left";
        $chart->legend->verticalAlign = "top";
        $chart->legend->x = 70;
        $chart->legend->y = 30;
        $chart->legend->floating = 1;
        $chart->legend->shadow = 1;

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function () {
                return '' + this.series.name +' : ' + this.x + ' : ' + this.y + '%';
            }");

        $chart->plotOptions->column->pointPadding = 0.2;
        $chart->plotOptions->column->borderWidth = 0;

        $chart->series[] = array(
            'name' => "Fundo",
            'data' => $serieDados[1]
        );

        $chart->series[] = array(
            'name' => "Benchmark",
            'data' => $serieDados[2]
        );

        return $chart;
    }

    /**
     * Gets the O desempenho do fundo no mês anterior.
     *
     * @return DesempenhoMensal
     */
    public function getDesempenhoMesAnterior()
    {
        return $this->desempenhoMesAnterior;
    }

    /**
     * Sets the O desempenho do fundo no mês anterior.
     *
     * @param DesempenhoMensal $desempenhoMesAnterior the desempenho mes anterior
     *
     * @return self
     */
    public function setDesempenhoMesAnterior(DesempenhoMensal $desempenhoMesAnterior)
    {
        $this->desempenhoMesAnterior = $desempenhoMesAnterior;

        return $this;
    }
}
