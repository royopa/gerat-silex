<?php
/**
* LiquidezDiaria File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * LiquidezDiaria Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Fundo;
use Viter\Gerat\GeratBundle\Model\FundoDiario;

class LiquidezDiaria extends \ArrayIterator
{
    /**
     * @var DateTime
     * Data do arquivo XML processado na rotina
     */
    private $dataReferencia;

    /**
     * @var DateTime
     * Data de processamento da rotina
     */
    private $dataAtualizacao;

    /**
     * @var string
     * Nome do fundo
     */
    private $nome;

    /**
     * @var string
     * CNPJ do fundo
     */
    private $cnpj;

    /**
     * @var float
     * Valor liquidez no dia
     */
    private $valor;

    /**
     * @var float
     * Valor liquidez no dia
     */
    private $valorDiaAnterior;

    /**
     * @var float
     * Valor liquidez no dia - conferência
     */
    private $valorConferencia;

    /**
     * @var float
     * Nome do custodiante
     */
    private $custodiante;

    /**
     * Gets the Fundo Diário.
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * Sets the Fundo Diário.
     *
     * @param FundoDiario $fundoDiario the fundo diario
     *
     * @return self
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;

        return $this;
    }

    /**
     * Gets the Valor liquidez no dia.
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Sets the Valor liquidez no dia.
     *
     * @param float $valor the valor
     *
     * @return self
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * LiquidezDiaria::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return LiquidezDiaria
     */
    public function create($row, \DateTime $dataAtualizacao, $conn = null)
    {
        $this->dataReferencia   = new \DateTime($row['DT_REF']);
        $this->dataAtualizacao  = $dataAtualizacao;
        $this->nome             = $row['NO_PRD'];
        $this->cnpj             = $row['CO_PRD'];
        $this->valor            = (float) $row['LIQ'];
        $this->valorDiaAnterior = (float) $row['LIQD'];
        $this->custodiante      = $row['EMISSOR'];
        $this->valorConferencia = (float) $row['LIQ_CONF'];

        return $this;
    }

    /**
     * Gets the Data do arquivo XML processado na rotina.
     *
     * @return DateTime
     */
    public function getDataReferencia()
    {
        return $this->dataReferencia;
    }

    /**
     * Sets the Data do arquivo XML processado na rotina.
     *
     * @param DateTime $dataReferencia the data referencia
     *
     * @return self
     */
    public function setDataReferencia(DateTime $dataReferencia)
    {
        $this->dataReferencia = $dataReferencia;

        return $this;
    }

    /**
     * Gets the Data de processamento da rotina.
     *
     * @return DateTime
     */
    public function getDataAtualizacao()
    {
        return $this->dataAtualizacao;
    }

    /**
     * Sets the Data de processamento da rotina.
     *
     * @param DateTime $dataAtualizacao the data atualizacao
     *
     * @return self
     */
    public function setDataAtualizacao(DateTime $dataAtualizacao)
    {
        $this->dataAtualizacao = $dataAtualizacao;

        return $this;
    }

    /**
     * Gets the Nome do fundo.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the Nome do fundo.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the CNPJ do fundo.
     *
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Sets the CNPJ do fundo.
     *
     * @param string $cnpj the cnpj
     *
     * @return self
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Gets the Nome do custodiante.
     *
     * @return float
     */
    public function getCustodiante()
    {
        return $this->custodiante;
    }

    /**
     * Sets the Nome do custodiante.
     *
     * @param float $custodiante the custodiante
     *
     * @return self
     */
    public function setCustodiante($custodiante)
    {
        $this->custodiante = $custodiante;

        return $this;
    }

    /**
     * Gets the Valor liquidez no dia - conferência.
     *
     * @return float
     */
    public function getValorConferencia()
    {
        return  round($this->valorConferencia, 0);
    }

    /**
     * Sets the Valor liquidez no dia - conferência.
     *
     * @param float $valorConferencia the valor conferencia
     *
     * @return self
     */
    public function setValorConferencia($valorConferencia)
    {
        $this->valorConferencia = $valorConferencia;

        return $this;
    }

    /**
     * Gets the Valor liquidez no dia.
     *
     * @return float
     */
    public function getValorDiaAnterior()
    {
        return $this->valorDiaAnterior;
    }

    /**
     * Sets the Valor liquidez no dia.
     *
     * @param float $valorDiaAnterior the valor dia anterior
     *
     * @return self
     */
    public function setValorDiaAnterior($valorDiaAnterior)
    {
        $this->valorDiaAnterior = $valorDiaAnterior;

        return $this;
    }
}
