<?php
/**
* DesempenhoMensalGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * DesempenhoMensalGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Highcharts\Highchart;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartJsExpr;

class DesempenhoMensalGrupo extends \ArrayIterator
{
    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * Gets the O gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the O gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }

    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->conn = $conn;

        if ($fundoDiario instanceof FundoDiario) {
            $datas = $this->getUltimoDiaMes($fundoDiario);
            $this->fetchAll($fundoDiario, $datas);
            $this->montaGrafico();
        }
    }

    /**
    * Monta um array com a série da composição da carteira de crédito que
    * será usada para montagem do gráfico de coluna da carteira de crédito
    *
    * @return  mixed[] $dados_serie Um array com os arrays que serão usados
    */
    private function montaSerieDados()
    {
        $dadosSerie        = array();
        $datas             = array();
        $retornosFundo     = array();
        $retornosBenchmark = array();

        foreach ($this as $desempenhoMensal) {

            $datas[] = $desempenhoMensal->getData()->format('m/Y');

            $retornosFundo[]
                = $desempenhoMensal->getRentabilidadeFundo();

            $retornosBenchmark[]
                = $desempenhoMensal->getRentabilidadeBenchmark();
        }

        $dadosSerie[0] = $datas;
        $dadosSerie[1] = $retornosFundo;
        $dadosSerie[2] = $retornosBenchmark;

        return $dadosSerie;
    }

    /**
     * DesempenhoMensalGrupo::montaGrafico()
     *
     * @return DesempenhoMensalGrupo
     *
     * Monta o gráfico de colunas dos retornos fundo/benchmark
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();

        $this->grafico =
            $this->getDesempenhoMensalChart(
                    $this->montaSerieDados(),
                    'chart_desempenho_mensal',
                    null,
                    'Rentabilidade Mensal do Fundo x Benchmark'
                );

        return $this;
    }

    /**
     * DesempenhoMensalGrupo::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return DesempenhoMensalGrupo
     *
     * Pega todos os últimos dias dos últimos meses do ano corrente
     */
    public function getUltimoDiaMes(FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {

            $dataAtualizacao = $fundoDiario->getDataAtualizacao()->format('Y-m-d');

            $sql
                = "
                SELECT
                    MAX(produto_dia.DT_REF) DT_REF
                FROM
                    produto_dia
                INNER JOIN
                    produto_dia_2
                ON
                    produto_dia.co_prd = produto_dia_2.co_prd AND
                    produto_dia.dt_ref = produto_dia_2.dt_ref AND
                    YEAR(produto_dia.DT_REF) = YEAR('$dataAtualizacao')
                WHERE
                    produto_dia.co_prd = :cnpj AND
                    produto_dia.dt_ref < :dataAtualizacao
                GROUP BY
                    MONTH(produto_dia.DT_REF)
                ORDER BY
                    produto_dia.DT_REF DESC
                ";

            $stmt = $this->conn->prepare($sql);

            $stmt->bindValue(
                'cnpj',
                $fundoDiario->getFundo()->getCnpj(),
                'integer'
                );

            $stmt->bindValue(
                'dataAtualizacao',
                $fundoDiario->getDataAtualizacao(),
                'datetime'
                );

            $stmt->execute();

            $datas = array();

            while ($row = $stmt->fetch()) {
                $datas[] = $row['DT_REF'];
            }

            return $datas;
        }
    }

    public function fetchAll(FundoDiario $fundoDiario = null, $datas = null)
    {
        if ($fundoDiario instanceof FundoDiario) {

            $stmt = $this->conn->executeQuery(
                '
                SELECT
                    DT_REF,
                    VR_RET_MES
                FROM
                    produto_dia_2
                WHERE
                    dt_ref
                IN
                    (?) AND
                CO_PRD = ?
                ',
                array($datas, $fundoDiario->getFundo()->getCnpj()),
                array(\Doctrine\DBAL\Connection::PARAM_STR_ARRAY)
            );

            while ($row = $stmt->fetch()) {
                $desempenho = new DesempenhoMensal();
                $desempenho->create($row, $fundoDiario);
                $this->append($desempenho);
            }
        }

        return $this;
    }

    /**
    * Monta o gráfico Highchart
    *
    * @access  public
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getDesempenhoMensalChart($serieDados = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();

        $chart->chart->renderTo = $render_div;
        $chart->chart->type     = "column";
        $chart->title->text     = $title;

        $chart->xAxis->categories = $serieDados[0];
        $chart->xAxis->labels->rotation = - 45;
        $chart->xAxis->labels->style->font = "normal 13px Verdana, sans-serif";

        $chart->yAxis->title->text = "Rentabilidade (%)";
        $chart->legend->layout = "vertical";
        $chart->legend->backgroundColor = "#FFFFFF";
        $chart->legend->align = "left";
        $chart->legend->verticalAlign = "top";
        $chart->legend->x = 500;
        $chart->legend->y = 270;
        $chart->legend->floating = 1;
        $chart->legend->shadow = 1;

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function () {
                return '' + this.series.name +' : ' + this.x + ' : ' + Highcharts.numberFormat(this.y,2,',','.') + '%';
            }");

        $chart->plotOptions->column->pointPadding = 0.2;
        $chart->plotOptions->column->borderWidth = 0;

        $chart->series[] = array(
            'name' => "Fundo",
            'data' => $serieDados[1]
        );

        $chart->series[] = array(
            'name' => "Benchmark",
            'data' => $serieDados[2]
        );

        return $chart;
    }
}
