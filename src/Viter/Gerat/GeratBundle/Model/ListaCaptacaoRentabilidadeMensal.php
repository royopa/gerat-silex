<?php
/**
* ListaCaptacaoRentabilidadeMensal File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Highcharts\Highchart;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartJsExpr;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartOption;
use Viter\Gerat\GeratBundle\Model\FundoDiario;
use Viter\Gerat\GeratBundle\Model\DesempenhoMensalGrupo;
use Viter\Gerat\GeratBundle\Model\Dbal;

/**
 * ListaCaptacaoRentabilidadeMensal Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

class ListaCaptacaoRentabilidadeMensal extends \ArrayIterator
{
    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * Gets the O gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the O gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }

    public function __construct(FundoDiario $fundoDiario = null, DesempenhoMensalGrupo $desempenhoMensalGrupo)
    {
        $dbal = new Dbal();
        $this->conn = $dbal->getConn();
        $this->populaLista($fundoDiario, $desempenhoMensalGrupo);
        $this->montaGrafico();
    }

    /**
     * ListaCaptacaoRentabilidadeMensal::populaLista()
     *
     * @param FundoDiario      $fundoDiario O objeto fundo diário
     * @param DesempenhoMensal $desempenho  Os dados mensais de desempenho
     *
     * @return ListaCaptacaoRentabilidadeMensal
     *
     */
    public function populaLista($fundoDiario, $desempenhoMensalGrupo)
    {
        foreach ($desempenhoMensalGrupo as $desempenho) {
            $this->fetchCaptacaoMensal($fundoDiario, $desempenho);
        }
    }

    /**
     * ListaCaptacaoRentabilidadeMensal::fetchCaptacaoMensal()
     *
     * @param FundoDiario      $fundoDiario O objeto fundo diário
     * @param DesempenhoMensal $desempenho  Os dados mensais de desempenho
     *
     * @return CaptacaoRentabilidade
     *
     */
    public function fetchCaptacaoMensal(FundoDiario $fundoDiario, DesempenhoMensal $desempenho)
    {
        $mes = $desempenho->getData()->format('m');
        $ano = $desempenho->getData()->format('Y');

        $sql
            = "
            SELECT
                CO_PRD,
                SUM(VR_APL) VR_APL,
                SUM(VR_RESG) VR_RESG
            FROM
                Produto_Dia_2
            WHERE
                CO_PRD = :cnpj AND
                MONTH(DT_REF) = $mes AND YEAR(DT_REF) = $ano
            GROUP BY
                CO_PRD
            ";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindValue(
            'cnpj',
            $fundoDiario->getFundo()->getCnpj(),
            'integer'
            );

        $stmt->execute();

        while ($row = $stmt->fetch()) {

            $captacaoRentabilidade = new CaptacaoRentabilidade();

            $captacaoRentabilidade->setDataReferencia($desempenho->getData());
            $captacaoRentabilidade->setRentabilidade($desempenho->getRentabilidadeFundo());

            $captacaoRentabilidade->setValorAplicado((float) $row['VR_APL']);
            $captacaoRentabilidade->setValorResgatado((float) $row['VR_RESG']);
            $captacaoRentabilidade->setCaptacaoLiquida(
                $captacaoRentabilidade->getValorAplicado() - $captacaoRentabilidade->getValorResgatado()
            );

            $this->append($captacaoRentabilidade);
        }

        return $this;
    }

    /**
    * Monta um array com a série para montagem do gráfico
    *
    * @return  mixed[] $dados_serie Um array com a composiçao da carteira
    */
    private function montaSerieDados()
    {
        $dados_serie = array();
        $datasRef          = array();
        $captacoesLiquidas = array();
        $retornosDia       = array();
        $serieDados = array();

        foreach ($this as $captacaoRentabilidade) {

            $datasRef[]
                = $captacaoRentabilidade
                    ->getDataReferencia()
                    ->format('m/Y');

            $retornosDia[]
                = round($captacaoRentabilidade->getRentabilidade(), 3);

            $captacoesLiquidas[] = $captacaoRentabilidade->getCaptacaoLiquida();
        }

        $serieDados[0] = $datasRef;
        $serieDados[1] = $captacoesLiquidas;
        $serieDados[2] = $retornosDia;

        return $serieDados;
    }

    /**
     * ListaCaptacaoRentabilidadeMensal::montaGrafico()
     *
     * @return ListaComposicao
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function montaGrafico()
    {
        $this
            ->setGrafico(
                $this
                    ->getCaptacaoRentabilidadeChart(
                        $this->montaSerieDados(),
                        'chart_captacao_rentabilidade',
                        null,
                        'Captação x Rentabilidade'
                    )
            );

        return $this;
    }

    /**
    * Monta o gráfico Highchart
    *
    * @access  public
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getCaptacaoRentabilidadeChart($serieDados = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();

        $chart->chart->renderTo = $render_div;
        $chart->chart->zoomType = "xy";
        $chart->title->text     = $title;

        $chart->xAxis = array(
            array(
                'categories' => $serieDados[0]
            )
        );

        $chart->xAxis->categories = $serieDados[0];
        $chart->xAxis->labels->rotation = - 25;
        $chart->xAxis->labels->style->font = "normal 13px Verdana, sans-serif";

        $chart->legend->layout = "vertical";
        $chart->legend->backgroundColor = "#FFFFFF";
        $chart->legend->align = "left";
        $chart->legend->verticalAlign = "top";
        $chart->legend->x = 70;
        $chart->legend->y = 30;
        $chart->legend->floating = 1;
        $chart->legend->shadow = 1;

        //$chart->xAxis->labels->rotation = - 45;
        //$chart->xAxis->labels->style->font = "normal 13px Verdana, sans-serif";

        $leftYaxis = new HighchartOption();
        $leftYaxis->labels->formatter = new HighchartJsExpr(
            "function () {
                return this.value +'%';
            }"
            );

        $leftYaxis->labels->style->color = "#89A54E";
        $leftYaxis->title->text = "Rentabilidade";
        $leftYaxis->title->style->color = "#89A54E";

        $rightYaxis = new HighchartOption();
        $rightYaxis->title->text = "Captação Líquida";
        $rightYaxis->title->style->color = "#4572A7";

        $rightYaxis->labels->formatter = new HighchartJsExpr(
            "function () {
                return 'R$ ' + Highcharts.numberFormat(this.value,2,',','.') +'';
            }"
            );

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function () {
                return '' + this.series.name + ': ' + this.x +': '+
                (this.series.name == 'Captação Líquida' ?
                    ' R$ ' : '') +
                Highcharts.numberFormat(this.y,2,',','.') +
                (this.series.name == 'Captação Líquida' ? '' : ' %');
            }");

        $rightYaxis->labels->style->color = "#4572A7";
        $rightYaxis->opposite = 1;
        $chart->yAxis = array(
            $leftYaxis,
            $rightYaxis
        );

        /*
        * Legenda
        */
        $chart->legend->layout = "vertical";
        $chart->legend->align = "left";
        $chart->legend->x = 120;
        $chart->legend->verticalAlign = "top";
        $chart->legend->y = 1;
        $chart->legend->floating = 1;
        $chart->legend->backgroundColor = "#FFFFFF";

        $chart->series[] = array(
            'name' => "Captação Líquida",
            'color' => "#4572A7",
            'type' => "column",
            'yAxis' => 1,
            'data' => $serieDados[1]
        );

        $chart->series[] = array(
            'name' => "Rentabilidade",
            'color' => "#89A54E",
            'type' => "spline",
            'data' => $serieDados[2]
        );

        $chart->exporting->enabled = true;

        return $chart;
    }
}
