<?php
/**
* FidcGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* DPGE., FIDC., LF., Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/

/**
 * FidcGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

 namespace Viter\Gerat\GeratBundle\Model;

class FidcGrupo extends AtivoCreditoGrupo
{
    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->conn = $conn;
        $this->fundoDiario = $fundoDiario;
        $this->nome     = 'FIDC';
        parent::fetchAll($fundoDiario);
    }
}
