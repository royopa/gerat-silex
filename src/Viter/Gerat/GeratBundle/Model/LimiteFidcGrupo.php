<?php
/**
* LimiteFidcGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * LimiteFidcGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LimiteFidcGrupo extends LimiteAlocacaoGrupo
{
    public function __construct(FundoDiario $fundoDiario = null,  $conn)
    {
        $this->conn = $conn;
        $this->setNome('FIDC');
        $this->fundoDiario = $fundoDiario;
        $this->fetchAll($fundoDiario);
    }
}
