<?php
/**
* AtivoCreditoGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* DPGE., FIDC., LF., Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * AtivoCreditoGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class AtivoCreditoGrupo extends \ArrayIterator
{
    /**
     * @var string
     * Nome do tipo de ativo de crédito
     */
    private $nome;

    /**
     * Gets the Nome do tipo de ativo de crédito.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the Nome do tipo de ativo de crédito.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->conn = $conn;
        $this->fundoDiario = $fundoDiario;
        $this->fetchAll($fundoDiario);
    }

    /**
     * AtivoCreditoGrupo::fetchAll()
     *
    * @param FundoDia $fundoDiario O objeto fundo diário
     *
     * @return AtivoCreditoGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario)
    {
        $dt_atu = $fundoDiario->getDataAtualizacao()->format('Y-m-d');
        $co_prd = $fundoDiario->getFundo()->getCnpj();
        $tipo_ativo = $this->nome;

        $sqlWhere
            = " O.DT_ATU      = '$dt_atu' AND
                O.CO_PRD      = '$co_prd' AND
                O.NO_TP_ATI   = '$tipo_ativo'
            ";

        if ($tipo_ativo == 'DPGE' || $tipo_ativo == 'FIDC' ||
            $tipo_ativo == 'IF' OR $tipo_ativo == 'INF') {
            $sqlWhere
                = " O.DT_ATU      = '$dt_atu' AND
                    O.CO_PRD      = '$co_prd' AND
                    V.NO_TP_EMISS = '$tipo_ativo'
                ";
        }

        $sql
            = "
            SELECT
                V.DT_ATU,
                V.DT_REF,
                V.CO_PRD,
                V.CO_EMISS,
                V.NO_EMISS,
                V.NO_TP_EMISS,
                V.NO_RATING,
                SUM(V.VR_MERC) AS VR_MERC,
                V.PC_LIM,
                V.VR_ALOC,
                V.PC_ALOC,
                V.VR_LIM,
                V.PC_ALOC_S_LIM,
                V.CO_ALERTA,
                V.VR_LIM_D0,
                V.PC_ALOC_D0,
                V.PC_ALOC_S_LIM_D0,
                V.CO_ALERTA_D0,
                O.CO_ATI,
                O.NO_ATI,
                O.NO_EMISS,
                O.IC_RECOMPRA,
                O.DT_EMISS,
                O.DT_VCTO,
                O.PC_IDX_A,
                O.PC_IDX_P,
                O.PC_COUPOM,
                O.VR_MERC,
                O.PC_MERC,
                O.VR_DIAS_CDI
            FROM
                [CR245002_RC_F_14.A] O
            INNER JOIN
                ATIVO A
            ON
                O.CO_ATI = A.CO_ATI
            RIGHT JOIN
                V_CR245002_MONIT_RC_FUNDOS_EMISS V
            ON
                O.NO_EMISS = V.NO_EMISS AND
                O.DT_ATU   = V.DT_ATU AND
                O.CO_PRD   = V.CO_PRD
            WHERE
                $sqlWhere
            GROUP BY
                V.NO_EMISS
            ORDER BY
                O.NO_ATI
            ";

        $stmt = $this->conn->prepare($sql);

        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $ativoCredito = new AtivoCredito();
            $this->append($ativoCredito->create($fundoDiario, $row));
        }

        return $this;
    }
}
