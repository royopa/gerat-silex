<?php
/**
 * ListaDetalhadaCarteiraCredito File Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 *
 */
namespace Viter\Gerat\GeratBundle\Model;

/**
 * ListaDetalhadaCarteiraCredito Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ListaDetalhadaCarteiraCredito extends \ArrayIterator
{
    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->conn = $conn;
        $this->fetchAll($fundoDiario);
    }

    /**
     * ListaDetalhadaCarteiraCredito::fetchAll()
     *
     * @param FundoDiario $fundoDiario O objeto fundo diário
     *
     * @return ListaDetalhadaCarteiraCredito
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        $sql
            = "
            SELECT
                NO_TP_ATI,
                NO_EMISS,
                SUM(VR_MERC) AS VR_MERC
            FROM
                CR245002_RC_F_11
            WHERE
                CO_PRD = :cnpj AND
                DT_ATU = :dataAtualizacao
            GROUP BY
                NO_TP_ATI,
                NO_EMISS
              ";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindValue(
            'cnpj',
            $fundoDiario->getFundo()->getCnpj(),
            'integer'
            );

        $stmt->bindValue(
            'dataAtualizacao',
            $fundoDiario->getDataAtualizacao(),
            'datetime'
            );

        $stmt->execute();

        while ($row = $stmt->fetch()) {

            $row['NO_TP_ATI'] = utf8_encode($row['NO_TP_ATI']);
            $row['NO_ATI'] = utf8_encode($row['NO_ATI']);
            $row['NO_EMISS'] = utf8_encode($row['NO_EMISS']);
            $row['VR_MERC'] = (float) $row['VR_MERC'];
            $row['PC_MERC'] = ($row['VR_MERC'] / $fundoDiario->getPatrimonioLiquido()) * 100;

            $this->append($row);
        }

        return $this;
    }
}
