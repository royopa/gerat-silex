<?php
/**
* LimiteIfGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * LimiteIfGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LimiteIfGrupo extends LimiteAlocacaoGrupo
{
    public function __construct(FundoDiario $fundoDiario = null,  $conn)
    {
        $this->conn = $conn;
        $this->setNome('IF');
        $this->fundoDiario = $fundoDiario;
        $this->fetchAll($fundoDiario);
        $this->preencheTotalizadores();
        $this->setSerieDadosLimiteAlocacao(new SerieDadosLimiteAlocacao($this));
        $this->montaGrafico();
    }

    /**
     * LimiteAlocacaoGrupo::montaGrafico()
     *
     * @return LimiteAlocacaoGrupo
     *
     * Monta o gráfico para a série de dados
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();
        $this
            ->setGrafico(
                $grafico
                    ->getColumnChart(
                        $this->getSerieDadosLimiteAlocacao()->getSerieArray(),
                        'chart_limite_if',
                        null,
                        "Instituições Financeiras"
                    )
            );

        return $this;
    }
}
