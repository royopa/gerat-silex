<?php
/**
* DesempenhoMensalFundosGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Highcharts\Highchart;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartJsExpr;

/**
 * DesempenhoMensalFundosGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Highcharts\Highchart;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartJsExpr;

class DesempenhoMensalFundosGrupo extends \ArrayIterator
{
    public function __construct(\DateTime $data, $conn)
    {
        $this->conn = $conn;
        $this->data = $data;
    }

    public function getTopMais($quantidade = null)
    {
        $quantidade = (int) $quantidade;

        if (is_null($quantidade)) {
            $quantidade = 10;
        }
        //todos os fundos ativos e suas rentabilidades
        //entra o último dia do mes anterior
        $sql
            = "
              SELECT TOP $quantidade
                  p.[CO_PRD]
                  ,p.[NO_PRD]
                  ,[VR_CAPTACAO]
                  ,pd.[VR_RET_MES] AS VR_RENTABILIDADE
                  ,[VR_BENCHMARK]
                  ,pd.[NU_COTISTAS]
              FROM
                  [SIRAT].[dbo].[WEB_CAPTACAO_RENTABILIDADE_MENSAL] c
              INNER JOIN
                  [SIRAT].[dbo].[Produto] p
              ON
                  c.CO_PRD = P.CO_PRD
              INNER JOIN
                  [SIRAT].[dbo].[produto_dia_2] pd
              ON
                  pd.CO_PRD = P.CO_PRD AND
                  pd.DT_REF = c.DT_REF
              WHERE
                  c.DT_REF = :dataReferencia AND
                  p.[NO_PRD] NOT LIKE 'FIC%'
              ORDER BY
                  c.VR_CAPTACAO DESC
              ";

            $stmt = $this->conn->prepare($sql);

            $stmt->bindValue(
                'dataReferencia',
                $this->data,
                'datetime'
                );

            $stmt->execute();

            $saida = new \ArrayIterator();

            while ($row = $stmt->fetch()) {

                $row['CO_PRD'] = $row['CO_PRD'];
                $row['NO_PRD'] = utf8_encode($row['NO_PRD']);
                $row['VR_CAPTACAO'] = (float) $row['VR_CAPTACAO'];
                $row['VR_RENTABILIDADE'] = (float) $row['VR_RENTABILIDADE'];
                $row['VR_BENCHMARK'] = (float) $row['VR_BENCHMARK'];
                $row['NU_COTISTAS'] = (int) $row['NU_COTISTAS'];
                $saida->append($row);

            }

        //faz o select do total viter e adiciona no final do array
        //$totalViter = $this->getTotalViter();
        //$saida->append($totalViter);
        return $saida;
    }

    public function getTotalViter()
    {
        //todos os fundos ativos e suas rentabilidades
        //entra o último dia do mes anterior
        $sql
            = "
              SELECT
                  SUM(VR_CAPTACAO) AS VR_CAPTACAO
                  ,'TOTAL VITER' AS NO_PRD
                  ,0 AS CO_PRD
              FROM
                  [SIRAT].[dbo].[WEB_CAPTACAO_RENTABILIDADE_MENSAL] c
              WHERE
                  c.DT_REF = :dataReferencia
              ";

            $stmt = $this->conn->prepare($sql);

            $stmt->bindValue(
                'dataReferencia',
                $this->data,
                'datetime'
                );

            $stmt->execute();

            while ($row = $stmt->fetch()) {
                $row['NO_PRD']      = utf8_encode($row['NO_PRD']);
                $row['VR_CAPTACAO'] = (float) $row['VR_CAPTACAO'];
                $row['CO_PRD']      = (float) $row['CO_PRD'];

                return $row;
            }

        return;
    }

    //fundos que renderam menos no último mês
    public function getTopMenos($quantidade = null)
    {
        $quantidade = (int) $quantidade;

        if (is_null($quantidade)) {
            $quantidade = 10;
        }
        //todos os fundos ativos e suas rentabilidades
        //entra o último dia do mes anterior
        $sql
            = "
            SELECT TOP $quantidade
               fd.codfundo
              ,fd.data
              ,fd.pl
              ,fd.valcota
              ,fd.rentdia
              ,fd.rentmes
              ,fd.rentano
              ,fd.datahora
              ,f.codfundo
              ,f.codinst
              ,f.fantasia
              ,f.gestor
              ,f.codtipo
              ,f.dataini
              ,f.datafim
              ,f.datainfo
              ,f.perfil_cota
              ,f.datadiv
              ,f.razaosoc
              ,f.cnpj
              ,f.aberto
              ,f.exclusivo
              ,f.prazo_emis_cotas
              ,f.prazo_conv_resg
              ,f.prazo_pgto_resg
              ,f.carencia_universal
              ,f.carencia_ciclica
              ,f.cota_abertura
              ,f.periodo_divulg,
              p.NO_PRD
            FROM
                [ANBID2].[dbo].[fundos_dia] fd
            INNER JOIN
                [ANBID2].[dbo].[fundos] f
            ON
                fd.codfundo = f.codfundo
            INNER JOIN
                [SIRAT].[dbo].[Produto] p
            ON
                cast(f.cnpj as bigint) = cast(p.CO_PRD as bigint)
            WHERE
                fd.data = :data
            ORDER BY
                fd.rentmes ASC
            ";

            $stmt = $this->conn->prepare($sql);

            $stmt->bindValue(
                'data',
                $this->data,
                'datetime'
                );

            $stmt->execute();

            $saida = new \ArrayIterator();

            while ($row = $stmt->fetch()) {
                //$liquidezDiaria = new LiquidezDiaria();
                //$liquidezDiaria->create($row, $this->dataAtualizacao, $this->conn);

                $row['pl']      = (float) $row['pl'];
                $row['valcota'] = (float) $row['valcota'];
                $row['rentdia'] = (float) $row['rentdia'];
                $row['rentmes'] = (float) $row['rentmes'];
                $row['rentano'] = (float) $row['rentano'];
                $row['razaosoc'] = utf8_encode($row['razaosoc']);
                //$this->append($row);
                $saida->append($row);
            }

        return $saida;
        //return $this;
    }

    /**
    * Monta o gráfico Highchart
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function montaGrafico($lista = null, $render_div = null, $title = null, $mes = null, $ano = null)
    {
        $serieDados = array();
        $name = '';
        $lista = $lista;
        $render_div = $render_div;
        $title = $title;

        foreach ($lista as $row) {
            $serieDados['x'][] = $row['NO_PRD'];
            $serieDados['y'][] = $row['VR_CAPTACAO'];
            $serieDados['u'][] = $row['CO_PRD'];
        }

        $chart = new Highchart();

        $chart->chart->renderTo = $render_div;
        $chart->chart->type = "column";
        $chart->chart->margin = array(
            50,
            50,
            150,
            80
        );
        $chart->title->text = "Fundos com as maiores captações no mês";
        $chart->subtitle->text = "$mes/$ano";

        $chart->xAxis->categories = $serieDados['x'];

        $chart->xAxis->labels->rotation = - 20;
        $chart->xAxis->labels->align = "right";
        $chart->xAxis->labels->style->font = "normal 13px Verdana, sans-serif";
        $chart->yAxis->min = 0;
        $chart->yAxis->title->text = "Captação (R$)";
        $chart->legend->enabled = false;

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function () {
            return '<b>'+ this.x +'</b><br/>'+
            'Captação em $mes/$ano: R$ '+ Highcharts.numberFormat(this.y,2,',','.')
            ;}");

        //teste dos links para o gráfico
        $clickFunction = new HighchartJsExpr(
            "function () {
                hs.htmlExpand(null, {
                    pageOrigin: {
                        x: this.pageX,
                        y: this.pageY
                    },
                    headingText: this.series.name,
                    maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) +':<br/> '+
                        this.y +' visits',
                    width: 200
                });
            }"
        );
        $chart->plotOptions->series->cursor = 'pointer';
        $chart->plotOptions->series->point->events->click = $clickFunction;
        $chart->plotOptions->series->marker->lineWidth = 1;
        //teste dos links para o gráfico

        $chart->series[] = array(
            'name' => 'Population',
            //'URLs' => $serieDados,
            'data' => $serieDados['y'],
            'dataLabels' => array(
                'enabled' => true,
                'rotation' => -90,
                'color' => '#FFFFFF',
                'align' => 'right',
                'x' => - 3,
                'y' => 10,
                'formatter' => new HighchartJsExpr(
                    "function () {return '';}"
                    //"function () {return this.y;}"
                  ),
                'style' => array(
                    'font' => 'normal 13px Verdana, sans-serif'
                )
            )
            /*
            ,'series' => array(
                'points' => array(
                    'click' => new HighchartJsExpr(
                      "function () {alert 'link';}"
                      )
                    )
                )
            */
        );

        return $chart;
    }

}
