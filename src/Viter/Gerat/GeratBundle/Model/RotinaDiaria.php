<?php
/**
* RotinaDiaria File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*
*/
namespace Viter\Gerat\GeratBundle\Model;

/**
 * RotinaDiaria Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class RotinaDiaria
{
    /**
     * @var conn Conexão DBAL
     */
    private $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
    * Consulta os XMLs dos fundos não processados na rotina diária do XIMP
    *
    * Tabela PRODUTO_DIA
    *
    * @param DateTime $dataAtualizacao A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getXmlNaoProcessados(\DateTime $dataAtualizacao)
    {
        $data = $dataAtualizacao->format('Y-m-d');
        $sql = "
            SELECT
                P.NO_PRD,
                P.CO_PRD,
                DT_ATU,
                DT_REF,
                C.NO_CUSTOD
            FROM
                PRODUTO_DIA D
            INNER JOIN
                PRODUTO P
            ON
              D.CO_PRD = P.CO_PRD
            INNER JOIN
                CUSTODIANTE C
            ON
              P.CO_CUSTOD = C.CO_CUSTOD
            WHERE
                D.CO_PRD NOT IN ('14728096000113') AND
                DT_ATU = ? AND
                DT_REF <> (
                      SELECT TOP 1
                          DT_REF
                      FROM
                          PRODUTO_DIA
                      WHERE
                          DT_ATU = '$data'
                      ORDER BY 
                          DT_REF DESC)
        ";

        $statement = $this->conn->prepare($sql);
        $statement->bindValue(1, $dataAtualizacao, "datetime");
        $statement->execute();

        return $statement->fetchAll();
    }

    /**
    * Consulta as diferenças nos XMLs dos fundos
    *
    * Tabela PRODUTO_DIA
    *        PRODUTO_POSICAO
    *
    * @param string $dt_atu A data de atualização da base de dados
    *
    * @return  mixed[] $dados Um array com os dados do banco
    */
    public function getDiferencasXml(\DateTime $dataAtualizacao)
    {
        $data = $dataAtualizacao->format('Y-m-d');

        $sql
            = "
            SELECT
                PP.DT_REF DT_REF,
                P.CO_PRD CO_PRD,
                P.NO_PRD NO_PRD,
                SUM(PP.VR_MERC_A) ATIVO,
                SUM(PP.VR_MERC_P) PASSIVO,
                SUM(PP.VR_MERC_A) - SUM(PP.VR_MERC_P) TOTAL,
                PD.VR_ATI VALOR_XML,
                SUM(PP.VR_MERC_A) - SUM(PP.VR_MERC_P) - PD.VR_ATI DIFERENCA
            FROM
                PRODUTO_POSICAO PP
            JOIN
                PRODUTO_DIA PD
            ON
                PD.DT_REF = PP.DT_REF AND
                PD.CO_PRD = PP.CO_PRD
            JOIN
                PRODUTO P
            ON
                P.CO_PRD = PP.CO_PRD
            WHERE
                PD.DT_ATU = ? AND
                P.CO_PRD <> '00000000000007' AND
                P.CO_CUSTOD = '00360305000104' AND
                NOT (CO_TP_OP = 'T' AND DT_VCTO_ALUG IS NOT NULL) AND
                P.CO_CUSTOD = '00360305000104'
            GROUP BY
                PP.DT_REF,
                P.CO_PRD,
                P.NO_PRD,
                PD.VR_ATI
            HAVING
                ABS(SUM(PP.VR_MERC_A) - SUM(PP.VR_MERC_P) - PD.VR_ATI) > 1000
            AND
                ABS(SUM(PP.VR_MERC_A) - PD.VR_ATI) > 1000
            ORDER BY
                DIFERENCA";

        $statement = $this->conn->prepare($sql);
        $statement->bindValue(1, $dataAtualizacao, "datetime");
        $statement->execute();

        return $statement->fetchAll();
    }

    /**
     * Gets the value of conn.
     *
     * @return conn Conexão DBAL
     */
    public function getConn()
    {
        return $this->conn;
    }
    
    /**
     * Sets the value of conn.
     *
     * @param conn Conexão DBAL $conn the conn
     *
     * @return self
     */
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }
}
