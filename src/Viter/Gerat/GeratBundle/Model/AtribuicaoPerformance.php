<?php
/**
* AtribuicaoPerformance File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * AtribuicaoPerformance Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

class AtribuicaoPerformance
{
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * AtribuicaoPerformance::getDataInicial()
     *
     */
    public function getDataInicial()
    {
        $data = $this->conn->fetchAssoc("SELECT MAX(DT_INI) AS DT_INI FROM SPN_PERIODO_PRODUTO");

        return new \DateTime($data['DT_INI']);
    }

    /**
     * AtribuicaoPerformance::getDataFinal()
     *
     */
    public function getDataFinal()
    {
        $data = $this->conn->fetchAssoc("SELECT MAX(DT_FIM) AS DT_FIM FROM SPN_PERIODO_PRODUTO");

        return new \DateTime($data['DT_FIM']);
    }

    /**
     * AtribuicaoPerformance::getListaAtribuicaoPerformance()
     *
     */
    public function getListaAtribuicaoPerformance(\DateTime $dataInicio, \DateTime $dataFim)
    {
        $dataInicio = $dataInicio->format('Y-m-d');
        $dataFim = $dataFim->format('Y-m-d');

        $sql
            = "
            SELECT DISTINCT
                pp.co_prd AS CO_PRD
                ,p.no_prd AS NO_PRD
                ,pd.VR_RET_MES AS RENT_MES
            FROM
                [RISCO].[dbo].[spn_periodo_produto] pp
            INNER JOIN
                [SIRAT].[dbo].[Produto] p
            ON
                pp.co_prd = p.co_prd
            INNER JOIN
                [SIRAT].[dbo].[produto_dia_2] pd
            ON
                pd.co_prd = p.co_prd
            WHERE
                dt_ini = '$dataInicio' and
                dt_fim = '$dataFim' and
                ic_chk_ret_prd = 1 and
                pd.DT_REF = '$dataFim'
            ORDER BY
                pd.VR_RET_MES DESC
            ";

        //var_dump($dataInicio);
        //var_dump($dataFim);

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
