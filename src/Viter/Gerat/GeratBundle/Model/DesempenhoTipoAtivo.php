<?php
/**
* DesempenhoTipoAtivo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * DesempenhoTipoAtivo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class DesempenhoTipoAtivo
{
    /**
     * @var string
     * O nome do ativo
     */
    private $nome;

    /**
     * @var float
     * O valor do resultado financeiro
     */
    private $valor;

    /**
     * @var float
     * O percentual da participação do ativo sobre a rentabilidade
     */
    private $percentual;

    /**
     * Gets the O nome do ativo.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the O nome do ativo.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the Tipo do ativo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Sets the O Tipo do ativo.
     *
     * @param string $tipo the Tipo
     *
     * @return self
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Gets the O valor do resultado financeiro.
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Sets the O valor do resultado financeiro.
     *
     * @param float $valor the valor
     *
     * @return self
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Gets the O percentual da participação do ativo sobre a rentabilidade.
     *
     * @return float
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * Sets the O percentual da participação do ativo sobre a rentabilidade.
     *
     * @param float $percentual the percentual
     *
     * @return self
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;

        return $this;
    }

    /**
     * DesempenhoTipoAtivo::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return DesempenhoTipoAtivo
     */
    public function create($row)
    {
        /*
        NO_MTP_ATI
        NO_TP_ATI
        VR_RET_TP_ATI
        PC_RET_TP_ATI
        PC_RET_TP_ATI_BASE_100
        */
        $this->setTipo(utf8_encode($row['NO_MTP_ATI']));
        if ($row['NO_TP_ATI'] == utf8_decode('Compromiss. Público')) {
            $this->setTipo('Títulos Públicos');
        }
        $this->setNome(utf8_encode($row['NO_TP_ATI']));
        $this->setValor((float) $row['VR_RET_TP_ATI']);

        $this->setPercentual((float) $row['PC_RET_TP_ATI']);
        //$this->setPercentual((float) $row['PC_RET_TP_ATI_BASE_100']);
        return $this;
    }
}
