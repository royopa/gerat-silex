<?php
/**
* GrupoAtivo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* DPGE., FIDC., LF., Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * GrupoAtivo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class GrupoAtivo extends \ArrayIterator
{
    /**
     * @var int
     * Id do tipo de ativo
     */
    private $id;

    /**
     * @var string
     * Nome do tipo de ativo
     */
    private $macrotipo;

    /**
     * @var string
     * Nome do tipo de ativo
     */
    private $nome;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * Gets the Id do tipo de ativo.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the Id do tipo de ativo.
     *
     * @param int $id the id
     *
     * @return self
     */
    public function buscaDados($id = null)
    {
        $id = (int) $id;

        $sql =
        "
        SELECT
        DISTINCT
            M.CO_MTP_ATI,
            M.NO_MTP_ATI,
            A.CO_TP_ATI,
            A.NO_TP_ATI
        FROM
            spn_PERIODO_PRODUTO_TIPO_ATIVO A
        LEFT JOIN
            ATIVO_TIPO T
                ON A.CO_TP_ATI = T.CO_TP_ATI
        LEFT JOIN
            SIRAT.DBO.ATIVO_TIPO_MACROTIPO M
                ON T.CO_MTP_ATI = M.CO_MTP_ATI
        WHERE A.CO_TP_ATI = :id
        ";

        $row = $this->conn->fetchAssoc($sql, array($id));

        $this->setId($row['CO_MTP_ATI']);

        $row['NO_TP_ATI'] = iconv("ISO-8859-1", "UTF-8", $row['NO_TP_ATI']);
        $this->setNome($row['NO_TP_ATI']);

        $row['NO_MTP_ATI'] = iconv("ISO-8859-1", "UTF-8", $row['NO_MTP_ATI']);
        $this->setMacrotipo($row['NO_MTP_ATI']);

        return $this;
    }

    /**  * Sets the Id do
    tipo de ativo.  *  * @param int $id the id  *  * @return self  */ public function setId($id) {
    $this->id = $id;

        return $this;
    }

    /**
     * Gets the Nome do tipo de ativo.
     *
     * @return string
     */
    public function getMacrotipo()
    {
        return $this->macrotipo;
    }

    /**
     * Sets the Nome do tipo de ativo.
     *
     * @param string $macrotipo the macrotipo
     *
     * @return self
     */
    public function setMacrotipo($macrotipo)
    {
        $this->macrotipo = $macrotipo;

        return $this;
    }

    /**
     * Gets the Nome do tipo de ativo.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the Nome do tipo de ativo.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }
}
