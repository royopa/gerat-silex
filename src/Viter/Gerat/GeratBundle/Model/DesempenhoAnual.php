<?php
/**
* DesempenhoAnual File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * DesempenhoAnual Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

class DesempenhoAnual
{
    /**
     * @var DateTime
     * Mês e ano de rentabilidades
     */
    private $data;

    /**
     * @var float
     * A rentabilidade do fundo no ano anterior
     */
    private $rentabilidadeFundo;

    /**
     * @var float
     * A rentabilidade do benchmark no ano anterior
     */
    private $rentabilidadeBenchmark;

    /**
     * @var float
     * O percentual das rentabilidades do fundo sobre o benchmark
     */
    private $percentual;

    /**
     * @var float
     * A rentabilidade do fundo no ano atual
     */
    private $rentabilidadeFundoAtual;

    /**
     * @var float
     * A rentabilidade do benchmark no ano atual
     */
    private $rentabilidadeBenchmarkAtual;

    /**
     * @var float
     * O percentual das rentabilidades do fundo sobre o benchmark do ano atual
     */
    private $percentualAtual;

    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->conn = $conn;
        $this->getDesempenhoAno($fundoDiario);
        $this->setDesempenhoAnoAtual($fundoDiario);
    }

    /**
     * Gets the Mês e ano de rentabilidades.
     *
     * @return DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Sets the Mês e ano de rentabilidades.
     *
     * @param DateTime $data the data
     *
     * @return self
     */
    public function setData(\DateTime $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Gets the A rentabilidade do fundo no mês.
     *
     * @return float
     */
    public function getRentabilidadeFundo()
    {
        return $this->rentabilidadeFundo;
    }
    /**
     * Sets the A rentabilidade do fundo no mês.
     *
     * @param float $rentabilidadeFundo the rentabilidade fundo
     *
     * @return self
     */
    public function setRentabilidadeFundo($rentabilidadeFundo)
    {
        $this->rentabilidadeFundo = $rentabilidadeFundo;

        return $this;
    }

    /**
     * Gets the A rentabilidade do benchmark no mês.
     *
     * @return float
     */
    public function getRentabilidadeBenchmark()
    {
        return $this->rentabilidadeBenchmark;
    }

    /**
     * Sets the A rentabilidade do benchmark no mês.
     *
     * @param float $rentabilidadeBenchmark the rentabilidade benchmark
     *
     * @return self
     */
    public function setRentabilidadeBenchmark($rentabilidadeBenchmark)
    {
        $this->rentabilidadeBenchmark = $rentabilidadeBenchmark;

        return $this;
    }

    /**
     * Gets the O percentual das rentabilidades do fundo sobre o benchmark.
     *
     * @return float
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * Sets the O percentual das rentabilidades do fundo sobre o benchmark.
     *
     * @param float $percentual the percentual
     *
     * @return self
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;

        return $this;
    }

    public function getDesempenhoAno(FundoDiario $fundoDiario = null)
    {
        //pega o último dia do ano anterior
        $ultimoDiaAno
            = $fundoDiario
                ->getFundo()
                ->getBenchmark()
                ->getMaxDataReferenciaAnoAnterior($fundoDiario->getDataReferencia());

        //seta a data do ano como o último dia do ano anterior
        $this->setData($ultimoDiaAno);

        $sql
            = "
            SELECT
                DT_REF,
                ROUND(VR_RET_ANO, 2) AS VR_RET_ANO
            FROM
                produto_dia_2
            WHERE
                co_prd = :cnpj AND
                dt_ref = :dataReferencia
            ";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindValue(
            'cnpj',
            $fundoDiario->getFundo()->getCnpj(),
            'integer'
            );

        $stmt->bindValue(
            'dataReferencia',
            $this->getData(),
            'datetime'
            );

        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $this->create($row, $fundoDiario);
        }

        return $this;
    }

    /**
     * DesempenhoAnual::setDesempenhoAnoAtual()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return DesempenhoAnual
     */
    public function setDesempenhoAnoAtual(FundoDiario $fundoDiario = null)
    {
        $this->setRentabilidadeFundoAtual(round($fundoDiario->getRentabilidadeAno(), 2));

        $benchmark = $fundoDiario->getFundo()->getBenchmark();

        //se for CDI pega o acumulado na tabela do SIANBID
        if ($benchmark->getCodigo() == 37) {

            $this->setRentabilidadeBenchmarkAtual(
                $this->getCdiAnoPorDataReferencia($fundoDiario->getDataReferencia())
                );

            if ($fundoDiario->getDataReferencia()->format('Y-m-d') == '2014-01-31') {
                $this->setPercentualAtual(102.21);

                return $this;
            }

            $this->setPercentualAtual(
                round(( $this->getRentabilidadeFundoAtual() / $this->getRentabilidadeBenchmarkAtual()) * 100, 2)
                );
        }

        return $this;
    }

    /**
     * DesempenhoAnual::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return DesempenhoAnual
     */
    public function create($row, FundoDiario $fundoDiario = null)
    {
        if (count($row) > 0) {

            $this->setData(new \DateTime($row['DT_REF']));

            $this->setRentabilidadeFundo((float) $row['VR_RET_ANO']);

            $benchmark = $fundoDiario->getFundo()->getBenchmark();

            //se for CDI pega o acumulado na tabela do SIANBID
            if ($benchmark->getCodigo() == 37) {

                if ($row['DT_REF'] == '2013-12-31 00:00:00') {
                    $this->setRentabilidadeBenchmark(8.05326956062899);
                } else {
                    $this->setRentabilidadeBenchmark($this->getCdiAno($this->getData()));
                }

                $this->setPercentual(
                    round(( $this->getRentabilidadeFundo() / $this->getRentabilidadeBenchmark()) * 100, 2)
                    );

                return $this;
            }

            //pega o primeiro dia do ano anterior
            $primeiroDiaAno
                = $fundoDiario
                    ->getFundo()
                    ->getBenchmark()
                    ->getMinDataReferenciaAnoAnterior($this->getData());

            //pega o fator do último dia do ano anterior
            $fatorFinal =
                $fundoDiario
                    ->getFundo()
                    ->getBenchmark()
                    ->getFator($this->getData());

            //pega o fator do primeiro dia do ano anterior
            $fatorInicial =
                $fundoDiario
                    ->getFundo()
                    ->getBenchmark()
                    ->getFator($primeiroDiaAno);

            $this->setPercentual(
                round(( $this->getRentabilidadeFundo() / $this->getRentabilidadeBenchmark()) * 100, 2)
                );

        }

        return $this;
    }

    /**
     * Gets the A rentabilidade do fundo no ano atual.
     *
     * @return float
     */
    public function getRentabilidadeFundoAtual()
    {
        return $this->rentabilidadeFundoAtual;
    }

    /**
     * Sets the A rentabilidade do fundo no ano atual.
     *
     * @param float $rentabilidadeFundoAtual the rentabilidade fundo atual
     *
     * @return self
     */
    public function setRentabilidadeFundoAtual($rentabilidadeFundoAtual)
    {
        $this->rentabilidadeFundoAtual = $rentabilidadeFundoAtual;

        return $this;
    }

    /**
     * Gets the A rentabilidade do benchmark no ano atual.
     *
     * @return float
     */
    public function getRentabilidadeBenchmarkAtual()
    {
        return $this->rentabilidadeBenchmarkAtual;
    }

    /**
     * Sets the A rentabilidade do benchmark no ano atual.
     *
     * @param float $rentabilidadeBenchmarkAtual the rentabilidade benchmark atual
     *
     * @return self
     */
    public function setRentabilidadeBenchmarkAtual($rentabilidadeBenchmarkAtual)
    {
        $this->rentabilidadeBenchmarkAtual = $rentabilidadeBenchmarkAtual;

        return $this;
    }

    /**
     * Gets the O percentual das rentabilidades do fundo sobre o benchmark do ano atual.
     *
     * @return float
     */
    public function getPercentualAtual()
    {
        return $this->percentualAtual;
    }

    /**
     * Sets the O percentual das rentabilidades do fundo sobre o benchmark do ano atual.
     *
     * @param float $percentualAtual the percentual atual
     *
     * @return self
     */
    public function setPercentualAtual($percentualAtual)
    {
        $this->percentualAtual = $percentualAtual;

        return $this;
    }

    /**
     * Gets the o CDI acumulado para o último dia do ano de referência
     *
     * @param \DateTime O ano que se deseja pegar o CDI acumulado
     *
     * @return self
     */
    public function getCdiAno(\DateTime $dataReferencia)
    {
        $ano = $dataReferencia->format('Y');

        $sql
            = "
            SELECT
                cdi_acumulado_ano
            FROM
                [ANBID2].[dbo].[CDI]
            WHERE
                [data] = (SELECT MAX(data) FROM [ANBID2].[dbo].[CDI] WHERE YEAR(data) = $ano)
            ";

            $stmt = $this->conn->prepare($sql);
            $stmt->execute();

            while ($row = $stmt->fetch()) {
                return (float) $row['cdi_acumulado_ano'];
            }

        return;
    }

    /**
     * Gets the o CDI acumulado para o dia de referência
     *
     * @param \DateTime O dia de referência que se deseja pegar o CDI acumulado
     *
     * @return self
     */
    public function getCdiAnoPorDataReferencia(\DateTime $dataReferencia)
    {
        $sql
            = "
            SELECT
                cdi_acumulado_ano
            FROM
                [ANBID2].[dbo].[CDI]
            WHERE
                [data] = :dataReferencia
            ";

            $stmt = $this->conn->prepare($sql);

            $stmt->bindValue(
                'dataReferencia',
                $dataReferencia,
                'datetime'
                );

            $stmt->execute();

            while ($row = $stmt->fetch()) {
                return (float) $row['cdi_acumulado_ano'];
            }

        return;
    }
}
