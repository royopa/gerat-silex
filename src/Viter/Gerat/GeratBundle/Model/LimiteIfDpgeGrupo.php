<?php
/**
* LimiteIfDpgeGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * LimiteIfDpgeGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LimiteIfDpgeGrupo extends LimiteAlocacaoGrupo
{
    public function __construct(FundoDiario $fundoDiario = null,  $conn)
    {
        $this->conn = $conn;
        $this->setNome('DPGE');
        $this->fundoDiario = $fundoDiario;
        $this->fetchAll($fundoDiario);
        $this->preencheTotalizadores();
    }

    /**
     * LimiteIfDpgeGrupo::preencheTotalizadores()
     *
     * @return LimiteIfDpgeGrupo
     *
     * Preenche os totalizadores de valor e percentual total de alocação
     */
    public function preencheTotalizadores()
    {
        foreach ($this as $limite) {
            $this->setValorTotal(
                $this->getValorTotal() + $limite->getValorAlocado()
            );
        }

        $this->setPercentualTotal(
            ($this->getValorTotal() / $this->fundoDiario->getPatrimonioLiquido()) * 100
            );

        return $this;
    }
}
