<?php
/**
* ListaAtivosMercado File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* DPGE., FIDC., LF., Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * ListaAtivosMercado Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ListaAtivosMercado extends \ArrayIterator
{
    public function __construct($conn)
    {
        $this->conn = $conn;
        $this->fetchAll();
    }

    /**
     * ListaAtivosMercado::fetchAll()
     *
     * @param FundoDia $fundoDiario O objeto fundo diário
     *
     * @return ListaAtivosMercado
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll()
    {
        $sql
            = "
            SELECT
                a.CO_ATIVO,
                a.NO_ATIVO,
                a.NO_GR_ATIVO,
                a.NO_ATIVO_LEGIVEL,
                t.CO_TP_ATI,
                AT.NO_TP_ATI
            FROM
                Produto_Dia_Expo_Ativo_x_Fator_Ativo a
            LEFT JOIN
                Produto_Dia_Expo_Ativo_x_Fator_Tipo_Ativo t
            ON a.CO_ATIVO = t.CO_ATIVO
            LEFT JOIN
                RISCO.DBO.Ativo_Tipo AT
            ON AT.CO_TP_ATI=T.CO_TP_ATI
            ";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $ativo = new AtivoRiscoMercado();
            $this->append($ativo->create($row));
        }

        return $this;
    }
}
