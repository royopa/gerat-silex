<?php
/*
 * This file is part of the Gerat package.
 *
 * (c) Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

class Fundo
{
    /**
     * @var string
     * Nome do fundo
     */
    private $nome;

    /**
     * @var string
     * CNPJ do fundo
     */
    private $cnpj;

    /**
     * @var boolean
     * Indicador se o fundo está ativo ou não
     */
    private $isAtivo;

    /**
     * @var float
     * Percentual da taxa de administração do fundo
     */
    private $taxaAdministracao;

    /**
     * @var string
     * Nome do fundo no SISFIN
     */
    private $nomeSisfin;

    /**
     * @var Benchmark
     * o Benchmark do fundo
     */
    private $benchmark;

    /**
     * @var DateTime
     * Data de início do fundo
     */
    private $dataInicio;

    /**
     * @var int
     * Tipo do fundo
     */
    private $tipo;

    /**
     * @var int
     * O código da política que o fundo segue -> 1 = CR245
     */
    private $codigoPolitica;

    /**
     * @param string $co_prd O CNPJ do fundo
     */
    public function __construct($co_prd = 0, $conn)
    {
        $this->conn = $conn;

        if ((int) $co_prd > 0) {
            $this->cnpj = $co_prd;
            $this->processa();
        }
    }

    /**
     * Fundo::getNome()
     *
     * @param void
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Fundo::getCnpj()
     *
     * @param void
     *
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Fundo::getCnpjFormatado()
     *
     * @param void
     *
     * @return string
     */
    public function getCnpjFormatado()
    {
        return $this->formatarCPF_CNPJ($this->cnpj);
    }

    /**
     * Fundo::getIsAtivo()
     *
     * @param void
     *
     * @return boolean
     */
    public function getIsAtivo()
    {
        return abs($this->isAtivo * -1);
    }

    /**
     * Fundo::getTaxaAdministracao()
     *
     * @param void
     *
     * @return float
     */
    public function getTaxaAdministracao()
    {
        return $this->taxaAdministracao;
    }

    /**
     * Fundo::setNome()
     *
     * @param string
     *
     * @return Fundo
     */
    public function setNome($nome)
    {
        //$this->nome = utf8_encode($nome);
        $this->nome = $nome;

        return $this;
    }

    /**
     * Fundo::setCnpj()
     *
     * @param string
     *
     * @return Fundo
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Fundo::setIsAtivo()
     *
     * @param boolean
     *
     * @return Fundo
     */
    public function setIsAtivo($isAtivo)
    {
        $this->isAtivo = abs($isAtivo * -1);

        return $this;
    }

    /**
     * Fundo::setTaxaAdministracao()
     *
     * @param float
     *
     * @return Fundo
     */
    public function setTaxaAdministracao($taxaAdministracao)
    {
        $this->taxaAdministracao = $taxaAdministracao;

        return $this;
    }

    /**
     * Fundo::processa()
     * Faz a busca no banco de dados e preenche o objeto
     *
     * @return FundoDiario
     */
    public function processa()
    {
        $sql
            = "
            SELECT
                p.CO_PRD,
                p.NO_PRD,
                p.CO_TIPO,
                p.IC_DESAT,
                p.NO_PRD_SISFIN,
                p.PC_TX_ADM,
                p.CO_BENCH,
                p.DT_INI_PRD,
                b.NO_BENCH,
                f.dataini,
                ps.[CO_POL]
            FROM
                [SIRAT].[dbo].[Produto] p
            LEFT JOIN
                BENCH b
            ON
                b.CO_BENCH = p.CO_BENCH
            INNER JOIN
                [ANBID2].[dbo].[fundos] f
            ON
                cast(f.cnpj as bigint) = cast(p.CO_PRD as bigint)
            INNER JOIN
                [SIRAT].[dbo].[Produto_Segmento] ps
            ON
                p.[CO_SEG] = ps.[CO_SEG]
            WHERE
                CO_PRD = :cnpj
            ";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(
            'cnpj',
            $this->cnpj,
            'integer'
            );

        $stmt->execute();

        while ($row = $stmt->fetch()) {

            $this->cnpj = $row['CO_PRD'];
            $this->nome = $row['NO_PRD'];
            $this->isAtivo = $row['IC_DESAT'];
            $this->tipo = $row['CO_TIPO'];
            $this->setNomeSisfin = $row['NO_PRD_SISFIN'];
            $this->taxaAdministracao = round($row['PC_TX_ADM'], 2);
            $this->dataInicio = new \DateTime($row['dataini']);
            $this->codigoPolitica = (int) $row['CO_POL'];

            $benchmark = new Benchmark($this->conn);

            if (is_null($row['CO_BENCH'])) {
                $benchmark->setCodigo(37);
                $benchmark->setNome('CDI');
            } else {
                $benchmark->setCodigo((int) $row['CO_BENCH']);
                $benchmark->setNome($row['NO_BENCH']);
            }

            //provisoriamente está sendo utilizado o CDI para todos
            $benchmark->setCodigo(37);
            $benchmark->setNome('CDI');

            $this->benchmark = $benchmark;
        }

        return $this;
    }

    /**
     * Fundo::getBenchmark()
     *
     * @param void
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Fundo::setBenchmark()
     *
     * @param codigo
     *
     * @return Benchmark
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Gets the Nome do fundo no SISFIN.
     *
     * @return string
     */
    public function getNomeSisfin()
    {
        return $this->nomeSisfin;
    }

    /**
     * Sets the Nome do fundo no SISFIN.
     *
     * @param string $nomeSisfin the nome sisfin
     *
     * @return self
     */
    public function setNomeSisfin($nomeSisfin)
    {
        $this->nomeSisfin = $nomeSisfin;

        return $this;
    }

    /**
     * Gets the o Benchmark do fundo.
     *
     * @return Benchmark
     */
    public function getBenchmark()
    {
        return $this->benchmark;
    }

    /**
     * Gets the data de início do fundo.
     *
     * @return DateTime
     */
    public function getDataInicio()
    {
        return $this->dataInicio;
    }

    /**
     * Sets the data de início do fundo.
     *
     * @param DateTime $dataInicio A data de início do fundo
     *
     * @return self
     */
    public function setDataInicio(DateTime $dataInicio)
    {
        $this->dataInicio = $dataInicio;

        return $this;
    }

    /**
     * Sets the o Benchmark do fundo.
     *
     * @param Benchmark $benchmark the benchmark
     *
     * @return self
     */
    public function setBenchmark(Benchmark $benchmark)
    {
        $this->benchmark = $benchmark;

        return $this;
    }

    public function formatarCPF_CNPJ($campo, $formatado = true)
    {
        //retira formato
       $codigoLimpo = preg_replace("[' '-./ t]",'',$campo);
        // pega o tamanho da string menos os digitos verificadores
        $tamanho = (strlen($codigoLimpo) -2);
        //verifica se o tamanho do código informado é válido
        if ($tamanho != 9 && $tamanho != 12) {
            return false;
        }

        if ($formatado) {
            // seleciona a máscara para cpf ou cnpj
            $mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##';

            $indice = -1;
            for ($i=0; $i < strlen($mascara); $i++) {
                if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
            }
            //retorna o campo formatado
            $retorno = $mascara;

        } else {
            //se não quer formatado, retorna o campo limpo
            $retorno = $codigoLimpo;
        }

        return $retorno;
    }

    /**
     * Gets the Tipo do fundo.
     *
     * @return int
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Sets the Tipo do fundo.
     *
     * @param int $tipo the tipo
     *
     * @return self
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Gets the O código da política que o fundo segue -> 1 = CR245.
     *
     * @return int
     */
    public function getCodigoPolitica()
    {
        return $this->codigoPolitica;
    }

    /**
     * Sets the O código da política que o fundo segue -> 1 = CR245.
     *
     * @param int $codigoPolitica the codigo politica
     *
     * @return self
     */
    public function setCodigoPolitica($codigoPolitica)
    {
        $this->codigoPolitica = $codigoPolitica;

        return $this;
    }
}
