<?php
/**
* ListaAtivosCreditoGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* DPGE., FIDC., LF., Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/
namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Highcharts\Highchart;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartJsExpr;

/**
 * ListaAtivosCreditoGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ListaAtivosCreditoGrupo extends \ArrayIterator
{
    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->conn = $conn;
        $this->fetchAll($fundoDiario);
        $this->montaGrafico();
    }

    /**
     * AtivoCreditoGrupo::fetchAll()
     *
    * @param FundoDia $fundoDiario O objeto fundo diário
     *
     * @return AtivoCreditoGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario)
    {
        $sql
            = "
            SELECT
                NO_ATI
                ,NO_TP_ATI
                ,NO_EMISS
                ,SUM(VR_MERC) AS VR_MERC
            FROM
                [SIRAT].[dbo].[CR245002_RC_F_11]
            WHERE
                DT_ATU = :dataAtualizacao AND
                CO_PRD = :cnpj
            GROUP BY
                NO_ATI
                ,NO_TP_ATI
                ,NO_EMISS
            ORDER BY
                NO_TP_ATI,
                NO_EMISS
            ";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindValue(
            'cnpj',
            $fundoDiario->getFundo()->getCnpj(),
            'integer'
            );

        $stmt->bindValue(
            'dataAtualizacao',
            $fundoDiario->getDataAtualizacao(),
            'datetime'
            );

        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $ativo = new AtivoEmissorCredito();
            $this->append($ativo->create($fundoDiario, $row));
        }

        return $this;
    }

    /**
     * ListaComposicaoCarteiraCredito::montaGrafico()
     *
     * @return ListaComposicao
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function montaGrafico()
    {
        $this
            ->setGrafico(
                $this
                    ->getColumnChart(
                        $this->montaArray(),
                        'chart_composicao_credito',
                        null,
                        ''
                    )
            );

        return $this;
    }

    /**
    * Monta o gráfico Highchart de pizza com legendas da série recebida
    *
    * @param mixed[] $dados_serie Dados da série para a geração do gráfico
    * @param string  $render_div  ID da div onde o gráfico será mostrado
    * @param string  $name        Nome que será mostrado no gráfico
    * @param string  $title       Titulo que será mostrado no gráfico
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getColumnChart($dados_serie = null, $render_div = '', $name = '', $title = '')
    {
        $subtitle   = "";
        $categorias = array();

        $chart = new Highchart();

        $chart->chart->renderTo = $render_div;
        $chart->chart->type     = "column";
        $chart->title->text     = $title;
        $chart->subtitle->text  = $subtitle;

        $chart->xAxis->categories = array(
            " % em relação ao PL"," % em relação ao PL"," % em relação ao PL",
        );

        foreach ($dados_serie as $row) {

            $categorias[] = $row[0];

            $chart->series[] = array(
                'name' => $row[0],
                'data' => array(
                    $row[1]
                )
            );
        }

        $chart->yAxis->min = 0;
        $chart->yAxis->title->text = "Patrimônio Líquido %";

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function () {
                return '' + this.series.name + ' : ' +
                Highcharts.numberFormat(this.y,2,',','.') + '%';
            }"
        );

//Highcharts.numberFormat(this.y,2,',','.')

        $chart->plotOptions->column->pointPadding = 0.2;
        $chart->plotOptions->column->borderWidth = 0;

        //$chart->xAxis->categories = $categorias;
        //$chart->xAxis->labels->rotation = - 45;
        $chart->xAxis->labels->align = "center";
        $chart->xAxis->labels->style->font = "normal 13px Verdana, sans-serif";

        $chart->legend->enabled = true;

        return $chart;
    }

    /**
     * Monta o array, get dados série que será usado no gráfico
     *
     * @return self
     */
    public function montaArray()
    {
        $dadosSerie     = array();
        $somaPercentual = 0;

        foreach ($this as $ativo) {

            $dados = array(
                0 => $ativo->getEmissor(),
                1 => $ativo->getPercentual()
            );

            $dadosSerie[]   = $dados;
        }

        return $dadosSerie;
    }

    /**
     * Gets the O gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the O gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }
}
