<?php
/**
* AtivoEmissorCredito File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * AtivoEmissorCredito Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

class AtivoEmissorCredito
{
    /**
     * @var string
     * O tipo do ativo
     */
    private $tipo;

    /**
     * @var string
     * O nome do emissor do ativo
     */
    private $emissor;

    /**
     * @var float
     * O valor de mercado
     */
    private $valorMercado;

    /**
     * @var float
     * O percentual do PL do fundo
     */
    private $percentual;

    /**
     * AtivoEmissorCredito::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return AtivoEmissorCredito
     */
    public function create(FundoDiario $fundoDiario = null, $row)
    {
        if (count($row) > 0) {
            $this->setTipo(utf8_encode($row['NO_TP_ATI']));

            $this->setEmissor(utf8_encode($row['NO_EMISS']));

            if ($this->getTipo() == 'FIDC') {
                $this->setEmissor(utf8_encode($row['NO_ATI']));
            }

            $this->setValorMercado((float) $row['VR_MERC']);
            $percentual = ($this->getValorMercado() / $fundoDiario->getPatrimonioLiquido()) * 100;
            $this->setPercentual(round($percentual, 2));
        }

        return $this;
    }

    /**
     * Gets the O tipo do ativo.
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Sets the O tipo do ativo.
     *
     * @param string $tipo the tipo
     *
     * @return self
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Gets the O nome do emissor do ativo.
     *
     * @return string
     */
    public function getEmissor()
    {
        return $this->emissor;
    }

    /**
     * Sets the O nome do emissor do ativo.
     *
     * @param string $emissor the emissor
     *
     * @return self
     */
    public function setEmissor($emissor)
    {
        $this->emissor = $emissor;

        return $this;
    }

    /**
     * Gets the O valor de mercado.
     *
     * @return float
     */
    public function getValorMercado()
    {
        return $this->valorMercado;
    }

    /**
     * Sets the O valor de mercado.
     *
     * @param float $valorMercado the valor mercado
     *
     * @return self
     */
    public function setValorMercado($valorMercado)
    {
        $this->valorMercado = $valorMercado;

        return $this;
    }

    /**
     * Gets the O percentual do PL do fundo.
     *
     * @return float
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * Sets the O percentual do PL do fundo.
     *
     * @param float $percentual the percentual
     *
     * @return self
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;

        return $this;
    }
}
