<?php
/**
* LimiteNaoIfGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * LimiteNaoIfGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LimiteNaoIfGrupo extends LimiteAlocacaoGrupo
{
    public function __construct(FundoDiario $fundoDiario = null,  $conn)
    {
        $this->conn = $conn;
        $this->setNome('INF');
        $this->fundoDiario = $fundoDiario;
        $this->fetchAll($fundoDiario);
        $this->preencheTotalizadores();
        $this->setSerieDadosLimiteAlocacao(new SerieDadosLimiteAlocacao($this));
        $this->montaGrafico();
    }

    public function montaGrafico()
    {
        $grafico = new Grafico();
        $this
            ->setGrafico(
                $grafico
                    ->getColumnChart(
                        $this->getSerieDadosLimiteAlocacao()->getSerieArray(),
                        'chart_limite_nao_if',
                        null,
                        "Instituições Não Financeiras"
                    )
            );

        return $this;
    }
}
