<?php
/**
* ProdutoDia File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * ProdutoDia Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

class ProdutoDia
{
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * ProdutoDia::getMinDataReferenciaMesAnterior(DateTime $dataReferencia)
     *
     */
    public function getMinDataReferenciaMesAnterior(\DateTime $dataReferencia = null)
    {
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                MIN(DT_REF) AS DT_REF
            FROM
                Produto_Dia
            WHERE
                MONTH(DT_REF) = ? AND
                YEAR(DT_REF) = ?
            ";

        $minDataReferenciaMesAnterior = new \DateTime($dataReferencia->format('Y-m-d'));

        $mes = (int) $minDataReferenciaMesAnterior->sub(new \DateInterval('P1M3D'))->format('m');
        $ano = (int) $minDataReferenciaMesAnterior->format('Y');

        $data = $conn->fetchAssoc($sql, array($mes, $ano));

        return new \DateTime($data['DT_REF']);
    }

    /**
     * ProdutoDia::getMinDataReferenciaMesAtual(DateTime $dataReferencia)
     *
     */
    public function getMinDataReferenciaMesAtual(\DateTime $data = null)
    {
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                MIN(DT_REF) AS DT_REF
            FROM
                Produto_Dia
            WHERE
                MONTH(DT_REF) = ? AND
                YEAR(DT_REF) = ?
            ";

        $mes = (int) $data->format('m');
        $ano = (int) $data->format('Y');

        $data = $conn->fetchAssoc($sql, array($mes, $ano));

        return new \DateTime($data['DT_REF']);
    }

    /**
     * ProdutoDia::getMaxDataReferenciaMesAnterior(DateTime $dataReferencia)
     *
     */
    public function getMaxDataReferenciaMesAnterior(\DateTime $dataReferencia = null)
    {
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                MAX(DT_REF) AS DT_REF
            FROM
                Produto_Dia
            WHERE
                MONTH(DT_REF) = ? AND
                YEAR(DT_REF) = ?
            ";

        $maxDataReferenciaMesAnterior = new \DateTime($dataReferencia->format('Y-m-d'));

        $mes = (int) $maxDataReferenciaMesAnterior->sub(new \DateInterval('P1M3D'))->format('m');
        $ano = (int) $maxDataReferenciaMesAnterior->format('Y');

        $data = $conn->fetchAssoc($sql, array($mes, $ano));

        return new \DateTime($data['DT_REF']);
    }

    /**
     * ProdutoDia::getMaxDataAtualizacao()
     *
     */
    public function getMaxDataAtualizacao()
    {
        $data = $this->conn->fetchAssoc('SELECT MAX(dt_atu) AS dt_atu FROM produto_dia');

        return new \DateTime($data['dt_atu']);
    }

    /**
     * ProdutoDia::getMaxDataReferencia()
     *
     */
    public function getMaxDataReferencia()
    {
        $data = $this->conn->fetchAssoc('SELECT MAX(dt_ref) AS dt_ref FROM produto_dia');

        return new \DateTime($data['dt_ref']);
    }

    /**
     * ProdutoDia::getDataReferencia()
     *
     */
    public function getDataReferencia(\DateTime $dataAtualizacao)
    {
        $dataAtualizacao = $dataAtualizacao->format('Y-m-d');
        $data = $this->conn->fetchAssoc("SELECT DISTINCT MAX(dt_ref) AS dt_ref FROM produto_dia WHERE dt_atu = '$dataAtualizacao'");

        return new \DateTime($data['dt_ref']);
    }

    /**
     * ProdutoDia::getDataAtualizacao()
     *
     */
    public function getDataAtualizacao(\DateTime $dataReferencia)
    {
        $dataReferencia = $dataReferencia->format('Y-m-d');
        $data = $this->conn->fetchAssoc("SELECT DISTINCT MIN(dt_atu) AS dt_atu FROM produto_dia WHERE dt_ref = '$dataReferencia'");

        return new \DateTime($data['dt_atu']);
    }

    /**
     * ProdutoDia::getDataReferenciaAnterior()
     *
     */
    public function getDataReferenciaAnterior(\DateTime $dataReferencia)
    {
        $dataReferencia = $dataReferencia->format('Y-m-d');
        $data = $this->conn->fetchAssoc(
            "
            SELECT
                TOP 1 dt_ref as dt_ref
            FROM
                PRODUTO_DIA
            WHERE
                DT_REF < '$dataReferencia'
            ORDER BY
                DT_REF
            DESC
            "
            );

        return new \DateTime($data['dt_ref']);
    }
}
