<?php
/**
* Benchmark File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * Benchmark Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

class Benchmark
{
    /**
     * @var int
     * Código benchmark
     */
    private $codigo;

    /**
     * @var string
     * Nome benchmark
     */
    private $nome;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * Benchmark::getNome()
     *
     * @param void
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Benchmark::setNome()
     *
     * @param nome
     *
     * @return Emissor
     */
    public function setNome($nome)
    {
        $this->nome = utf8_encode($nome);

        return $this;
    }

    /**
     * Benchmark::getCodigo()
     *
     * @param void
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Benchmark::setCodigo()
     *
     * @param codigo
     *
     * @return Benchmark
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Benchmark::getMaxDataReferenciaMesAnterior(DateTime $dataReferencia)
     *
     * @param codigo
     *
     */
    public function getMaxDataReferenciaMesAnterior(\DateTime $dataReferencia = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                MAX(DT_REF) AS DT_REF
            FROM
                Bench_Fator
            WHERE
                CO_BENCH = ? AND
                MONTH(DT_REF) = ? AND
                YEAR(DT_REF) = ?
            ";

        $maxDataReferenciaMesAnterior = new \DateTime($dataReferencia->format('Y-m-d'));
        $mes = (int) $maxDataReferenciaMesAnterior->sub(new \DateInterval('P1M3D'))->format('m');
        $ano = (int) $maxDataReferenciaMesAnterior->format('Y');

        $data = $conn->fetchAssoc($sql, array($this->getCodigo(), $mes, $ano));

        return new \DateTime($data['DT_REF']);
    }

    /**
     * Benchmark::getMinDataReferenciaMesAnterior(DateTime $dataReferencia)
     *
     * @param codigo
     *
     */
    public function getMinDataReferenciaMesAnterior(\DateTime $dataReferencia = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                MIN(DT_REF) AS DT_REF
            FROM
                Bench_Fator
            WHERE
                CO_BENCH = ? AND
                MONTH(DT_REF) = ? AND
                YEAR(DT_REF) = ?
            ";

        $minDataReferenciaMesAnterior = new \DateTime($dataReferencia->format('Y-m-d'));

        $mes = (int) $minDataReferenciaMesAnterior->sub(new \DateInterval('P1M3D'))->format('m');
        $ano = (int) $minDataReferenciaMesAnterior->format('Y');

        $data = $conn->fetchAssoc($sql, array($this->getCodigo(), $mes, $ano));

        return new \DateTime($data['DT_REF']);
    }

    /**
     * Benchmark::getMaxDataReferenciaAnoAnterior(DateTime $dataReferencia)
     *
     * @param dataReferencia
     *
     */
    public function getMaxDataReferenciaAnoAnterior(\DateTime $dataReferencia = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                MAX(DT_REF) AS DT_REF
            FROM
                Bench_Fator
            WHERE
                CO_BENCH = ? AND
                YEAR(DT_REF) = ?
            ";

        //ultimo dia do ano anterior
        $maxDataReferenciaAnoAnterior = new \DateTime($dataReferencia->format('Y-m-d'));
        //diminui um ano do ano atual/referencia
        $maxDataReferenciaAnoAnterior->sub(new \DateInterval('P1Y'));
        $ano = (int) $maxDataReferenciaAnoAnterior->format('Y');

        $data = $conn->fetchAssoc($sql, array($this->getCodigo(), $ano));

        return new \DateTime($data['DT_REF']);
    }

    /**
     * Benchmark::getMinDataReferenciaAnoAnterior(DateTime $dataReferencia)
     *
     * @param dataReferencia
     *
     */
    public function getMinDataReferenciaAnoAtual(\DateTime $dataReferencia = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                MIN(DT_REF) AS DT_REF
            FROM
                Bench_Fator
            WHERE
                CO_BENCH = ? AND
                YEAR(DT_REF) = ?
            ";

        $ano = (int) $dataReferencia->format('Y');
        $data = $conn->fetchAssoc($sql, array($this->getCodigo(), $ano));

        return new \DateTime($data['DT_REF']);
    }

    /**
     * Benchmark::getRentabilidadeMensal()
     *
     * @param codigo
     *
     */

    //o CDI o certo é de 31 do mes anterior a 30 do mes atual
    public function getRentabilidadeMensal
        (
            \DateTime $maxDataReferenciaInicial = null,
            \DateTime $maxDataReferenciaFinal = null
        )
    {
        $sql
            = "
            SELECT
                DT_REF,
                VR_FAT
            FROM
                Bench_Fator
            WHERE
                CO_BENCH = :codigoBench AND
                DT_REF = :maxDataReferenciaInicial
            ORDER BY
                DT_REF
            ";

        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(
           'codigoBench',
            $this->getCodigo(),
            'integer'
        );

        $stmt->bindValue(
            'maxDataReferenciaInicial',
            $maxDataReferenciaInicial,
            'datetime'
            );

        $stmt->execute();

        $arrayFatores = $stmt->fetch();

        $fatorInicial = $arrayFatores[0]['VR_FATOR'];
        $fatorFinal   = $arrayFatores[1]['VR_FATOR'];

        return $fatorFinal / $fatorInicial;
    }

    /**
     * Benchmark::getFator()
     *
     * @param codigo
     *
     */
    public function getFator(\DateTime $dataReferencia = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                ROUND(VR_FAT, 6) AS VR_FAT
            FROM
                Bench_Fator
            WHERE
                CO_BENCH = ? AND
                DT_REF = ?
            ";

        $data
            = $conn->fetchAssoc(
                $sql, array(
                    $this->getCodigo(),
                    $dataReferencia->format('Y-m-d'))
            );

        return (float) $data['VR_FAT'];
    }

    /**
     * Benchmark::getRetornoDia()
     *
     * @param codigo
     *
     */
    public function getRetornoDia(\DateTime $dataReferencia = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $sql
            = "
            SELECT
                VR_RET_DIA
            FROM
                Bench_Fator
            WHERE
                CO_BENCH = ? AND
                DT_REF = ?
            ";

        $data
            = $conn->fetchAssoc(
                $sql, array(
                    $this->getCodigo(),
                    $dataReferencia->format('Y-m-d'))
            );

        return (float) $data['VR_RET_DIA'];
    }
}
