<?php
/**
* ListaDistribuicaoCotistas File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* LF, Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/
namespace Viter\Gerat\GeratBundle\Model;

/**
 * ListaDistribuicaoCotistas Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ListaDistribuicaoCotistas extends ListaComposicao
{
    /**
     * @var float
     * O valor total
     */
    private $valor;

    /**
     * @var float
     * O percentual total
     */
    private $percentual;

    /**
     * @var float
     * A quantidade de cotistas total
     */
    private $quantidade;

    /**
     * @var float
     * A quantidade de cotistas total do mês anterior
     */
    private $quantidadeMesAnterior;

    public function __construct(FundoDiario $fundoDiario = null,  $conn)
    {
        $this->conn = $conn;
        $this->fundoDiario = $fundoDiario;
        $this->fetchAll($fundoDiario);
        $this->preencheTotalizadores();
        $this->setSerieDados(new SerieDados($this));
        $this->montaGrafico();
    }

    /**
     * ListaDistribuicaoCotistas::fetchAll()
     *
     * @param FundoDia $fundoDiario O objeto fundo diário
     *
     * @return ListaDistribuicaoCotistas
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        $dataReferencia = $fundoDiario->getDataReferencia()->format('Y-m-d');
        $sql
            = "
            SELECT
                P.CO_PRD CO_PRD,
                P.NO_PRD NO_PRD,
                A.VR_COTA_FF VR_COTA_FF,
                SUM(C.NU_COTIS) NU_COTIS
            FROM
                DISTR_FIC_P_FF A
            INNER JOIN
                PRODUTO P
            ON
                A.CO_FIC = P.CO_PRD
            LEFT JOIN
                CR245001_RL_23 C
            ON
                C.CO_FDO = P.CO_PRD AND
                C.DT_REF = '$dataReferencia'
            WHERE
                A.CO_FF = :cnpj AND
                A.DT_ATU = :dataAtualizacao
            GROUP BY
                P.CO_PRD,
                P.NO_PRD,
                A.VR_COTA_FF
            ORDER BY
                A.VR_COTA_FF DESC
            ";

        $stmt = $this->conn->prepare($sql);

        $stmt->bindValue(
            'cnpj',
            $fundoDiario->getFundo()->getCnpj(),
            'integer'
            );

        $stmt->bindValue(
            'dataAtualizacao',
            $fundoDiario->getDataAtualizacao(),
            'datetime'
            );

        $stmt->execute();
        $bench = $fundoDiario->getFundo()->getBenchmark();
        $dataReferencia = $fundoDiario->getDataReferencia();
        $maxData = $bench->getMaxDataReferenciaMesAnterior($dataReferencia);

        while ($row = $stmt->fetch()) {

            $cotista = new Cotista($this->conn);
            //popula o objeto com os dados do banco
            $cotista->create($fundoDiario, $row, $maxData);
            //adiciona o objeto cotista na lista;
            $this->append($cotista);
        }

        return $this;
    }
    /**
     * ListaDistribuicaoCotistas::montaGrafico()
     *
     * @return ListaComposicao
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();
        $this
            ->setGrafico(
                $grafico
                    ->getColumnChart(
                        $this->getSerieDados()->getSerieArray(),
                        'chart_distribuicao_cotistas',
                        null,
                        'Distribuição de cotistas'
                    )
            );

        return $this;
    }

    /**
     * Gets the O valor total.
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Sets the O valor total.
     *
     * @param float $valor the valor
     *
     * @return self
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Gets the O percentual total.
     *
     * @return float
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * Sets the O percentual total.
     *
     * @param float $percentual the percentual
     *
     * @return self
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;

        return $this;
    }

    /**
     * ListaDistribuicaoCotistas::preencheTotalizadores
     *
     */
    public function preencheTotalizadores()
    {
        foreach ($this as $composicao) {
            $this->valor = $this->valor + $composicao->getValor();
            $this->percentual = $this->percentual + $composicao->getPercentual();
            $this->quantidade = $this->quantidade + $composicao->getQuantidade();
            $this->quantidadeMesAnterior = $this->quantidadeMesAnterior + $composicao->getQuantidadeMesAnterior();
        }
    }

    /**
     * Gets the A quantidade de cotistas total.
     *
     * @return float
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * Sets the A quantidade de cotistas total.
     *
     * @param float $quantidade the quantidade
     *
     * @return self
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;

        return $this;
    }

    /**
     * Gets the A quantidade de cotistas total do mês anterior.
     *
     * @return float
     */
    public function getQuantidadeMesAnterior()
    {
        return $this->quantidadeMesAnterior;
    }

    /**
     * Sets the A quantidade de cotistas total do mês anterior.
     *
     * @param float $quantidadeMesAnterior the quantidade mes anterior
     *
     * @return self
     */
    public function setQuantidadeMesAnterior($quantidadeMesAnterior)
    {
        $this->quantidadeMesAnterior = $quantidadeMesAnterior;

        return $this;
    }
}
