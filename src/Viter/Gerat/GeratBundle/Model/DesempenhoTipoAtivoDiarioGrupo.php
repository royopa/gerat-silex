<?php
/**
* DesempenhoTipoAtivoDiarioGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * DesempenhoTipoAtivoDiarioGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Highcharts\Highchart;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartJsExpr;

class DesempenhoTipoAtivoDiarioGrupo extends \ArrayIterator
{
    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * @var float
     * O resultado financeiro total
     */
    private $resultado;

    /**
     * @var float
     * O valor total
     */
    private $valorTotal;

    /**
     * @var float
     * Rentabilidade total do fundo
     */
    private $rentabilidadeTotal;

    /**
     * @var float
     * O valor das despesas do fundo
     */
    private $despesa;

    /**
     * Gets the gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $desempenho;
    }

    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->conn = $conn;

        if ($fundoDiario instanceof FundoDiario) {
            $this->fetchAll($fundoDiario);
            $this->preencheRentabilidadeTotal();
            $this->preencheResultadoTotal();
            $this->montaGrafico();
        }
    }

    /**
    * Monta um array com a série da composição da carteira de crédito que
    * será usada para montagem do gráfico de coluna da carteira de crédito
    *
    * @return  mixed[] $dados_serie Um array com os arrays que serão usados
    */
    public function montaSerieDados()
    {
        $dadosSerie        = array();
        $datas             = array();

        foreach ($this as $desempenhoTipoAtivo) {

            $desempenhoArray = array();
            $desempenhoArray['name']      = $desempenhoTipoAtivo->getNome();
            $desempenhoArray['y']         = $desempenhoTipoAtivo->getPercentual();
            $desempenhoArray['valor']     = number_format($desempenhoTipoAtivo->getValor(),2,",",".");

            $dadosSerie[] = $desempenhoArray;
        }

        return $dadosSerie;
    }

    /**
     * DesempenhoTipoAtivoDiarioGrupo::montaGrafico()
     *
     * @return DesempenhoTipoAtivoDiarioGrupo
     *
     * Monta o gráfico de colunas dos retornos fundo/benchmark
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();

        $this->grafico =
            $this->getDesempenhoTipoAtivoChart(
                    $this->montaSerieDados(),
                    'chart_desempenho_tipo_ativo_diario',
                    null,
                    'Desempenho por tipo de ativo'
                );

        return $this;
    }

    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        $dbal = new Dbal();
        $conn = $dbal->getConnRisco();

        $dataReferencia = $fundoDiario->getDataReferencia()->format('Y-m-d');
        $cnpj = $fundoDiario->getFundo()->getCnpj();

        $sql
            = "
            SELECT
                M.NO_MTP_ATI,
                A.NO_TP_ATI,
                A.VR_RET_TP_ATI AS VR_RET_TP_ATI,
                A.PC_RET_TP_ATI,
                A.PC_RET_TP_ATI / P.PC_RET_PRD * 100 PC_RET_TP_ATI_BASE_100
            FROM
                SPN_PERIODO_PRODUTO_TIPO_ATIVO A
            INNER JOIN
                ATIVO_TIPO T
            ON
                A.CO_TP_ATI = T.CO_TP_ATI
            INNER JOIN
                SIRAT.DBO.ATIVO_TIPO_MACROTIPO M
            ON
                T.CO_MTP_ATI = M.CO_MTP_ATI
            INNER JOIN
                SPN_PERIODO_PRODUTO P
            ON
                A.CO_PRD = P.CO_PRD AND
                A.DT_INI = P.DT_INI AND
                A.DT_FIM = P.DT_FIM
            WHERE
                A.CO_PRD = '$cnpj' AND
                A.DT_FIM = '$dataReferencia'
            ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $desempenho = new DesempenhoTipoAtivoDiario();
            $desempenho->create($row);
            $this->append($desempenho);
        }

        $sqlDespesa
            = "
            SELECT
                'Despesas' AS NO_MTP_ATI,
                'Despesas' AS NO_TP_ATI,
                VR_RET_DESP AS VR_RET_TP_ATI,
                PC_RET_DESP AS PC_RET_TP_ATI,
                PC_RET_DESP / PC_RET_PRD * 100 PC_RET_TP_ATI_BASE_100
            FROM
                SPN_PERIODO_PRODUTO
            WHERE
                CO_PRD = '$cnpj' AND
                DT_FIM = '$dataReferencia'
            ";

        $despesa = $conn->fetchAssoc($sqlDespesa, array());

        $desempenho = new DesempenhoTipoAtivoDiario();
        $desempenho->create($despesa);
        $this->append($desempenho);

        return $this;
    }

    /**
    * Monta o gráfico Highchart
    *
    * @access  public
    *
    * @return  Highchart $chart O gráfico highchart que será renderizado
    */
    public function getDesempenhoTipoAtivoChart($serieDados = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();
        $chart->includeExtraScripts();

        $chart->chart = array(
            'renderTo' => $render_div,
            'type'     => 'waterfall'
        );
        $chart->title->text = 'Contribuição de Rentabilidade por Ativo';

        $chart->subtitle->text = 'Rentabilidade Total do Fundo no dia: ' . number_format($this->getRentabilidadeTotal(),2,",",".") . '%';

        $chart->xAxis->type = 'category';

        $chart->legend->enabled = false;

        $chart->tooltip->pointFormat =
            "R$ <b>{point.valor}</b> : <b>{point.y:,.2f}</b>%";

        $somaSerie = array(
                    'name' => 'Total',
                    'isIntermediateSum' => true,
                    'color' => new HighchartJsExpr(
                        'Highcharts.getOptions().colors[1]'
                    )
                );

        $serieDados[] = $somaSerie;

        $chart->xAxis->labels->rotation = 25;
        $chart->xAxis->labels->style->font = "normal 12px Verdana, sans-serif";

        $chart->yAxis->title->text = "Rentabilidade %";

        $chart->series = array(
            array(
                'upColor' => new HighchartJsExpr(
                    'Highcharts.getOptions().colors[2]'
                    ),
                'color' => new HighchartJsExpr(
                    'Highcharts.getOptions().colors[3]'
                    ),
                'data' => $serieDados,
                'dataLabels' => array(
                    'enabled' => false,
                    'inside'  => false,
                    'zIndex'  => 6,
                    'formatter' => new HighchartJsExpr(
                        "function () {
                            return Highcharts.numberFormat(this.y, 2, ',') + '%';
                        }"
                    ),
                    'style' => array(
                        'color'      => '#274b6d',
                        //'color'      => '#FFFFFF',
                        //'color'      => '#4F4F4F',
                        'fontWeight' => 'bold',
                        //'textShadow' => '0px 0px 3px black'
                    )
                ),
                'pointPadding' => 0
            )
        );

        return $chart;
    }

    /**
     * DesempenhoTipoAtivoDiarioGrupo::preencheRentabilidadeTotal
     *
     */
    public function preencheRentabilidadeTotal()
    {
        foreach ($this as $desempenho) {
            $this->rentabilidadeTotal = $this->rentabilidadeTotal + $desempenho->getPercentual();
        }
    }

    /**
     * DesempenhoTipoAtivoDiarioGrupo::preencheResultadoTotal
     *
     */
    public function preencheResultadoTotal()
    {
        foreach ($this as $desempenho) {
            $this->valorTotal = $this->valorTotal + $desempenho->getValor();
        }
    }

    /**
     * Gets the O resultado financeiro total.
     *
     * @return float
     */
    public function getResultado()
    {
        return $this->resultado;
    }

    /**
     * Sets the O resultado financeiro total.
     *
     * @param float $resultado the resultado
     *
     * @return self
     */
    public function setResultado($resultado)
    {
        $this->resultado = $resultado;

        return $this;
    }

    /**
     * Gets the Rentabilidade total do fundo.
     *
     * @return float
     */
    public function getRentabilidadeTotal()
    {
        return $this->rentabilidadeTotal;
    }

    /**
     * Sets the Rentabilidade total do fundo.
     *
     * @param float $rentabilidadeTotal the rentabilidade total
     *
     * @return self
     */
    public function setRentabilidadeTotal($rentabilidadeTotal)
    {
        $this->rentabilidadeTotal = $rentabilidadeTotal;

        return $this;
    }

    /**
     * Gets the O valor das despesas do fundo.
     *
     * @return float
     */
    public function getDespesa()
    {
        return $this->despesa;
    }

    /**
     * Sets the O valor das despesas do fundo.
     *
     * @param float $despesa the despesa
     *
     * @return self
     */
    public function setDespesa($despesa)
    {
        $this->despesa = $despesa;

        return $this;
    }

    /**
     * Gets the O valor total.
     *
     * @return float
     */
    public function getValorTotal()
    {
        return $this->valorTotal;
    }

    /**
     * Sets the O valor total.
     *
     * @param float $valorTotal the valor total
     *
     * @return self
     */
    public function setValorTotal($valorTotal)
    {
        $this->valorTotal = $valorTotal;

        return $this;
    }
}
