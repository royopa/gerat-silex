<?php
/**
* ItemRiscoMercadoGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Highcharts\Highchart;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartJsExpr;

/**
 * ItemRiscoMercadoGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class ItemRiscoMercadoGrupo extends \ArrayIterator
{
    /**
     * @var FundoDiario
     * O fundo diário do ativo
     */
    private $fundoDiario;

    /**
     * @var RiscoMercado
     * Dados de risco de mercado
     */
    private $riscoMercado;

    /**
     * @var float
     * A soma total dos valores MtM líquidos
     */
    private $valorMtmTotal;

    /**
     * @var float
     * A soma total dos valores CVaR
     */
    private $valorCvarTotal;

    /**
     * @var float
     * A soma total dos percentuais em relação ao PL dos valores alocados
     */
    private $percentualTotal;

    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    public function __construct(FundoDiario $fundoDiario = null, RiscoMercado $riscoMercado= null, $conn = null, DesempenhoTipoAtivoGrupo $desempenhoTipoAtivoGrupo = null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            $this->fundoDiario     = $fundoDiario;
            $this->riscoMercado    = $riscoMercado;
            $this->conn            = $conn;
            $this->desempenhoTipoAtivoGrupo = $desempenhoTipoAtivoGrupo;

            $this->valorEmGarantia = $fundoDiario->getValorEmGarantia();
            $this->fetchAll($fundoDiario, $desempenhoTipoAtivoGrupo);
            $this->preencheTotalizadores();
        }
    }

    /**
     * Gets the O fundo diário do ativo.
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * Sets the O fundo diário do ativo.
     *
     * @param FundoDiario $fundoDiario the fundo diario
     *
     * @return self
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;

        return $this;
    }

    /**
     * Gets the A soma total dos valores MtM líquidos.
     *
     * @return float
     */
    public function getValorMtmTotal()
    {
        return $this->valorMtmTotal;
    }

    /**
     * Sets the A soma total dos valores MtM líquidos.
     *
     * @param float $valorMtmTotal the valor mtm total
     *
     * @return self
     */
    public function setValorMtmTotal($valorMtmTotal)
    {
        $this->valorMtmTotal = $valorMtmTotal;

        return $this;
    }

    /**
     * Gets the A soma total dos valores CVaR.
     *
     * @return float
     */
    public function getValorCvarTotal()
    {
        return $this->valorCvarTotal;
    }

    /**
     * Sets the A soma total dos valores CVaR.
     *
     * @param float $valorCvarTotal the valor cvar total
     *
     * @return self
     */
    public function setValorCvarTotal($valorCvarTotal)
    {
        $this->valorCvarTotal = $valorCvarTotal;

        return $this;
    }

    /**
     * Gets the A soma total dos percentuais em relação ao PL dos valores alocados.
     *
     * @return float
     */
    public function getPercentualTotal()
    {
        return $this->percentualTotal;
    }

    /**
     * Sets the A soma total dos percentuais em relação ao PL dos valores alocados.
     *
     * @param float $percentualTotal the percentual total
     *
     * @return self
     */
    public function setPercentualTotal($percentualTotal)
    {
        $this->percentualTotal = $percentualTotal;

        return $this;
    }

    /**
     * ItemRiscoMercadoGrupo::fetchAll()
     *
    * @param FundoDia $fundoDia O objeto fundo diário
     *
     * @return ItemRiscoMercadoGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null, DesempenhoTipoAtivoGrupo $desempenhoTipoAtivoGrupo)
    {
        if ($fundoDiario instanceof FundoDiario) {

            //instancia o model Dbal para fazer acesso ao banco de dados
            $dbal = new Dbal();
            $conn = $dbal->getConn();

            $sql
                = "
                    SELECT
                        O.NO_GR_ATIVO,
                        SUM(O.VR_MTM) VR_MTM,
                        SUM(O.VR_CVAR) VR_CVAR,
                        SUM(O.VR_VAR_S) VR_VAR_S,
                        T.CO_TP_ATI,
                        AT.NO_TP_ATI
                    FROM
                        [CR245002_RM_F_16.A] O
                    INNER JOIN
                        Produto_Dia_Expo_Ativo_x_Fator_Ativo A
                    ON
                        O.CO_ATIVO=A.CO_ATIVO
                    INNER JOIN
                        Produto_Dia_Expo_Ativo_x_Fator_Tipo_Ativo T
                    ON
                        T.CO_ATIVO=A.CO_ATIVO
                    LEFT JOIN
                        Ativo_Tipo AT
                    ON
                        AT.CO_TP_ATI=T.CO_TP_ATI
                    LEFT JOIN
                        Ativo_Tipo_Macrotipo MT
                    ON
                        AT.CO_MTP_ATI=MT.CO_MTP_ATI
                    WHERE
                        O.CO_PRD = :cnpj AND
                        O.DT_ATU = :dataAtualizacao
                    GROUP BY
                        O.NO_GR_ATIVO,
                        T.CO_TP_ATI,
                        AT.NO_TP_ATI
                    ORDER BY
                        O.NO_GR_ATIVO
                  ";

            $sql
                = "
                    SELECT
                        CO_TP_ATI,
                        NO_TP_ATI,
                        NO_GR_ATIVO,
                        VR_MTM,
                        VR_CVAR,
                        NO_MAIOR_FATOR
                    FROM
                        [dbo].[WEB_RISCO_MERCADO]
                    WHERE
                        CO_PRD = :cnpj AND
                        DT_ATU = :dataAtualizacao
                    ORDER BY
                        NO_GR_ATIVO
                  ";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue(
                'cnpj',
                $fundoDiario->getFundo()->getCnpj(),
                'integer'
                );
            $stmt->bindValue(
                'dataAtualizacao',
                $fundoDiario->getDataAtualizacao(),
                'datetime'
                );
            $stmt->execute();

            while ($row = $stmt->fetch()) {

                //$fatorRisco = new FatorRisco($fundoDiario, $conn);
                //$row['NO_FATOR'] = $fatorRisco->getMaiorFator($fundoDiario, $row['NO_GR_ATIVO']);
                $row['NO_FATOR'] = $row['NO_MAIOR_FATOR'];

                $itemRiscoMercado = new ItemRiscoMercado();
                //adiciona o ativo de crédito no objeto grupo
                $this->append($itemRiscoMercado->create(
                    $fundoDiario,
                    $row,
                    $this->getRiscoMercado(),
                    $conn,
                    $desempenhoTipoAtivoGrupo
                    )
                );
            }
        }

        return $this;
    }

    /**
     * ItemRiscoMercadoGrupo::preencheTotalizadores()
     *
     * @return ItemRiscoMercadoGrupo
     *
     * Preenche os totalizadores de valor e percentual total de alocação
     */
    public function preencheTotalizadores()
    {
        foreach ($this as $itemRiscoMercado) {

            $this->valorCvarTotal
                = $this->valorCvarTotal + $itemRiscoMercado->getValorCvar();

            $this->valorMtmTotal
                = $this->valorMtmTotal + $itemRiscoMercado->getValorMtm();

            $this->percentualTotal
                = ($this->valorCvarTotal / $this->getFundoDiario()->getPatrimonioLiquido()) * 100;
        }

        return $this;
    }

    /**
     * Gets the O gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        $grafico = new Grafico();

        $this->grafico =
            $this->getItemRiscoMercadoGrupoChart(
                    $this->montaSerieDados(),
                    'chart_risco_mercado',
                    null,
                    'Relação Risco x Retorno'
                );

        return $this->grafico;
    }

    /**
    * Monta um array com a série de dados
    *
    * @return  mixed[] $dados_serie Um array com os arrays separados por séries
    */
    private function montaSerieDados()
    {
        $dadosSerie        = array();
        $categorias        = array();
        $serieCvar         = array();
        $serieMtm          = array();
        $serieRetorno      = array();

        foreach ($this as $itemRiscoMercado) {

            //só faz o gráfico para os ativos que não sejam caixa
            if ($itemRiscoMercado->getNome() != 'Caixa') {

                $categorias[] = $itemRiscoMercado->getNome();

                $serieCvar[]
                    = $itemRiscoMercado->getPercentualCvar();

                $serieMtm[]
                    = $itemRiscoMercado->getPercentualMtm();

                $serieRetorno[]
                    = $itemRiscoMercado->getPercentualRetorno();
            }
        }

        $dadosSerie['categorias']   = $categorias;
        $dadosSerie['serieCvar']    = $serieCvar;
        $dadosSerie['serieMtm']     = $serieMtm;
        $dadosSerie['serieRetorno'] = $serieRetorno;

        return $dadosSerie;
    }

    /**
     * Sets the O gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }

    /**
     * Gets the Dados de risco de mercado.
     *
     * @return RiscoMercado
     */
    public function getRiscoMercado()
    {
        return $this->riscoMercado;
    }

    /**
     * Sets the Dados de risco de mercado.
     *
     * @param RiscoMercado $riscoMercado the risco mercado
     *
     * @return self
     */
    public function setRiscoMercado(RiscoMercado $riscoMercado)
    {
        $this->riscoMercado = $riscoMercado;

        return $this;
    }

    /**
    * Monta o gráfico Highchart de pizza com legendas da série recebida
    *
    * @param mixed[] $dados_serie Dados da série para a geração do gráfico
    * @param string  $render_div  ID da div onde o gráfico será mostrado
    * @param string  $name        Nome que será mostrado no gráfico
    * @param string  $title       Titulo que será mostrado no gráfico
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getItemRiscoMercadoGrupoChart($dadosSerie = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();

        $chart->chart->renderTo = $render_div;
        $chart->chart->type = "column";
        $chart->title->text = $title;

        $chart->xAxis->categories = $dadosSerie['categorias'];

        //$chart->yAxis->min = 0;
        $chart->yAxis->title->text = "";

        $chart->legend->layout = "vertical";
        $chart->legend->backgroundColor = "#FFFFFF";
        $chart->legend->align = "left";
        $chart->legend->verticalAlign = "top";
        $chart->legend->x = 60;
        $chart->legend->y = 35;
        $chart->legend->floating = 1;
        $chart->legend->shadow = 1;

        $chart->xAxis->labels->style->font = "normal 12px Verdana, sans-serif";

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function () {
                return '' + this.x +' : ' + this.series.name + ' ' + Highcharts.numberFormat(this.y,2,',','.') + '%';
            }"
            );

        $chart->plotOptions->column->pointPadding = 0.2;
        $chart->plotOptions->column->borderWidth = 0;

        $chart->series[] = array(
            'name' => "cVar",
            'data' => $dadosSerie['serieCvar']
        );

        $chart->series[] = array(
            'name' => "MtM",
            'data' => $dadosSerie['serieMtm']
        );

        $chart->series[] = array(
            'name' => "Retorno",
            'data' => $dadosSerie['serieRetorno']
        );

        return $chart;
    }
}
