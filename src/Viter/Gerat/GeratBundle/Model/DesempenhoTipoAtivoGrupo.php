<?php
/**
* DesempenhoTipoAtivoGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * DesempenhoTipoAtivoGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Highcharts\Highchart;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartJsExpr;
use Viter\Gerat\GeratBundle\Model\ProdutoDia;

class DesempenhoTipoAtivoGrupo extends \ArrayIterator
{
    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * @var float
     * O resultado financeiro total
     */
    private $resultado;

    /**
     * @var float
     * Rentabilidade total do fundo
     */
    private $rentabilidadeTotal;

    /**
     * @var float
     * O valor das despesas do fundo
     */
    private $despesa;

    /**
     * Gets the gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $desempenho;
    }

    public function __construct(FundoDiario $fundoDiario = null, $conn, $dataInicio, $dataFim)
    {
        $this->conn = $conn;
        $this->fetchAll($fundoDiario, $dataInicio, $dataFim);
        $this->preencheRentabilidadeTotal();
        $this->preencheResultadoTotal();
        $this->montaGrafico();
    }

    /**
    * Monta um array com a série da composição da carteira de crédito que
    * será usada para montagem do gráfico de coluna da carteira de crédito
    *
    * @return  mixed[] $dados_serie Um array com os arrays que serão usados
    */
    public function montaSerieDados()
    {
        $dadosSerie        = array();
        $datas         = array();

        foreach ($this as $desempenhoTipoAtivo) {

        $desempenhoArray = array();
        $desempenhoArray['name']      = $desempenhoTipoAtivo->getNome();
        $desempenhoArray['y']         = $desempenhoTipoAtivo->getPercentual();
        $desempenhoArray['valor']     = number_format($desempenhoTipoAtivo->getValor(),2,",",".");

        $dadosSerie[] = $desempenhoArray;
        }

        return $dadosSerie;
    }

    /**
     * DesempenhoTipoAtivoGrupo::montaGrafico()
     *
     * @return DesempenhoTipoAtivoGrupo
     *
     * Monta o gráfico de colunas dos retornos fundo/benchmark
     */
    public function montaGrafico()
    {
        $grafico = new Grafico();

        $this->grafico =
        $this->getDesempenhoTipoAtivoChart(
                $this->montaSerieDados(),
                'chart_desempenho_tipo_ativo',
                null,
                'Desempenho por tipo de ativo'
            );

        return $this;
    }

    public function fetchAll(FundoDiario $fundoDiario = null, $dataInicio, $dataFim)
    {
        /*
        Ao definir o período, colocar
        @dt_ini = último dia útil do mês anterior
        @dt_fim = último dia útil do mês - 2
        */

        $dbal = new Dbal();
        $conn = $dbal->getConnRisco();

        $cnpj = $fundoDiario->getFundo()->getCnpj();

        if (($dataInicio instanceof \DateTime) && ($dataFim instanceof \DateTime)) {

        } else {
            $produtoDia = new ProdutoDia($conn);
            //Data Fim = último dia do mês anterior
            $dataFim = $produtoDia->getMaxDataReferenciaMesAnterior($fundoDiario->getDataReferencia());
            //Data Início = último dia de mês anterior ao mês anterior (m-2)
            $dataInicio = $produtoDia->getMaxDataReferenciaMesAnterior($dataFim);
        }

        $dataInicio = $dataInicio->format('Y-m-d');
        $dataFim = $dataFim->format('Y-m-d');

        $sql
            = "
            SELECT
                M.NO_MTP_ATI,
                T.NO_TP_ATI,
                A.VR_RET_TP_ATI AS VR_RET_TP_ATI,
                A.PC_RET_TP_ATI,
                A.PC_RET_TP_ATI / P.PC_RET_PRD * 100 PC_RET_TP_ATI_BASE_100
            FROM
                SPN_PERIODO_PRODUTO_TIPO_ATIVO A
            INNER JOIN
                ATIVO_TIPO T
            ON
                A.CO_TP_ATI = T.CO_TP_ATI
            INNER JOIN
                SIRAT.DBO.ATIVO_TIPO_MACROTIPO M
            ON
                T.CO_MTP_ATI = M.CO_MTP_ATI
            INNER JOIN
                SPN_PERIODO_PRODUTO P
            ON
                A.CO_PRD = P.CO_PRD AND
                A.DT_INI = P.DT_INI AND
                A.DT_FIM = P.DT_FIM
            WHERE
                A.CO_PRD = '$cnpj' AND
                A.DT_INI = '$dataInicio' AND
                A.DT_FIM = '$dataFim'
            ";

        $sql
            = "
            SELECT
                M.NO_MTP_ATI,
                T.NO_TP_ATI,
                A.VR_RET_TP_ATI,
                A.PC_RET_TP_ATI
            FROM
                spn_PERIODO_PRODUTO_TIPO_ATIVO A
            INNER JOIN
                SIRAT.DBO.PRODUTO X ON A.CO_PRD = X.CO_PRD
            INNER JOIN
                ATIVO_TIPO T ON A.CO_TP_ATI = T.CO_TP_ATI
            INNER JOIN
                ATIVO_TIPO_MACROTIPO M
            ON
                T.CO_MTP_ATI = M.CO_MTP_ATI
            INNER JOIN
                spn_PERIODO_PRODUTO P
            ON
                A.CO_PRD = P.CO_PRD AND
                A.DT_INI = P.DT_INI AND
                A.DT_FIM = P.DT_FIM
            WHERE
                (A.CO_PRD = '$cnpj' OR  '$cnpj' IS NULL) AND
                A.DT_INI = '$dataInicio' AND
                A.DT_FIM = '$dataFim'
            ";

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $desempenho = new DesempenhoTipoAtivo();
            $desempenho->create($row);
            $this->append($desempenho);
        }

        $sqlDespesa
            = "
            SELECT
                'Despesas' AS NO_MTP_ATI,
                'Despesas' AS NO_TP_ATI,
                VR_RET_DESP AS VR_RET_TP_ATI,
                PC_RET_DESP AS PC_RET_TP_ATI,
                PC_RET_DESP / PC_RET_PRD * 100 PC_RET_TP_ATI_BASE_100
            FROM
                SPN_PERIODO_PRODUTO
            WHERE
                co_prd = '$cnpj' AND
                dt_ini = '$dataInicio' AND
                dt_fim = '$dataFim'
            ";

        $despesa = $conn->fetchAssoc(
            $sqlDespesa,
            array($cnpj, $dataInicio, $dataFim));

        $desempenho = new DesempenhoTipoAtivo();
        $desempenho->create($despesa);
        $this->append($desempenho);

        return $this;
    }

    /**
     * DesempenhoTipoAtivoGrupo::getMinDataReferenciaMesAnterior(FundoDiario $fundoDiario)
     *
     * @param $dataAtualizacao DateTime
     *
     */
    public function getMinDataReferenciaMesAnterior(FundoDiario $fundoDiario = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConnRisco();

        $sql
        = "
        SELECT
            MIN(DT_REF) AS DT_REF
        FROM
            spn_Produto_Dia
        WHERE
            CO_PRD = ? AND
            MONTH(DT_REF) = ? AND
            YEAR(DT_REF) = ?
        ";

        $minDataReferenciaMesAnterior = new \DateTime($fundoDiario->getDataAtualizacao()->format('Y-m-d'));
        $minDataReferenciaMesAnterior->sub(new \DateInterval('P1M3D'));
        $mes = (int) $minDataReferenciaMesAnterior->format('m');
        $ano = (int) $minDataReferenciaMesAnterior->format('Y');

        $data
        = $conn->fetchAssoc(
            $sql,
            array(
                $fundoDiario->getFundo()->getCnpj(),
                $mes,
                $ano)
            );

        return new \DateTime($data['DT_REF']);
    }

    /**
     * DesempenhoTipoAtivoGrupo::getMinDataReferenciaMesAtual(FundoDiario $fundoDiario)
     *
     * @param $dataAtualizacao DateTime
     *
     */
    public function getMinDataReferenciaMesAtual(FundoDiario $fundoDiario = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConnRisco();

        $sql
        = "
        SELECT
            MIN(DT_REF) AS DT_REF
        FROM
            spn_Produto_Dia
        WHERE
            CO_PRD = ? AND
            MONTH(DT_REF) = ? AND
            YEAR(DT_REF) = ?
        ";

        $mes = (int) $fundoDiario->getDataAtualizacao()->format('m');
        $ano = (int) $fundoDiario->getDataAtualizacao()->format('Y');

        $data
        = $conn->fetchAssoc(
            $sql,
            array(
                $fundoDiario->getFundo()->getCnpj(),
                $mes,
                $ano)
            );

        return new \DateTime($data['DT_REF']);
    }

    /**
     * DesempenhoTipoAtivoGrupo::preencheRentabilidadeTotal
     *
     */
    public function preencheRentabilidadeTotal()
    {
        foreach ($this as $desempenho) {
        $this->rentabilidadeTotal = $this->rentabilidadeTotal + $desempenho->getPercentual();
        }
    }

    /**
     * DesempenhoTipoAtivoDiarioGrupo::preencheResultadoTotal
     *
     */
    public function preencheResultadoTotal()
    {
        foreach ($this as $desempenho) {
            $this->resultado = $this->resultado + $desempenho->getValor();
        }
    }

    /**
    * Monta o gráfico Highchart
    *
    * @access  public
    *
    * @return  Highchart $chart O gráfico highchart que será renderizado
    */
    public function getDesempenhoTipoAtivoChart($serieDados = null, $render_div = '', $name = '', $title = '')
    {
        $chart = new Highchart();
        $chart->includeExtraScripts();

        $chart->chart = array(
        'renderTo' => 'chart_desempenho_tipo_ativo',
        'type'     => 'waterfall'
        );
        $chart->title->text = 'Contribuição de Rentabilidade por Ativo';

        $chart->subtitle->text = 'Rentabilidade Total do Fundo: ' . number_format($this->getRentabilidadeTotal(),2,",",".") . '%';

        $chart->xAxis->type = 'category';

        $chart->legend->enabled = false;

        $chart->tooltip->pointFormat =
        "R$ <b>{point.valor}</b> : <b>{point.y:,.2f}</b>%";

        $somaSerie = array(
                'name' => 'Total',
                'isIntermediateSum' => true,
                'color' => new HighchartJsExpr(
                'Highcharts.getOptions().colors[1]'
                )
            );

        $serieDados[] = $somaSerie;

        $chart->xAxis->labels->rotation = 25;
        $chart->xAxis->labels->style->font = "normal 12px Verdana, sans-serif";

        $chart->yAxis->title->text = "Rentabilidade %";

        $chart->series = array(
        array(
            'upColor' => new HighchartJsExpr(
                'Highcharts.getOptions().colors[2]'
                ),
            'color' => new HighchartJsExpr(
                'Highcharts.getOptions().colors[3]'
                ),
            'data' => $serieDados,
            'dataLabels' => array(
                'enabled' => false,
                'inside'  => false,
                'zIndex'  => 6,
                'formatter' => new HighchartJsExpr(
                "function () {
                    return Highcharts.numberFormat(this.y, 2, ',') + '%';
                }"
                ),
                'style' => array(
                'color'      => '#274b6d',
                //'color'      => '#FFFFFF',
                //'color'      => '#4F4F4F',
                'fontWeight' => 'bold',
                //'textShadow' => '0px 0px 3px black'
                )
            ),
            'pointPadding' => 0
        )
        );

        return $chart;
    }

    /**
     * Gets the O resultado financeiro total.
     *
     * @return float
     */
    public function getResultado()
    {
        return $this->resultado;
    }

    /**
     * Sets the O resultado financeiro total.
     *
     * @param float $resultado the resultado
     *
     * @return self
     */
    public function setResultado($resultado)
    {
        $this->resultado = $resultado;

        return $this;
    }

    /**
     * Gets the Rentabilidade total do fundo.
     *
     * @return float
     */
    public function getRentabilidadeTotal()
    {
        return $this->rentabilidadeTotal;
    }

    /**
     * Sets the Rentabilidade total do fundo.
     *
     * @param float $rentabilidadeTotal the rentabilidade total
     *
     * @return self
     */
    public function setRentabilidadeTotal($rentabilidadeTotal)
    {
        $this->rentabilidadeTotal = $rentabilidadeTotal;

        return $this;
    }

    /**
     * Gets the O valor das despesas do fundo.
     *
     * @return float
     */
    public function getDespesa()
    {
        return $this->despesa;
    }

    /**
     * Sets the O valor das despesas do fundo.
     *
     * @param float $despesa the despesa
     *
     * @return self
     */
    public function setDespesa($despesa)
    {
        $this->despesa = $despesa;

        return $this;
    }
}
