<?php
/**
* DesempenhoTipoAtivoDiario File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * DesempenhoTipoAtivoDiario Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class DesempenhoTipoAtivoDiario extends DesempenhoTipoAtivo
{
    /**
     * @var float
     * O percentual de contribuicao do ativo dentro do fundo
     */
    private $percentualContribuicao;

    /**
     * Gets the O percentual de contribuicao do ativo dentro do fundo.
     *
     * @return float
     */
    public function getPercentualContribuicao()
    {
        return $this->percentualContribuicao;
    }

    /**
     * Sets the O percentual de contribuicao do ativo dentro do fundo.
     *
     * @param float $percentualContribuicao the percentual contribuicao
     *
     * @return self
     */
    public function setPercentualContribuicao($percentualContribuicao)
    {
        $this->percentualContribuicao = $percentualContribuicao;

        return $this;
    }

    /**
     * DesempenhoTipoAtivoDiario::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return DesempenhoTipoAtivoDiario
     */
    public function create($row)
    {
        $this->setTipo(utf8_encode($row['NO_MTP_ATI']));
        if ($row['NO_TP_ATI'] == utf8_decode('Compromiss. Público')) {
            $this->setTipo('Títulos Públicos');
        }
        $this->setNome(utf8_encode($row['NO_TP_ATI']));
        $this->setValor((float) $row['VR_RET_TP_ATI']);
        $this->setPercentual((float) $row['PC_RET_TP_ATI']);

        //se for diario
        $this->setPercentualContribuicao((float) $row['PC_RET_TP_ATI_BASE_100']);

        return $this;
    }
}
