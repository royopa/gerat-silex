<?php
/**
* Cotista File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * Cotista Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class Cotista
{
    /**
     * @var int
     * CNPJ do cotista
     */
    private $cnpj;

    /**
     * @var string
     * O nome dos ativos
     */
    private $nome;

    /**
     * @var float
     * O valor total investido nesse nome de ativo
     */
    private $valor;

    /**
     * @var float
     * O percentual sobre o patrimônio líquido
     */
    private $percentual;

    /**
     * @var int
     * Quantidade de cotistas
     */
    private $quantidade;

    /**
     * @var int
     * Quantidade de cotistas no último dia do mês anterior
     */
    private $quantidadeMesAnterior;

    public function __construct($conn)
    {
        $dbal = new Dbal();
        $this->conn = $dbal->getConn();
    }

    /**
     * Gets the nome dos ativos.
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Sets the nome dos ativos.
     *
     * @param string $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Gets the O valor total investido nesse nome de ativo.
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Sets the O valor total investido nesse nome de ativo.
     *
     * @param float $valor the valor
     *
     * @return self
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * Gets the O percentual sobre o patrimônio líquido.
     *
     * @return float
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * Sets the O percentual sobre o patrimônio líquido.
     *
     * @param float $percentual the percentual
     *
     * @return self
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;

        return $this;
    }

    /**
     * Cotista::create()
     *
     * @param mixed[] O array com os dados do objeto
     *
     * @return Cotista
     */
    public function create(FundoDiario $fundoDiario, $row, \DateTime $maxData)
    {
        if ($fundoDiario instanceof FundoDiario) {
            $this->fundoDiario = $fundoDiario;
        }

        if (count($row) > 0) {
            $this->setQuantidade($row['NU_COTIS']);
            $this->setCnpj($row['CO_PRD']);
            $this->setNome(utf8_encode($row['NO_PRD']));
            $this->setValor((float) abs($row['VR_COTA_FF']));
            $this->setPercentual(
                (float) ($this->getValor() / $fundoDiario->getPatrimonioLiquido() * 100)
            );

            $this->fetchQuantidadeCotistasMesAnterior($maxData);
        }

        return $this;
    }

    /**
     * Cotista::preencheQuantidadeCotistasMesAnterior()
     *
     * @param \DateTime $maxData O último dia do mês anterior
     *
     * @return Cotista
     *
     */
    public function fetchQuantidadeCotistasMesAnterior(\DateTime $maxData)
    {
        $dataReferencia = $maxData->format('Y-m-d');
        $cnpj = $this->getCnpj();

        $sql
            = "
            SELECT
                DT_REF,
                CO_FDO,
                SUM(NU_COTIS) NU_COTIS
            FROM
                CR245001_RL_23
            WHERE
                DT_REF = '$dataReferencia' AND
                CO_FDO = '$cnpj'
            GROUP BY
                DT_REF,
                CO_FDO
            ";

        $stmt = $this->conn->prepare($sql);

        $stmt->execute();

        while ($row = $stmt->fetch()) {
            //adiciona a quantidade de cotistas do mês anterior
            $this->setQuantidadeMesAnterior($row['NU_COTIS']);
        }

        return $this;
    }

    /**
     * Gets the CNPJ do cotista.
     *
     * @return int
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Sets the CNPJ do cotista.
     *
     * @param int $cnpj the cnpj
     *
     * @return self
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Gets the Quantidade de cotistas.
     *
     * @return int
     */
    public function getQuantidade()
    {
        return $this->quantidade;
    }

    /**
     * Sets the Quantidade de cotistas.
     *
     * @param int $quantidade the quantidade
     *
     * @return self
     */
    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;

        return $this;
    }

    /**
     * Gets the Quantidade de cotistas no último dia do mês anterior.
     *
     * @return int
     */
    public function getQuantidadeMesAnterior()
    {
        return $this->quantidadeMesAnterior;
    }

    /**
     * Sets the Quantidade de cotistas no último dia do mês anterior.
     *
     * @param int $quantidadeMesAnterior the quantidade mes anterior
     *
     * @return self
     */
    public function setQuantidadeMesAnterior($quantidadeMesAnterior)
    {
        $this->quantidadeMesAnterior = $quantidadeMesAnterior;

        return $this;
    }
}
