<?php
/**
* LetraFinanceiraGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* LF, Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * LetraFinanceiraGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */
class LetraFinanceiraGrupo extends AtivoCreditoGrupo
{
    public function __construct(FundoDiario $fundoDiario = null, $conn)
    {
        $this->conn = $conn;
        $this->fundoDiario = $fundoDiario;
        $this->setNome('LF');
        parent::fetchAll($fundoDiario);
    }
}
