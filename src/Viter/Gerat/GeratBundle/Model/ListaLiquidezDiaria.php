<?php
/**
* ListaLiquidezDiaria File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * ListaLiquidezDiaria Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\LiquidezDiaria;

class ListaLiquidezDiaria extends \ArrayIterator
{
    /**
     * @var \DateTime
     * Data do arquivo XML processado na rotina D-1
     */
    private $dataReferencia;

    /**
     * @var \DateTime
     * Data de processamento da rotina
     */
    private $dataAtualizacao;

    public function __construct(\DateTime $dataAtualizacao = null, $conn = null)
    {
        $this->conn = $conn;
        $this->dataAtualizacao = $dataAtualizacao;
        $this->dataReferencia  = $this->fetchDataReferencia();
        $this->fetchAll();
    }

    /**
     * Gets the Data do arquivo XML processado na rotina D-1.
     *
     * @return \DateTime
     */
    public function getDataReferencia()
    {
        return $this->dataReferencia;
    }

    /**
     * Sets the Data do arquivo XML processado na rotina D-1.
     *
     * @param \DateTime $dataReferencia the data referencia
     *
     * @return self
     */
    public function setDataReferencia(\DateTime $dataReferencia)
    {
        $this->dataReferencia = $dataReferencia;

        return $this;
    }

    /**
     * Gets the Data de processamento da rotina.
     *
     * @return \DateTime
     */
    public function getDataAtualizacao()
    {
        return $this->dataAtualizacao;
    }

    /**
     * Sets the Data de processamento da rotina.
     *
     * @param \DateTime $dataAtualizacao the data atualizacao
     *
     * @return self
     */
    public function setDataAtualizacao(\DateTime $dataAtualizacao)
    {
        $this->dataAtualizacao = $dataAtualizacao;

        return $this;
    }

    /**
     * ListaLiquidezDiaria::fetchAll()
     * Faz a busca no banco de dados e preenche o objeto
     *
     * @return FundoDiario
     */
    public function fetchAll()
    {
        //pega a data de referencia d-2
        //SELECT TOP 1 DT_REF FROM PRODUTO_DIA WHERE DT_REF < '2013-12-12' ORDER BY DT_REF DESC
        $produtoDia = new ProdutoDia($this->conn);
        $dataReferenciaAnterior = $produtoDia->getDataReferenciaAnterior($this->getDataReferencia());
        $dataReferenciaAnterior = $dataReferenciaAnterior->format('Y-m-d');

        $sql
            = "
            SELECT
                p.ID,
                p.DT_REF,
                p.CO_PRD,
                p.NO_PRD,
                p.LIQ,
                p.EMISSOR,
                p.VR_PROV_C,
                p.VR_PROV_D,
                c.LIQ AS LIQ_CONF,
                pd.LIQ AS LIQD,
                pd.DT_REF AS DT_REFD
            FROM
                Perfil_CVM_Liq p
            LEFT JOIN
                Perfil_CVM_Liq_Conferencia c
            ON
                p.DT_REF = c.DT_REF AND
                p.CO_PRD = c.CO_PRD
            LEFT JOIN
                (SELECT
                    pd.DT_REF,
                    pd.CO_PRD,
                    pd.LIQ
                FROM
                    Perfil_CVM_Liq pd
                WHERE
                    pd.DT_REF = '$dataReferenciaAnterior') pd
            ON
                p.CO_PRD = pd.CO_PRD
            WHERE
                p.DT_REF = :dataReferencia
            ";

            $stmt = $this->conn->prepare($sql);

            $stmt->bindValue(
                'dataReferencia',
                $this->getDataReferencia(),
                'datetime'
                );

            $stmt->execute();

            while ($row = $stmt->fetch()) {
                $liquidezDiaria = new LiquidezDiaria();
                $liquidezDiaria->create($row, $this->dataAtualizacao, $this->conn);
                $this->append($liquidezDiaria);
            }

        return $this;
    }

    public function fetchDataReferencia()
    {
        $data = $this->conn->fetchAssoc(
            'SELECT DISTINCT
                MAX(dt_ref) AS dt_ref
            FROM
                produto_dia
            WHERE
                dt_atu = ?', array($this->getDataAtualizacao()->format('Y-m-d')));

        return new \DateTime($data['dt_ref']);
    }
}
