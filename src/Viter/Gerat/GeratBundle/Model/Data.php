<?php
/**
* Data File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
* LF, Letra Hipotecária, CCB, Operacao Compromissada
* LF SUB, LH, CDB SUB - Subordinada, Debêntures, CRI, CDB, CCI
*
*/
namespace Viter\Gerat\GeratBundle\Model;
/**
 * Data Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

##############################################
#                                            #
#  Autor: Roberto Beraldo Chaiben (Beraldo)  #
#    E-Mail: rbchaiben[arroba]gmail.com      #
#                                            #
##############################################

/****************************************
* Desenvolvido para PHP 5.0 ou superior *
****************************************/

/*
   Esta classe possui funções para validar datas, formatá-las na forma brasileira (DD/MM/YYYY) ou internacional (YYYY-MM-DD) e adicionar e subtrair períodos.

   Os formatos de data suportados pelas funções são:

       1. DD/MM/YYYY
       2. DD/MM/YYYY hh:mm
       3. DD/MM/YYYY hh:mm:ss
       4. YYYY-MM-DD
       5. YYYY-MM-DD hh:mm
       6. YYYY-MM-DD hh:mm:ss

*/

class Data
{

    /*
       bool ValidarData (string data)

       Essa função valida a data passada como parâmetro para o construtor da classe. Retorna TRUE se a data for válida. Caso contrário, retorna FALSE.
    */
    public function ValidarData($data)
    {
        if (!($dados = $this->explode_date ($data)))
            return false;

        //verifica se há elementos em $data
        if (count ($dados) > 0) {//se houver elementos no array
            if (!checkdate ($dados['mês'], $dados['dia'], $dados['ano'])) {
                echo "<p><strong>Erro:</strong> função <strong>". __FUNCTION__ ." (". $data .")</strong>: data inválida.</p>";

                return false;
            }

            //se houver o elemento 'horas', existirá 'minutos'
            if (array_key_exists ("horas", $dados)) {
                if ($dados['horas'] > 23) {
                    echo "<p><strong>Erro:</strong> função <strong>". __FUNCTION__ ." ()</strong>: hora maior que 23.</p>";

                    return false;
                }
                if ($dados['minutos'] > 59) {
                    echo "<p><strong>Erro:</strong> função <strong>". __FUNCTION__ ." ()</strong>: minutos maior que 59.</p>";

                    return false;
                }
            }
            if (array_key_exists ("segundos", $dados)) {
                if ($dados['segundos'] > 59) {
                    echo "<p><strong>Erro:</strong> função <strong>". __FUNCTION__ ." ()</strong>: segundos maior que 59.</p>";

                    return false;
                }
            }
        } else {//se não houver elementos no array

            return false;
        }

        return true;

    }

    /*
       mixed explode_date (string $data)

       Essa função divide o argumento 'data' em dia, mês e ano. Se houver especificação de horário, divide-o em horas, minutos e segundos, se houver. Após essa separação, os valores são colocados no array $dados.

       As seguintes chaves (índices) do array podem ser retornadas:

       'dia'     => retorna o dia
       'mês'     => retorna o mês
       'ano'     => retorna o ano
       'hora'    => retorna a hora
       'minuto'  => retorna os minutos
       'segundo' => retorna os segundos

       Se o formato de 'data' for inválido, retorna FALSE.
    */
    private function explode_date($data)
    {
        //retira o excesso de espaçoes no início e no final da data
        $data = trim ($data);

        //retira o excesso de espaços, se existir
        $data = preg_replace ("/( ){2,}/", " ", $data);

        switch (true) {
            case preg_match ("/^([0-9]{2}\/){2}[0-9]{4}$/", $data):
                list ($dia, $mês, $ano) = explode ("/", $data);
                break;
            case preg_match ("/^([0-9]{2}\/){2}[0-9]{4} [0-9]{2}:[0-9]{2}$/", $data):
                $explode = explode (" ", $data);
                list ($dia, $mês, $ano) = explode ("/", $explode[0]);
                list ($horas, $minutos) = explode (":", $explode[1]);
                break;
            case preg_match ("/^([0-9]{2}\/){2}[0-9]{4} [0-9]{2}(:[0-9]{2}){2}$/", $data):
                $explode = explode (" ", $data);
                list ($dia, $mês, $ano) = explode ("/", $explode[0]);
                list ($horas, $minutos, $segundos) = explode (":", $explode[1]);
                break;
            case preg_match ("/^[0-9]{4}(-[0-9]{2}){2}$/", $data):
                list ($ano, $mês, $dia) = explode ("-", $data);
                break;
            case preg_match ("/^[0-9]{4}(-[0-9]{2}){2} [0-9]{2}:[0-9]{2}$/", $data):
                $explode = explode (" ", $data);
                list ($ano, $mês, $dia) = explode ("-", $explode[0]);
                list ($horas, $minutos) = explode (":", $explode[1]);
                break;
            case preg_match ("/^[0-9]{4}(-[0-9]{2}){2} [0-9]{2}(:[0-9]{2}){2}$/", $data):
                $explode = explode (" ", $data);
                list ($ano, $mês, $dia) = explode ("-", $explode[0]);
                list ($horas, $minutos, $segundos) = explode (":", $explode[1]);
                break;
            default:
                echo "<p><strong>Erro:</strong> função <strong>". __FUNCTION__ ." (". $data .")</strong>: formato de data inválido.</p>";

                return false;
                break;

        }

        $dados['dia'] = $dia;
        $dados['mês'] = $mês;
        $dados['ano'] = $ano;

        //se existir $hora, também existirá $minuto
        if (isset($horas)) {
            $dados['horas'] = $horas;
            $dados['minutos'] = $minutos;
        }
        if (isset ($segundos)) {
            $dados['segundos'] = $segundos;
        }

        return $dados;

    }

    /*
       string AdicionarPeiodo(string data, string formato, int horas, int minutos, int segundos, int meses, int dias, int anos)

       Adiciona 'horas', 'minutos', 'segundos', 'meses', 'dias' e 'anos' à data 'data'. O argumento 'formato' define o tipo da data que será retornada. Se ele receber "pt", retornará a data no formato brasileiro (DD/MM/YY hh:mm:ss); se receber "en", retornará a data no formato internacional (YYYY-MM-DD hh:mm:ss). Se 'formato' tiver um valor diferente de 'pt' ou 'en', a data será retornada no formato brasileiro.
    */
    public function AdicionarPeriodo($data, $forma, $horas, $minutos, $segundos, $meses, $dias, $anos)
    {
        switch (false) {
            case is_int ($horas):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"horas\" deve ser um número inteiro.</p>";

                return false;
                break;
            case is_int ($minutos):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"minutos\" deve ser um número inteiro.</p>";

                return false;
                break;
            case is_int ($segundos):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"segundos\" deve ser um número inteiro.</p>";

                return false;
                break;
            case is_int ($meses):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"meses\" deve ser um número inteiro.</p>";

                return false;
                break;
            case is_int ($dias):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"dias\" deve ser um número inteiro.</p>";

                return false;
                break;
            case is_int ($anos):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"anos\" deve ser um número inteiro.</p>";

                return false;
                break;
        }

        $forma = strtolower ($forma);
        if ($forma != "pt" AND $forma != "en")
            $forma = "pt";

        if (!$this->ValidarData($data))
            return false;

        $dataEUA = $this->FormatarData ($data, "en");

        $data_segs = strtotime ($dataEUA);
        $data_retorno = date ("Y-m-d H:i:s", strtotime ("+". $anos ." Year + ". $meses ." Month + ". $dias ." Day + ". $horas ." Hour + ". $minutos ." Minute + ". $segundos ." Second", $data_segs));

        return (($forma == "pt") ? $this->FormatarData ($data_retorno, "pt") : $this->FormatarData ($data_retorno, "en"));

    }

    /*
       string SubtrairPeiodo(string data, string formato, int horas, int minutos, int segundos, int meses, int dias, int anos)

       Subtrai 'horas', 'minutos', 'segundos', 'meses', 'dias' e 'anos' da data 'data'. O argumento 'formato' define o tipo da data que será retornada. Se ele receber "pt", retornará a data no formato brasileiro (DD/MM/YY hh:mm:ss); se receber "en", retornará a data no formato internacional (YYYY-MM-DD hh:mm:ss). Se 'formato' tiver um valor diferente de 'pt' ou 'en', a data será retornada no formato brasileiro.
    */
    public function SubtrairPeriodo($data, $forma, $horas, $minutos, $segundos, $meses, $dias, $anos)
    {
        switch (false) {
            case is_int ($horas):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"horas\" deve ser um número inteiro.</p>";

                return false;
                break;
            case is_int ($minutos):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"minutos\" deve ser um número inteiro.</p>";

                return false;
                break;
            case is_int ($segundos):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"segundos\" deve ser um número inteiro.</p>";

                return false;
                break;
            case is_int ($meses):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"meses\" deve ser um número inteiro.</p>";

                return false;
                break;
            case is_int ($dias):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"dias\" deve ser um número inteiro.</p>";

                return false;
                break;
            case is_int ($anos):
                echo "<p>Erro. Função <sctrong>". __FUNCTION__ ."</strong>: o argumento \"anos\" deve ser um número inteiro.</p>";

                return false;
                break;
        }

        $forma = strtolower ($forma);
        if ($forma != "pt" AND $forma != "en")
            $forma = "pt";

        if (!$this->ValidarData($data))
            return false;

        $dataEUA = $this->FormatarData ($data, "en");

        $data_segs = strtotime ($dataEUA);
        $data_retorno = date ("Y-m-d H:i:s", strtotime ("-". $anos ." Year - ". $meses ." Month - ". $dias ." Day - ". $horas ." Hour - ". $minutos ." Minute - ". $segundos ." Second", $data_segs));

        return (($forma == "pt") ? $this->FormatarData ($data_retorno, "pt") : $this->FormatarData ($data_retorno, "en"));

    }

    /*
       string FormatarData (string data[, string formato])

       Formata 'data' segundo o valor de 'formato', que deve ter um destes valaores:

       "pt" => retirna a data no formato DD/MM/YYYY hh:mm:ss
       "en" => retorna a data no formato YYYY-MM-DD hh:mm:ss

       Se formato não for especificado ou tiver um valor inválido, a data será retornada na forma "pt".
    */
    public function FormatarData($data, $formato = "pt")
    {
        $formato = strtolower ($formato);
        if ($formato != "pt" AND $formato != "en")
            $formato = "pt";

        if (!$this->ValidarData($data))
            return false;

        $dados = $this->explode_date ($data);

        if ($formato == "pt") {
            $return = $dados['dia'] . "/" . $dados['mês'] . "/" . $dados['ano'];
            if (array_key_exists ("horas", $dados)) {
                $return .= " " . $dados['horas'] . ":" . $dados['minutos'];
                if (array_key_exists ("segundos", $dados))
                    $return .= ":" . $dados['segundos'];
            }
        } else {
            $return = $dados['ano'] . "-" . $dados['mês'] . "-" . $dados['dia'];
            if (array_key_exists ("horas", $dados)) {
                $return .= " " . $dados['horas'] . ":" . $dados['minutos'];
                if (array_key_exists ("segundos", $dados))
                    $return .= ":" . $dados['segundos'];
            }
        }

        return $return;

    }

    public function formataData($data)
    {
      if ((substr($data, 4, 1) == '-') && (substr($data, 7, 1) == '-')) {
        return $data;
      }

      if ((substr($data, 4, 1) == '/') && (substr($data, 7, 1) == '/')) {
        return $data;
      }

      $dia = substr($data, 0, 2);
      $mes = substr($data, 3, 2);
      $ano = substr($data, 6, 4);
      $data_formatada = $ano . '-' . $mes . '-' . $dia;

      return $data_formatada;
    }

    public function mesPorExtenso($mesNumerico)
    {
        switch ($mesNumerico) {
            case 01:
            $descricao = "Janeiro";
            break;

            case 02:
            $descricao = "Fevereiro";
            break;

            case 03:
            $descricao = "Março";
            break;

            case 04:
            $descricao = "Abril";
            break;

            case 05:
            $descricao = "Maio";
            break;

            case 06:
            $descricao = "Junho";
            break;

            case 07:
            $descricao = "Julho";
            break;

            case 08:
            $descricao = "Agosto";
            break;

            case 09:
            $descricao = "Setembro";
            break;

            case 10:
            $descricao = "Outubro";
            break;

            case 11:
            $descricao = "Novembro";
            break;

            case 12:
            $descricao = "Dezembro";
            break;
        }

        return $descricao;
    }
}
