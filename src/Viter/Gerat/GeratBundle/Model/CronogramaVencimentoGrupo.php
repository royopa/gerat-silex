<?php
/**
* CronogramaVencimentoGrupo File Doc Comment
*
* @category Class
* @package  Classes
* @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
* @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
* @link     http://www.caixa.gov.br
*
*/

/**
 * CronogramaVencimentoGrupo Class Doc Comment
 *
 * @category Class
 * @package  Classes
 * @author   Rodrigo Prado de Jesus <rodrigo.p.jesus@caixa.gov.br>
 * @license  http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @link     http://www.caixa.gov.br
 *
 */

namespace Viter\Gerat\GeratBundle\Model;

use Viter\Gerat\GeratBundle\Model\Highcharts\Highchart;
use Viter\Gerat\GeratBundle\Model\Highcharts\HighchartJsExpr;

class CronogramaVencimentoGrupo extends \ArrayIterator
{
    /**
     * @var FundoDiario
     * O fundo diário do ativo
     */
    private $fundoDiario;

    /**
     * @var Highchart
     * O gráfico da composição
     */
    private $grafico;

    /**
     * Gets the O fundo diário do ativo.
     *
     * @return FundoDiario
     */
    public function getFundoDiario()
    {
        return $this->fundoDiario;
    }

    /**
     * Sets the O fundo diário do ativo.
     *
     * @param FundoDiario $fundoDiario the fundo diario
     *
     * @return self
     */
    public function setFundoDiario(FundoDiario $fundoDiario)
    {
        $this->fundoDiario = $fundoDiario;

        return $this;
    }

    /**
     * Gets the O gráfico da composição.
     *
     * @return Highchart
     */
    public function getGrafico()
    {
        return $this->grafico;
    }

    /**
     * Sets the O gráfico da composição.
     *
     * @param Highchart $grafico the grafico
     *
     * @return self
     */
    public function setGrafico(Highchart $grafico)
    {
        $this->grafico = $grafico;

        return $this;
    }

    public function __construct(FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {
            $this->fundoDiario = $fundoDiario;
            $this->getProximosVencimentosAnoAtual($fundoDiario);
            $this->getProximosVencimentosAnosFuturos($fundoDiario);
            $this->montaGrafico($this->montaArray($fundoDiario));
        }
    }

    /**
     * Monta o array, get dados série que será usado no gráfico
     *
     * @return mixed[] o array com os dados da série
     */
    public function montaArray(FundoDiario $fundoDiario)
    {
        $dadosSerie     = array();

        foreach ($this as $cronograma) {

            if ($cronograma->getDataVencimento()->format('Y') == $fundoDiario->getDataAtualizacao()->format('Y')) {
                $name = $cronograma->getDataVencimento()->format('m/Y');
            } else {
                $name = $cronograma->getDataVencimento()->format('Y');
            }

            $dados = array(
                'name'  => $name,
                'data'  => round($cronograma->getPercentual(), 2),
                'valor' => $cronograma->getValorVolume()
            );

            $dadosSerie[]   = $dados;
        }

        return $dadosSerie;
    }

    /**
     * CronogramaVencimentoGrupo::montaGrafico()
     *
     * @return CronogramaVencimentoGrupo
     *
     * Monta o gráfico de colunas para os vencimentos
     */
    public function montaGrafico($serieDados)
    {
        $grafico = new Grafico();
        $this
            ->setGrafico(
                $this
                    ->getCronogramaVencimentoChart(
                        $serieDados,
                        'chart_cronograma_vencimento',
                        null,
                        'Cronograma de Vencimentos'
                    )
            );

        return $this;
    }

    /**
    * Monta o gráfico Highchart de pizza com legendas da série recebida
    *
    * @param mixed[] $dados_serie Dados da série para a geração do gráfico
    * @param string  $render_div  ID da div onde o gráfico será mostrado
    * @param string  $name        Nome que será mostrado no gráfico
    * @param string  $title       Titulo que será mostrado no gráfico
    *
    * @return  Highchart $chart   O gráfico highchart que será renderizado
    */
    public function getCronogramaVencimentoChart($dados_serie = null, $render_div = '', $name = '', $title = '', $categorias = null)
    {
        $chart = new Highchart();

        $subtitle   = "";

        $chart->chart->renderTo = $render_div;
        $chart->chart->type     = "column";
        $chart->title->text     = $title;
        $chart->subtitle->text  = $subtitle;

        $chart->xAxis->categories = array(
            ""
        );

        $chart->yAxis->min = 0;
        $chart->yAxis->title->text = null;
        $chart->yAxis->title->text = "";

        $chart->tooltip->formatter = new HighchartJsExpr(
            "function () {
                return '' +
                    this.series.name +
                    ' : R$ ' +
                    Highcharts.numberFormat(this.y, 2, ',', '.')
                ;
            }"
        );

        $chart->plotOptions->column->pointPadding = 0.2;

        foreach ($dados_serie as $row) {

            $chart->series[] = array(
                'name' => $row['name'],
                'data' => array(
                    0  => $row['valor']
                    ),
            );
        }

        return $chart;
    }

    /**
     * CronogramaVencimentoGrupo::fetchAll()
     *
    * @param FundoDia $fundoDiario O objeto fundo diário
     *
     * @return CronogramaVencimentoGrupo
     *
     * Pega todos os elementos do banco de dados e adiciona no objeto
     */
    public function fetchAll(FundoDiario $fundoDiario = null)
    {
        if ($fundoDiario instanceof FundoDiario) {

            //instancia o model Dbal para fazer acesso ao banco de dados
            $dbal = new Dbal();
            $conn = $dbal->getConn();

            $sql
                = "
                    SELECT
                        MONTH(DT_VCTO) mes,
                        YEAR(DT_VCTO) ano,
                        SUM(VR_MERC) VR_VOLUME
                    FROM
                        CR245002_RL_11
                    WHERE
                        CO_PRD = :cnpj AND
                        DT_ATU = :dataAtualizacao AND
                        DT_VCTO >= :dataVencimento AND
                        CO_TP_ATI <> 5
                    GROUP BY
                        MONTH(DT_VCTO),
                        YEAR(DT_VCTO)
                    ORDER BY
                        YEAR(DT_VCTO),
                        MONTH(DT_VCTO)
                  ";

            $stmt = $conn->prepare($sql);
            $stmt->bindValue(
                'cnpj',
                $fundoDiario->getFundo()->getCnpj(),
                'integer'
                );
            $stmt->bindValue(
                'dataAtualizacao',
                $fundoDiario->getDataAtualizacao(),
                'datetime'
                );
            $stmt->bindValue(
                'dataVencimento',
                $fundoDiario->getDataAtualizacao(),
                'datetime'
                );
            $stmt->execute();

            while ($row = $stmt->fetch()) {
                $cronograma = new CronogramaVencimento();
                $this->append($cronograma->create($fundoDiario, $row));
            }
        }
    }

    /**
     * CronogramaVencimentoGrupo::getProximosVencimentosAnoAtual()
     *
     * @param FundoDia $fundoDiario O objeto fundo diário
     *
     * @return CronogramaVencimentoGrupo
     *
     * Pega os próximos vencimentos do ano corrente
     */
    public function getProximosVencimentosAnoAtual(FundoDiario $fundoDiario = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $dataAtualizacao = $fundoDiario->getDataAtualizacao()->format('Y-m-d');

        $sql
            = "
                SELECT
                    MONTH(DT_VCTO) mes,
                    YEAR(DT_VCTO) ano,
                    SUM(VR_MERC) VR_VOLUME
                FROM
                    CR245002_RL_11
                WHERE
                    CO_PRD = :cnpj AND
                    DT_ATU = :dataAtualizacao AND
                    DT_VCTO >= :dataVencimento AND
                    YEAR(DT_VCTO) = YEAR('$dataAtualizacao') AND
                    CO_TP_ATI <> 5
                GROUP BY
                    MONTH(DT_VCTO),
                    YEAR(DT_VCTO)
                ORDER BY
                    YEAR(DT_VCTO),
                    MONTH(DT_VCTO)
              ";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(
            'cnpj',
            $fundoDiario->getFundo()->getCnpj(),
            'integer'
            );
        $stmt->bindValue(
            'dataAtualizacao',
            $fundoDiario->getDataAtualizacao(),
            'datetime'
            );
        $stmt->bindValue(
            'dataVencimento',
            $fundoDiario->getDataAtualizacao(),
            'datetime'
            );
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $cronograma = new CronogramaVencimento();
            $this->append($cronograma->create($fundoDiario, $row));
        }
    }

    /**
     * CronogramaVencimentoGrupo::getProximosVencimentosAnosFuturos()
     *
     * @param FundoDia $fundoDiario O objeto fundo diário
     *
     * @return CronogramaVencimentoGrupo
     *
     * Pega os próximos vencimentos do ano corrente
     */
    public function getProximosVencimentosAnosFuturos(FundoDiario $fundoDiario = null)
    {
        //instancia o model Dbal para fazer acesso ao banco de dados
        $dbal = new Dbal();
        $conn = $dbal->getConn();

        $dataAtualizacao = $fundoDiario->getDataAtualizacao()->format('Y-m-d');

        $sql
            = "
                SELECT
                    '12' AS mes,
                    YEAR(DT_VCTO) ano,
                    SUM(VR_MERC) VR_VOLUME
                FROM
                    CR245002_RL_11
                WHERE
                    CO_PRD = :cnpj AND
                    DT_ATU = :dataAtualizacao AND
                    DT_VCTO >= :dataVencimento AND
                    YEAR(DT_VCTO) > YEAR('$dataAtualizacao')
                GROUP BY
                    YEAR(DT_VCTO)
                ORDER BY
                    YEAR(DT_VCTO)
              ";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue(
            'cnpj',
            $fundoDiario->getFundo()->getCnpj(),
            'integer'
            );
        $stmt->bindValue(
            'dataAtualizacao',
            $fundoDiario->getDataAtualizacao(),
            'datetime'
            );
        $stmt->bindValue(
            'dataVencimento',
            $fundoDiario->getDataAtualizacao(),
            'datetime'
            );
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            $cronograma = new CronogramaVencimento();
            $this->append($cronograma->create($fundoDiario, $row));
        }
    }
}
