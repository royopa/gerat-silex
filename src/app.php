<?php

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\DoctrineServiceProviderSql2000;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\RememberMeServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Gerat\UserManager;

$app = new Application();
$app->register(new UrlGeneratorServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new SessionServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new FormServiceProvider());
$app['twig'] = $app->share($app->extend('twig', function ($twig, $app) {
    // add custom globals, filters, tags, ...
    return $twig;
}));

//http://silex.sensiolabs.org/doc/providers/doctrine.html
$app->register(new DoctrineServiceProviderSql2000(), array(
    'dbs.options' => array (
        'sql_server' => array(
            //'driver'       => 'pdo_sqlsrv',
            'driverClass'  => 'Doctrine\DBAL\Driver\PDOSqlsrv2000\Driver',
            'host'         => '10.6.9.47',
            'dbname'       => 'SIRAT',
            'user'         => 'SIRAT',
            'password'     => 'SIRAT',
            'charset'      => 'utf8',
        ),
        'mysql_read' => array(
            'driver'    => 'pdo_mysql',
            'host'      => 'localhost',
            'dbname'    => 'tematres',
            'user'      => 'root',
            'password'  => 'root',
            'charset'   => 'utf8',
        ),
    ),
));

$app['user.manager'] = new UserManager($app['db'], $app);

$app->register(new SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'secured_area' => array(
            'pattern' => '^.*$',
            'anonymous' => true,
            //'remember_me' => array(),
            'form' => array(
                'login_path' => '/login',
                'check_path' => '/user/login_check',
            ),
            'logout' => array(
                'logout_path' => '/logout'
            ),
            //'users' => $app->share(function ($app) { return $app['user.manager']; }),
            'users' => $app->share(function ($app) { return new UserManager($app['db'], $app); }),
        ),
    ),
));
// Note: As of this writing, RememberMeServiceProvider must be registered *after* SecurityServiceProvider
// throws 'InvalidArgumentException' with message 'Identifier "security.remember_me.service.secured_area" is not defined.'
//$app->register(new RememberMeServiceProvider());

// Register the SimpleUser service provider.
//$app->register($u = new SimpleUser\UserServiceProvider());

// Optionally mount the SimpleUser controller provider.
//$app->mount('/user', $u);

$app->register(new TranslationServiceProvider(), array(
    'translator.domains' => array(),
));

//****************************************************************
//********** Parâmetros da aplicação *****************************
//****************************************************************
$app['base_path'] = 'http://localhost/gerat-silex';

//********** Parâmetros para fevereiro/2014 **********************
$app['dataInicio']      = new \DateTime('2014-01-31');
$app['dataReferencia']  = new \DateTime('2014-02-28');
$app['dataAtualizacao'] = new \DateTime('2014-03-05');
$app['dataFim']         = $app['dataReferencia'];

return $app;
