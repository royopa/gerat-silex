<?php if (count($fundoDiario->getListaLimiteAlocacaoGrupo()->getLimiteNaoIfGrupo()) > 0) : ?>
<div class="row">
<h2 class="panel_title" id="titulo_painel_2">
    Instituições Não Financeiras 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    R$ <?php 
      echo formataMoeda($fundoDiario
            ->getListaLimiteAlocacaoGrupo()
            ->getLimiteNaoIfGrupo()
            ->getValorTotal()); ?>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    
    <?php 
      echo round($fundoDiario
            ->getListaLimiteAlocacaoGrupo()
            ->getLimiteNaoIfGrupo()
            ->getPercentualTotal(), 2); ?>% PL
</h2>
<div class="col-md-12">
  <div class="panel panel-info">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Contraparte</th>
            <th>Rating</th>
            <th>Alocado</th>
            <th>Limite % PL</th>            
            <th>Alocado % PL</th>
            <th title="Percentual sobre o limite">% Limite</th>
            <th>% Limite - Resgate Stress</th>
            <th width="1px">&nbsp;</th>
          </tr>
        </thead>
        <tbody class="table table-bordered">
          <?php foreach ($fundoDiario->getListaLimiteAlocacaoGrupo()->getLimiteNaoIfGrupo() as $limite) : ?>
          <tr>
            <td>
              <?php echo $limite->getEmissor()->getNome(); ?>
            </td>
            <td>
              <?php echo $limite->getEmissor()->getRating(); ?>
            </td>
            <td>
              R$ <?php echo formataMoeda($limite->getValorAlocado()); ?>
            </td>
            <td>
              <?php echo $limite->getEmissor()->getPercentualLimite(); ?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocado(), 2)?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocadoLimite(), 2)?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocadoLimiteStress(), 2)?>%
            </td>
            <td>
              <?php if (($limite->getPercentualAlocadoLimite() >= 70) && 
                ($limite->getPercentualAlocadoLimite() < 90)) : ?>
              <span class="label alerta-amarelo">Alerta</span>
              <?php endif; ?>

              <?php if (($limite->getPercentualAlocadoLimite() >= 90) && 
                ($limite->getPercentualAlocadoLimite() < 100)) : ?>
              <span class="label alerta-laranja">Alerta</span>
              <?php endif; ?>

              <?php if (($limite->getPercentualAlocadoLimite() >= 100)) : ?>
              <span class="label alerta-vermelho">Alerta</span>
              <?php endif; ?>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php endif; ?>