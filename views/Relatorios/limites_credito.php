<?php $listaAtivosCredito = new ListaAtivosCredito($fundoDiario); ?>
<?php foreach ($listaAtivosCredito as $limiteGrupo) : ?>
<?php if (count($limiteGrupo) > 0) : ?>
<div class="row">
<div class="col-md-8">
  <div class="panel panel-info">
  <div class="panel-heading" id="titulo_painel">
    Limites - <?php echo $limiteGrupo->getNome(); ?>
  </div>
    <div class="panel-body">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Contraparte</th>
            <th>Rating</th>
            <th>Limite % PL</th>
            <th>Alocado</th>
            <th>Alocado % PL</th>
            <th title="Percentual sobre o limite">% Limite</th>
            <th>% Limite - Resgate Stress</th>
          </tr>
        </thead>
        <tbody class="table-hover table-striped">
          <?php $i = 1; ?>
          <?php foreach ($limiteGrupo as $limite) : ?>
          <tr>
            <td>
              <?php echo $i; ?>
            </td>
            <td>
              <?php echo $limite->getEmissor()->getNome(); ?>
            </td>
            <td>
              <?php echo $limite->getEmissor()->getRating(); ?>
            </td>
            <td>
              <?php echo $limite->getEmissor()->getPercentualLimite(); ?>%
            </td>
            <td>
              R$ <?php echo formataMoeda($limite->getValorAlocado()); ?>
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocado(), 2)?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocadoLimite(), 2)?>%
            </td>
            <td>
              <?php echo round($limite->getPercentualAlocadoLimiteStress(), 2)?>%
            </td>
          </tr>
          <?php $i++; ?>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    </div>
  </div>
</div>
<?php endif; ?>
<?php endforeach; ?>