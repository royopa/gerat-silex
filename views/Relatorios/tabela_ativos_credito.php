<?php $listaAtivosCredito = new ListaAtivosCredito($fundoDiario) ?>
<?php foreach ($listaAtivosCredito as $grupo) : ?>
<?php if (count($grupo) > 0) : ?>
<div class="panel panel-info">
    <div class="panel-heading" id="titulo_painel">
      <?php echo $grupo->getNome(); ?>
    </div>
    <div class="panel-body">
    <table class="table table-bordered text-center">
        <thead>
            <tr>
                <th>Ativo</th>
                <th>Emissor</th>
                <th>Rating</th>
                <th>Limite (% do PL)</th>
                <th>Valor mercado</th>
                <th>Alocado  (% do PL)</th>
                <th>% Limite</th>
                <th>% Limite - Resgate Stress</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($grupo as $ativoCredito) : ?>
            <tr>
                <td>
                  <?php echo $ativoCredito->getNome(); ?>
                </td>
                <td>
                  <?php echo $ativoCredito->getEmissor()->getNome(); ?>
                </td>
                <td>
                  <?php echo $ativoCredito->getEmissor()->getRating(); ?>
                </td>
                <td>
                  <?php echo $ativoCredito->getPercentualLimite(); ?>%
                </td>
                <td>
                  R$ <?php echo formataMoeda($ativoCredito->getValorMercado()); ?>
                </td>
                <td>
                  <?php echo round($ativoCredito->getPercentualAlocado(), 2); ?>%
                </td>
                <td>
                  <?php echo round($ativoCredito->getPercentualAlocadoSobreLimite(), 2); ?>%
                </td>
                <td>
                  <?php echo round($ativoCredito->getPercentualLimiteStress(), 2) . '%'; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
</div>
<?php endif; ?>
<?php endforeach; ?>