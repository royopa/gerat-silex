<?php 
  //var_dump($fundoDiario);
  //var_dump($dados_serie_distribuicao_cotistas); 
  //var_dump($captacao_rentabilidade);
  //var_dump($dados_serie_captacao_rentabilidade)
  //var_dump($produto_dia);
  //var_dump($dados_liquidez_agrupados);
  //maior resgate dos últimos 3 anos
  //var_dump($fundoDiario->getDataAtualizacao()->format('d/m/Y'));
?>
 
<!--
  NAV BAR - Só mostra se não for a versão para impressão
  -->
<?php if ($print != 'yes') : ?>
<nav class="navbar navbar-inverse" role="navigation">
  <div class="navbar-header">
    <a class="navbar-brand" href="#">&nbsp;</a>
  </div>
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      <li>
        <?php echo anchor(
                    'produtos/relatorioRisco?co_prd=' . 
                    $produto->CO_PRD . 
                    '&print=yes' . 
                    '&dt_atu=' . $fundoDiario->getDataAtualizacao()->format('d/m/Y'), 
                    '<i class="glyphicon glyphicon-print" ></i>', 
                      array(
                        'title' => 'Gerar versão para impressão')
                        ); ?>
      </li>
      <li><a href="#">Link</a></li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li><a href="#">Separated link</a></li>
          <li><a href="#">One more separated link</a></li>
        </ul>
      </li>
    </ul>
    <?php 
    $attributes = array('class' => 'navbar-form navbar-right', 'id' => 'form_consulta', 'method' => 'get');
    echo form_open('produtos/relatorioRisco', $attributes); 
    ?>
      <div class="form-group">
        <input type="hidden" id="co_prd" name="co_prd" value="<?php echo $fundoDiario->getFundo()->getCnpj(); ?>" />
        <input type="text" class="form-control" value="<?php echo $fundoDiario->getDataAtualizacao()->format('d/m/Y'); ?>" name="dt_atu" id="dt_atu" maxlength="10">
      </div>
      <button type="submit" class="btn btn-default">Atualiza data</button>
      <label for="dt_atu" class="error label label-danger"></label>
    </form>
  </div>
</nav>
<?php endif; ?>
<!--
  FIM DA NAV BAR
-->

<!--
  H1 com o nome do fundo
-->
<div class="page-header">
  <h1 id="nome_fundo_h1"><?php echo $fundoDiario->getFundo()->getNome(); ?></h1>
</div>
<!--
  Fim do H1 com o nome do fundo
-->

<!--
  PAINEL
-->
<div class="row" id="info_fundo">
  <strong id="posicao_dt_atu">Posição&nbsp;<?php echo $fundoDiario->getDataAtualizacao()->format('d/m/Y'); ?></strong>
  <strong id="patrimonio_liquido_span">Patrimônio Líquido R$ <?php echo formataMoeda($fundoDiario->getPatrimonioLiquido()); ?></strong>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title" id="titulo_painel">Alocação - Composição da Carteira*</h3>
      </div>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Ativos</th>
            <th>Total de Investimento</th>
            <th>% PL</th>
          </tr>
        </thead>
        <tbody class="table-hover table-striped">
          <?php 
            $i = 1; 
            $soma_vr_carteira = 0;
          ?>
          <?php foreach ($composicao as $row) : ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo utf8_encode($row['NO_MTP_ATI']); ?></td>
            <td>R$ <?php echo formataMoeda(abs($row['VR_MERC'])); ?></td>
            <td><?php echo round((abs($row['VR_MERC']) / $vr_pl) * 100, 2) . '%'; ?></td>
          </tr>
          <?php 
            $i++;
            $soma_vr_carteira = $soma_vr_carteira + abs($row['VR_MERC']);
          ?>
          <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>R$ <?php echo formataMoeda($soma_vr_carteira); ?></td>
            <td>&nbsp;</td>
          </tr>
        </tfoot>          
      </table>
    </div>
    
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title" id="titulo_painel">Carteira de Crédito</h3>
      </div>
      <table class="table bordered-table" id="table_carteira_credito">
        <thead>
          <tr>
            <th>#</th>
            <th>Ativos</th>
            <th>Total de Investimento</th>
            <th>% PL</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            $i = 1; 
            $soma_vr_carteira_rc = 0;
          ?>
          <?php foreach ($composicao_rc as $row) : ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo utf8_encode($row['NO_TP_ATI']); ?></td>
            <td>R$ <?php echo formataMoeda(abs($row['VR_MERC'])); ?></td>
            <td><?php echo round(abs($row['VR_MERC'] / $vr_pl) * 100, 2) . '%'; ?></td>
          </tr>
          <?php 
            $i++;
            $soma_vr_carteira_rc = $soma_vr_carteira_rc + abs($row['VR_MERC']);
          ?>
          <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>R$ <?php echo formataMoeda($soma_vr_carteira_rc); ?></td>
            <td>&nbsp;</td>
          </tr>
        </tfoot>        
      </table>
    </div>
    
  </div><!-- /.col-md-6 -->

  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="pie_chart_composicao_carteira"></div>
        <script type="text/javascript">
          <?php echo $chart_composicao_carteira->render("chart1"); ?>
        </script>
      </div>
    </div>
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="chart_composicao_credito"></div>
        <script type="text/javascript">
          <?php echo $chart_composicao_credito->render("chart1"); ?>
        </script>        
      </div>
    </div>
  </div><!-- /.col-md-6 -->

</div>

  <?php if (count($composicao_cdb) > 1): ?>
  <div class="panel panel-info">
    <div class="panel-heading" id="titulo_painel">CDB</div>
      <div class="panel-body">
        <h4>Instituição Financeira</h4>
        <?php var_dump($composicao_cdb); ?>
      </div>
  </div>
  <?php endif; ?>

  <div class="panel panel-info">
    <div class="panel-heading" id="titulo_painel">CDBSUB</div>
      <div class="panel-body">
        <h4>Instituição Financeira</h4>
        <?php var_dump($composicao_cdbsub); ?>
      </div>
  </div>

  <!--
    Debêntures Fazer tabela que receba o objeto e monte a tabela
  -->
  <div class="panel panel-info">
    <div class="panel-heading" id="titulo_painel">Debêntures</div>
        <table class="table table-bordered text-center">
          <thead>
            <tr>
              <th>Ativo</th>
              <th>Emissor</th>
              <th>Rating</th>
              <th>Limite</th>
              <th>Valor mercado</th>
              <th>% PL</th>
              <th>% Limite</th>
              <th>% Limite - Resgate Stress</th>
              <th>% Limite - Resgate Stress</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($composicao_debentures as $row) : ?>
            <?php 
              //porcentagem limite
              $limite           = $row['PC_LIM'] * 100;
              //valor com limite de stress
              $limite_stress    = $row['PC_LIM'] * $fundoDiario->getPatrimonioLiquidoStress();
              //valor de mercado do ativo
              $vr_mercado       = abs($row['VR_MERC']);
              //percentual do ativo sobre o pl do fundo
              $pc_pl            = abs($row['VR_MERC'] / $vr_pl) * 100;
              //percentual do ativo sobre o limite
              $pc_limite        = ($pc_pl/$limite) * 100;
              //percentual do ativo sobre o limite de stress
              $pc_limite_stress = ($vr_mercado / $fundoDiario->getPatrimonioLiquidoStress() / $limite) * 10000;
              //percentual do ativo sobre o limite de stress de acordo com o 
              //resgate projetado
              //o valor de mercado / pl -( resgate projetado * 3,2)
              $pc_limite_stress2 
                = $vr_mercado / (
                    $vr_pl - ($produto_dia['VR_RESG_PROJ'] * 3.2)
                    ) * 100;
            ?>              
            <tr>
              <td><?php echo utf8_encode($row['NO_ATI']); ?></td>
              <td><?php echo utf8_encode($row['NO_EMISS']); ?></td>
              <td><?php echo utf8_encode($row['NO_RATING']); ?></td>
              <td><?php echo $limite; ?>%</td>
              <td>R$ <?php echo formataMoeda(abs($row['VR_MERC'])); ?></td>
              <td><?php echo round($pc_pl, 2) . '%'; ?></td>
              <td><?php echo round($pc_limite, 2) . '%'; ?></td>
              <td><?php echo round($pc_limite_stress, 2) . '%'; ?></td>
              <td><?php echo round($pc_limite_stress2, 2) . '%'; ?></td>
            </tr>
            <?php 
              $i++;
              $soma_vr_carteira_rc = $soma_vr_carteira_rc + abs($row['VR_MERC']);
            ?>
            <?php endforeach; ?>
          </tbody>
        </table>
  </div>

  <div class="panel panel-info">
    <div class="panel-heading" id="titulo_painel">DPGE</div>
      <div class="panel-body">
        <h4>Instituição Financeira</h4>
        <?php var_dump($composicao_dpge); ?>
      </div>
  </div>

  <!--
    FIDC
  -->
  <div class="panel panel-info">
    <div class="panel-heading" id="titulo_painel">FIDC</div>
      <div class="panel-body">
        <h4>Instituição Financeira</h4>
        <?php var_dump($composicao_fidc); ?>
      </div>
  </div>

  <!--
    LF
  -->
  <div class="panel panel-info">
    <div class="panel-heading" id="titulo_painel">LF</div>
      <div class="panel-body">
        <h4>Instituição Financeira</h4>
        <?php var_dump($composicao_lf); ?>
      </div>
  </div>


<div class="panel panel-info">
  <div class="panel-heading" id="titulo_painel">LFSUB</div>
    <div class="panel-body">
      <h4>Instituição Financeira</h4>
      <?php var_dump($composicao_lfsub); ?>
    </div>
</div>

<div class="panel panel-info">
  <div class="panel-heading" id="titulo_painel">Operação Compromissada</div>
    <div class="panel-body">
      <h4>Instituição Financeira</h4>
      <?php var_dump($composicao_op_compromissada); ?>
    </div>
</div>

<div class="panel panel-info">
  <div class="panel-heading" id="titulo_painel">Letra Hipotecária</div>
    <div class="panel-body">
      <h4>Instituição Financeira</h4>
      <?php var_dump($composicao_letra_hipotecaria); ?>
    </div>
</div>

<div class="panel panel-info">
  <div class="panel-heading" id="titulo_painel">LH</div>
    <div class="panel-body">
      <h4>Instituição Financeira</h4>
      <?php var_dump($composicao_lh); ?>
    </div>
</div>

<div class="panel panel-info">
  <div class="panel-heading" id="titulo_painel">CRI</div>
    <div class="panel-body">
      <h4>Instituição Financeira</h4>
      <?php var_dump($composicao_cri); ?>
    </div>
</div>

<div class="panel panel-info">
  <div class="panel-heading" id="titulo_painel">CCI</div>
    <div class="panel-body">
      <h4>Instituição Financeira</h4>
      <?php var_dump($composicao_cci); ?>
    </div>
</div>

<div class="panel panel-info">
  <div class="panel-heading" id="titulo_painel">Limites</div>
    <div class="panel-body">
      <h4>Instituição Financeira</h4>
      <table class="table bordered-table">
        <thead>
          <tr>
            <th>#</th>
            <th>Contraparte</th>
            <th>Rating</th>
            <th>Limite % PL</th>
            <th>Alocado</th>
            <th>Alocado % PL</th>
            <th title="Percentual sobre o limite">% Limite</th>
            <th>% Limite - Resgate Stress</th>
          </tr>
        </thead>
        <tbody>
          <?php $i = 1; ?>
          <?php foreach ($limite_if as $row) : ?>
          <?php
             $limite = $row['PC_LIM'] * $vr_pl;
             $limite_stress = $row['PC_LIM'] *  ($fundoDiario->getPatrimonioLiquidoStress());
          ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['NO_EMISS'] ?></td>
            <td><?php echo $row['NO_RATING'] ?></td>
            <td><?php echo $row['PC_LIM'] * 100 ?>%</td>
            <td>R$ <?php echo formataMoeda($row['VR_ALOC']); ?></td>
            <td><?php echo round($row['PC_ALOC'] * 100, 2)?>%</td>
            <td><?php echo round(($row['VR_ALOC'] / $limite) * 100, 2)?>%</td>
            <td>
              <?php echo round(($row['VR_ALOC'] / $limite_stress) * 100, 2) . '%'; ?>
            </td>
          </tr>
          <?php $i++; ?>
          <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>R$ <?php //echo formataMoeda($soma_vr_carteira_rc); ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </tfoot>
      </table>         
      <h4>Instituição Não-financeira</h4>
      <table class="table bordered-table">
        <thead>
          <tr>
            <th>#</th>
            <th>Contraparte</th>
            <th>Rating</th>
            <th>Limite % PL</th>
            <th>Alocado</th>
            <th>Alocado % PL</th>
            <th title="Percentual sobre o limite">% Limite</th>
            <th>% Limite - Resgate Stress</th>
          </tr>
        </thead>
        <tbody>
          <?php $i = 1; ?>
          <?php foreach ($limite_nao_if as $row) : ?>
          <?php
             $limite = $row['PC_LIM'] * $vr_pl;
             $limite_stress = $row['PC_LIM'] *  ($fundoDiario->getPatrimonioLiquidoStress());
          ?>
          <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['NO_EMISS'] ?></td>
            <td><?php echo $row['NO_RATING'] ?></td>
            <td><?php echo $row['PC_LIM'] * 100 ?>%</td>
            <td>R$ <?php echo formataMoeda($row['VR_ALOC']); ?></td>
            <td><?php echo round($row['PC_ALOC'] * 100, 2)?>%</td>
            <td><?php echo round(($row['VR_ALOC'] / $limite) * 100, 2)?>%</td>
            <td>
              <?php echo round(($row['VR_ALOC'] / $limite_stress) * 100, 2) . '%'; ?>
            </td>            
          </tr>
          <?php $i++; ?>
          <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>R$ <?php //echo formataMoeda($soma_vr_carteira_rc); ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </tfoot>        
      </table>      
    </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title" id="titulo_painel">Risco de Liquidez</h3>
      </div>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Quadro</th>
            <th>Volume Líquido</th>
            <th>Valor Mercado</th>
            <th>% Liquidez</th>
          </tr>
        </thead>
        <tbody class="table-hover table-striped">
          <?php $soma_vol_liquido = 0; ?>
          <?php $soma_vr_mercado = 0; ?>
          <?php foreach ($dados_liquidez_agrupados as $row) : ?>
          <tr>
            <td><?php echo utf8_encode($row['NO_MTP_ATI']); ?></td>
            <td>R$ <?php echo formataMoeda(abs($row['VR_VOLUME_1D'])); ?></td>
            <td>R$ <?php echo formataMoeda(abs($row['VR_MERC'])); ?></td>
            <td>
              <?php 
                echo 
                  round(
                      abs($row['VR_VOLUME_1D'])/abs($row['VR_MERC']) * 100, 2
                      ); ?>%
            </td>
          </tr>
          <?php $soma_vol_liquido = $soma_vol_liquido + abs($row['VR_VOLUME_1D']); ?>
          <?php $soma_vr_mercado = $soma_vr_mercado + abs($row['VR_MERC']); ?>
          <?php endforeach; ?>
        </tbody>
        <tfoot>
          <tr>
            <td>Total volume líquido - VL</td>
            <td>R$ <?php echo formataMoeda($soma_vol_liquido); ?></td>
            <td>R$ <?php echo formataMoeda($soma_vr_mercado); ?></td>
            <td>
              <?php 
                echo 
                  round(
                      (float)$soma_vol_liquido/(float)$soma_vr_mercado * 100, 2
                      ); ?>%
            </td>
          </tr>
        </tfoot>        
      </table>
    </div>
  </div><!-- /.col-md-6 -->
  
  <!-- Indicadores de liquidez -->
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title" id="titulo_painel">Indicadores de Liquidez</h3>
      </div>
      <table class="table table-bordered">
        <tbody class="">
          <tr>
            <td>Resgate projetado (RP)</td>
            <td>R$ <?php echo formataMoeda($fundoDiario->getResgateProjetado()) ?></td>
          </tr>
          <tr>
            <td>RL1 - RP / VL</td>
            <td>
              <?php echo 
                round($fundoDiario->getResgateProjetado()/$soma_vol_liquido * 100, 2) ?>%
            </td>
          </tr>
          <tr>
            <td>"Resgate Stress </br>(Maior resgate - 3 anos)"</td>
            <td>R$ <?php echo formataMoeda($fundoDiario->getMaiorResgateUltimos3Anos()) ?></td>
          </tr>                    
        </tbody>
      </table>     
    </div>
  </div><!-- /.col-md-6 -->
</div>

<!-- Limite de liquidez - SIRAT último quadro inferior -->
<!--
<div class="row">
  <div class="panel panel-info">
    <div class="panel-heading" id="titulo_painel">Limite de liquidez</div>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Quadro</th>
            <th>Vol 1 dia</th>
            <th>Vol 1 dia stress</th>
            <th>Valor mercado</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($limite_liquidez as $row) : ?>
          <tr>
            <td><?php echo utf8_encode($row['NO_ITEM']); ?></td>
            <td><?php echo formataMoeda($row['VR_VOLUME_1D']); ?>
            <td><?php echo formataMoeda($row['VR_VOLUME_1D_S']); ?>
            <td><?php echo formataMoeda($row['VR_MERC']); ?></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>               
    </div>  
  </div>
  -->
<!-- Fim - Limite de liquidez - SIRAT último quadro inferior -->

<!-- Quadro/Gráfico Captação/Rentabilidade -->
<div class="panel panel-info">
  <div id="chart_captacao_rentabilidade"></div>
  <script type="text/javascript"><?php echo $chart_captacao_rentabilidade->render("chart1"); ?></script>
</div><!-- Fim - Quadro/Gráfico Captação/Rentabilidade -->


<!-- Distribuição dos cotistas de um fundo -->
<div class="row">
  <div class="col-md-6">
    <!-- Perfil de distribuição dos cotistas -->
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title" id="titulo_painel">Perfil de Distribuição - Cotistas</h3>
      </div>
      <table class="table bordered-table">
        <thead>
          <tr>
            <th>FIC/Carteira</th>
            <th>Valor aplicado</th>
            <th>% PL</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($distribuicao_cotistas as $row) : ?>
          <tr>
            <td><?php echo utf8_encode($row['NO_PRD']); ?></td>
            <td>R$ <?php echo formataMoeda(abs($row['VR_COTA_FF'])); ?></td>
            <td><?php echo round(($row['VR_COTA_FF'] / $vr_pl) * 100, 2) . '%'; ?></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>    
    </div>
  </div><!-- /.col-md-6 -->
  
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-body">
        <div id="chart_distribuicao_cotistas"></div>
        <script type="text/javascript"><?php echo $chart_distribuicao_cotistas->render("chart1"); ?></script>
      </div>
    </div>
  </div><!-- /.col-md-6 -->  
</div><!-- Fim - distribuição dos cotistas de um fundo -->



</div><!-- Fim do container -->