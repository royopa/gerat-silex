<?php

/* layout.html.twig */
class __TwigTemplate_51d8c50b37dfb503385427bc946662749c5a5b2c847a5de27b3934bf32e729b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'navbar' => array($this, 'block_navbar'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"pt-br\" lang=\"pt-br\" >
    <head>
        <!-- Bootstrap CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/bootstrap/dist/css/bootstrap-cerulean.min.css\">
        <!-- DataTables CSS -->
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/datatables/media/css/jquery.dataTables.css\">
        <!-- DataTables Tabletools CSS -->
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/datatables-tabletools/css/dataTables.tableTools.css\">
        <!-- CSS da página -->
        <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "basepath"), "html", null, true);
        echo "/css/main.css\" rel=\"stylesheet\" type=\"text/css\" />
        <!-- jQueryUI CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/jquery-ui/themes/redmond/jquery-ui.min.css\" />
        <!-- FontAwesome CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/font-awesome/css/font-awesome.min.css\" />
        <meta charset=\"utf-8\">
        <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js') }}\"></script>
        <![endif]-->
        ";
        // line 20
        $this->displayBlock('title', $context, $blocks);
        // line 27
        echo "
         <!-- Favicon Caixa -->
        <link rel=\"icon\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "basepath"), "html", null, true);
        echo "/img/favicon.ico\" type=\"image/x-icon\">
    </head>
    <body>
        ";
        // line 32
        $this->displayBlock('navbar', $context, $blocks);
        // line 35
        echo "
        <div class=\"container theme-showcase\" id=\"container-theme-showcase\">
        ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session"), "flashbag"), "get", array(0 => "success"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 38
            echo "            <div class=\"alert alert-success\">
                ";
            // line 39
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : null), "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "
        ";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session"), "flashbag"), "get", array(0 => "info"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 44
            echo "            <div class=\"alert alert-info\">
                ";
            // line 45
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : null), "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "
        ";
        // line 49
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session"), "flashbag"), "get", array(0 => "danger"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 50
            echo "            <div class=\"alert alert-danger\">
                ";
            // line 51
            echo twig_escape_filter($this->env, (isset($context["flashMessage"]) ? $context["flashMessage"] : null), "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "
        ";
        // line 55
        $this->displayBlock('content', $context, $blocks);
        // line 56
        echo "        </div><!-- /container -->

        <!-- jQuery JS -->
        <script src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/jquery/dist/jquery.min.js\"></script>
        <!-- jQueryUI JS -->
        <script src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/jquery-ui/ui/minified/jquery-ui.min.js\"></script>
        <!-- Bootstrap JS -->
        <script src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/bootstrap/dist/js/bootstrap.min.js\"></script>
        <!-- DataTables JS -->
        <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/datatables/media/js/jquery.dataTables.js\"></script>
        <!-- DataTables Tabletools JS -->
        <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/datatables-tabletools/js/dataTables.tableTools.js\"></script>
        <!-- JS da página -->
        <script type=\"text/javascript\" language=\"javascript\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "basepath"), "html", null, true);
        echo "/js/main.js\"></script>
        <!-- Códigos JS -->
        <script type=\"text/javascript\" charset=\"utf-8\">
        \$( document ).ready(function() {
            \$('.datatable').dataTable( {
                \"sDom\": 'T<\"clear\">lfrtip',
                \"oTableTools\": {
                    \"sSwfPath\": \"";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["app"]) ? $context["app"] : null), "base_path"), "html", null, true);
        echo "/bower_components/datatables-tabletools/swf/copy_csv_xls_pdf.swf\"
                },
                \"oLanguage\": {
                    \"sProcessing\": \"Aguarde enquanto os dados são carregados ...\",
                    \"sLengthMenu\": \"Mostrar _MENU_ registros por pagina\",
                    \"sZeroRecords\": \"Nenhum registro encontrado\",
                    \"sInfoEmtpy\": \"Exibindo 0 a 0 de 0 registros\",
                    \"sInfo\": \"Exibindo de _START_ a _END_ de _TOTAL_ registros\",
                    \"sInfoFiltered\": \"\",
                    \"sSearch\": \"Procurar\",
                    \"oPaginate\": {
                        \"sFirst\":    \"Primeiro\",
                        \"sPrevious\": \"Anterior\",
                        \"sNext\":     \"Próximo\",
                        \"sLast\":     \"Último\",
                    },
                },
                \"bPaginate\": true,
                //\"bLengthChange\": false,
                //\"bFilter\": true,
                //\"bSort\": true,
                //\"bInfo\": true,
                //\"bAutoWidth\": true,
                \"bJQueryUI\": false,
                \"sPaginationType\": \"full_numbers\",
                \"iDisplayLength\": 25
            });
        });
        </script>
        ";
        // line 105
        $this->displayBlock('footer', $context, $blocks);
        // line 112
        echo "    </body>
</html>
";
    }

    // line 20
    public function block_title($context, array $blocks = array())
    {
        // line 21
        echo "            ";
        if (array_key_exists("title", $context)) {
            // line 22
            echo "                <title>";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</title>
            ";
        } else {
            // line 24
            echo "                <title>My Silex Application</title>
            ";
        }
        // line 26
        echo "        ";
    }

    // line 32
    public function block_navbar($context, array $blocks = array())
    {
        // line 33
        echo "        ";
        $this->env->loadTemplate("layout.html.twig", "1318591357")->display($context);
        // line 34
        echo "        ";
    }

    // line 55
    public function block_content($context, array $blocks = array())
    {
    }

    // line 105
    public function block_footer($context, array $blocks = array())
    {
        // line 106
        echo "        <div class=\"footer text-center\">
            <p>GERAT - Gerência Nacional de Risco de Ativos de Terceiros</p>
            <p>DETER - Diretoria Executiva de Ativos de Terceiros</p>
            <p>VITER - Vice-Presidência de Gestão de Ativos de Terceiros</p>
        </div>
        ";
    }

    public function getTemplateName()
    {
        return "layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 106,  254 => 105,  249 => 55,  245 => 34,  242 => 33,  239 => 32,  235 => 26,  231 => 24,  225 => 22,  222 => 21,  219 => 20,  213 => 112,  211 => 105,  179 => 76,  169 => 69,  164 => 67,  159 => 65,  154 => 63,  149 => 61,  144 => 59,  139 => 56,  137 => 55,  134 => 54,  125 => 51,  122 => 50,  118 => 49,  115 => 48,  106 => 45,  103 => 44,  99 => 43,  96 => 42,  87 => 39,  84 => 38,  80 => 37,  76 => 35,  74 => 32,  68 => 29,  64 => 27,  62 => 20,  54 => 15,  49 => 13,  44 => 11,  39 => 9,  34 => 7,  29 => 5,  23 => 1,  31 => 4,  28 => 3,);
    }
}


/* layout.html.twig */
class __TwigTemplate_51d8c50b37dfb503385427bc946662749c5a5b2c847a5de27b3934bf32e729b7_1318591357 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("navbar.html.twig");

        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "navbar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 106,  254 => 105,  249 => 55,  245 => 34,  242 => 33,  239 => 32,  235 => 26,  231 => 24,  225 => 22,  222 => 21,  219 => 20,  213 => 112,  211 => 105,  179 => 76,  169 => 69,  164 => 67,  159 => 65,  154 => 63,  149 => 61,  144 => 59,  139 => 56,  137 => 55,  134 => 54,  125 => 51,  122 => 50,  118 => 49,  115 => 48,  106 => 45,  103 => 44,  99 => 43,  96 => 42,  87 => 39,  84 => 38,  80 => 37,  76 => 35,  74 => 32,  68 => 29,  64 => 27,  62 => 20,  54 => 15,  49 => 13,  44 => 11,  39 => 9,  34 => 7,  29 => 5,  23 => 1,  31 => 4,  28 => 3,);
    }
}
