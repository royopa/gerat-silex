<?php

/* navbar.html.twig */
class __TwigTemplate_79a126a209cc2224b755db4906bede937364a83cdf6c848744fcbbbed3e69ad8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "        <!-- Fixed navbar -->
        <div class=\"navbar navbar-inverse navbar-fixed-top\">
            <div class=\"container\">
              <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                  <span class=\"icon-bar\"></span>
                  <span class=\"icon-bar\"></span>
                  <span class=\"icon-bar\"></span>
                </button>
                <img id=\"logo\" height=\"40\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "basepath"), "html", null, true);
        echo "/img/logo_caixa.png\">
              </div>
              <div class=\"navbar-collapse collapse\">
                <ul class=\"nav navbar-nav\">
                  <li><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\" class=\"glyphicon glyphicon-home\"></a></li>
                    <li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("fundos");
        echo "\" >Fundos</a></li>

                  <li class=\"dropdown\">
                    <a href='' class=\"dropdown-toggle\" data-toggle=\"dropdown\">Administração <b class=\"caret\"></b></a>
                    <ul class=\"dropdown-menu\">
                      <li><a href=\"#\">
                      Consulta usuários</a>
                      </li>
                    </ul>
                  </li>

                </ul>
                ";
        // line 27
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "matricula", array(), "any", true, true)) {
            // line 28
            echo "                <ul class=\"nav navbar-nav navbar-right\">
                    <li>
                        <a href=\"#\" title=\"Usuário: ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "matricula"), "html", null, true);
            echo "\" class=\"glyphicon glyphicon-user\"></a>
                    </li>
                </ul>
                ";
        } else {
            // line 34
            echo "                <ul class=\"nav navbar-nav navbar-right\">
                    <li>
                        <a href=\"";
            // line 36
            echo $this->env->getExtension('routing')->getPath("login");
            echo "\" title=\"Login\">
                        <i class=\"fa fa-unlock-alt\"></i>
                        </a>
                    </li>
                </ul>
                ";
        }
        // line 42
        echo "
                <ul class=\"nav navbar-nav\">
                  <li><a href=\"";
        // line 44
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\" class=\"glyphicon glyphicon-home\"></a></li>
                  ";
        // line 45
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 46
            echo "                  <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("fundos");
            echo "\">Fundos</a></li>

                  <li class=\"dropdown\">
                    <a href='' class=\"dropdown-toggle\" data-toggle=\"dropdown\">Risco de Liquidez <b class=\"caret\"></b></a>
                    <ul class=\"dropdown-menu\">
                      <li><a href=\"";
            // line 51
            echo $this->env->getExtension('routing')->getPath("consulta_liquidez");
            echo "\">Consulta valor de liquidez diário</a></li>
                    </ul>
                  </li>
                  <li class=\"dropdown\">
                    <a href='' class=\"dropdown-toggle\" data-toggle=\"dropdown\">Rotina Diária <b class=\"caret\"></b></a>
                    <ul class=\"dropdown-menu\">
                      <li><a href=\"";
            // line 57
            echo $this->env->getExtension('routing')->getPath("consulta_xml_nao_processados");
            echo "\">Consulta XML não processados</a></li>
                      <li><a href=\"";
            // line 58
            echo $this->env->getExtension('routing')->getPath("consulta_diferencas_xml");
            echo "\">Consulta diferenças XML</a></li>
                      <li><a href=\"";
            // line 59
            echo $this->env->getExtension('routing')->getPath("consulta_benchmark");
            echo "\">Consulta benchmark</a></li>
                      <li><a href=\"http://10.4.45.99/gerat-sf/web/app_dev.php/consulta_pastas_xml\">Consulta XML faltando Bradesco</a></li>
                      <li><a href=\"http://10.4.45.99/gerat-sf/web/app_dev.php/renomear_xml_santander\">Renomear XMLs Santander</a></li>
                      <li><a href=\"http://10.4.45.99/gerat-sf/web/app_dev.php/processar_xml_itau\">Processar XMLs Itaú</a></li>
                      <li><a href=\"";
            // line 63
            echo $this->env->getExtension('routing')->getPath("simular_dpge");
            echo "\">Simular DPGE</a></li>
                      <li><a href=\"";
            // line 64
            echo $this->env->getExtension('routing')->getPath("consulta_monitoramento");
            echo "\">Monitoramento CR 245</a></li>
                    </ul>
                  </li>
                  ";
        }
        // line 68
        echo "
                  ";
        // line 69
        if ($this->env->getExtension('security')->isGranted("ROLE_USER")) {
            // line 70
            echo "                  <li class=\"dropdown\">
                    <a class=\"dropdown-toggle\" data-toggle=\"dropdown\">
                    Relatórios <b class=\"caret\"></b></a>
                    <ul class=\"dropdown-menu\">
                      <!-- Relatório atribuição de performance mensal -->
                      <li>
                        <a href=\"";
            // line 76
            echo $this->env->getExtension('routing')->getPath("relatorio_atribuicao_performance");
            echo "\">Relatório - Mensal
                        </a>
                      </li>
                    </ul>
                  </li>
                  ";
        }
        // line 82
        echo "
                ";
        // line 83
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 84
            echo "                  <li class=\"dropdown\">
                    <a href='' class=\"dropdown-toggle\" data-toggle=\"dropdown\">Administração <b class=\"caret\"></b></a>
                    <ul class=\"dropdown-menu\">
                      <li><a href=\"";
            // line 87
            echo $this->env->getExtension('routing')->getPath("lista_usuarios");
            echo "\">
                      Consulta usuários</a>
                      </li>
                      <li><a href=\"";
            // line 90
            echo $this->env->getExtension('routing')->getPath("lista_ativos_mercado");
            echo "\">
                      Lista Ativos Risco Mercado</a>
                      </li>
                      <li><a href=\"";
            // line 93
            echo $this->env->getExtension('routing')->getPath("lista_grupos_ativos");
            echo "\">
                      Lista Grupos de Ativos</a>
                      </li>
                      <li><a href=\"";
            // line 96
            echo $this->env->getExtension('routing')->getPath("consulta_ativos_por_emissor");
            echo "\">
                      Consulta ativos alocados por emissor</a>
                      </li>
                      <li><a href=\"";
            // line 99
            echo $this->env->getExtension('routing')->getPath("consulta_rating");
            echo "\">
                      Consulta rating por emissor</a>
                      </li>
                    </ul>
                  </li>
                ";
        }
        // line 105
        echo "
                </ul>



              </div><!--/.nav-collapse -->
            </div>
        </div>


";
    }

    public function getTemplateName()
    {
        return "navbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 105,  192 => 99,  186 => 96,  180 => 93,  174 => 90,  168 => 87,  163 => 84,  161 => 83,  158 => 82,  141 => 70,  136 => 68,  129 => 64,  114 => 58,  110 => 57,  101 => 51,  92 => 46,  90 => 45,  86 => 44,  82 => 42,  73 => 36,  69 => 34,  58 => 28,  56 => 27,  41 => 15,  37 => 14,  30 => 10,  19 => 1,  257 => 106,  254 => 105,  249 => 55,  245 => 34,  242 => 33,  239 => 32,  235 => 26,  231 => 24,  225 => 22,  222 => 21,  219 => 20,  213 => 112,  211 => 105,  179 => 76,  169 => 69,  164 => 67,  159 => 65,  154 => 63,  149 => 76,  144 => 59,  139 => 69,  137 => 55,  134 => 54,  125 => 63,  122 => 50,  118 => 59,  115 => 48,  106 => 45,  103 => 44,  99 => 43,  96 => 42,  87 => 39,  84 => 38,  80 => 37,  76 => 35,  74 => 32,  68 => 29,  64 => 27,  62 => 30,  54 => 15,  49 => 13,  44 => 11,  39 => 9,  34 => 7,  29 => 5,  23 => 1,  31 => 4,  28 => 3,);
    }
}
