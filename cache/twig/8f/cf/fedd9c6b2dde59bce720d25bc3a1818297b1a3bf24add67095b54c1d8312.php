<?php

/* navbar.html.twig */
class __TwigTemplate_8fcffedd9c6b2dde59bce720d25bc3a1818297b1a3bf24add67095b54c1d8312 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "        <!-- Fixed navbar -->
        <div class=\"navbar navbar-default navbar-fixed-top\">
            <div class=\"container\">
              <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                  <span class=\"icon-bar\"></span>
                  <span class=\"icon-bar\"></span>
                  <span class=\"icon-bar\"></span>
                </button>
                <img id=\"logo\" height=\"40\" src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "basepath"), "html", null, true);
        echo "/img/logo_caixa.png\">
              </div>
              <div class=\"navbar-collapse collapse\">
                <ul class=\"nav navbar-nav\">
                  <li><a href=\"#\" class=\"glyphicon glyphicon-home\"></a></li>
                    <li><a href=\"#\" >Cadastro</a></li>

                  <li class=\"dropdown\">
                    <a href='' class=\"dropdown-toggle\" data-toggle=\"dropdown\">Administração <b class=\"caret\"></b></a>
                    <ul class=\"dropdown-menu\">
                      <li><a href=\"#\">
                      Consulta usuários</a>
                      </li>
                    </ul>
                  </li>

                </ul>

                <ul class=\"nav navbar-nav navbar-right\">
                    <li>
                      <a href=\"#\"
                        class=\"glyphicon glyphicon-off\" title=\"Logout\">
                      </a>
                    </li>
                    <li>
                        <a href=\"#\" title=\"Clique aqui para se cadastrar\" class=\"glyphicon glyphicon-user\"></a>
                    </li>
                </ul>

              </div><!--/.nav-collapse -->
            </div>
        </div>";
    }

    public function getTemplateName()
    {
        return "navbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 10,  19 => 1,  123 => 54,  119 => 51,  116 => 50,  113 => 49,  109 => 46,  105 => 44,  99 => 42,  96 => 41,  93 => 40,  86 => 55,  84 => 54,  80 => 52,  78 => 49,  74 => 47,  72 => 40,  57 => 28,  52 => 26,  27 => 4,  22 => 1,  31 => 4,  28 => 3,);
    }
}
