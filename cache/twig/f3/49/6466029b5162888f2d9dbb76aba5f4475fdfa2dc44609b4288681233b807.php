<?php

/* 500.html */
class __TwigTemplate_f3496466029b5162888f2d9dbb76aba5f4475fdfa2dc44609b4288681233b807 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
    <div class=\"jumbotron\">
        <h1><span class=\"glyphicon glyphicon-fire red\"></span> 500 Internal Server Error</h1>
        <p class=\"lead\">The web server is returning an internal error.</p>
        <a href=\"javascript:document.location.reload(true);\" class=\"btn btn-default btn-lg text-center\"><span class=\"green\">Try This Page Again</span></a>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "500.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 4,  28 => 3,);
    }
}
