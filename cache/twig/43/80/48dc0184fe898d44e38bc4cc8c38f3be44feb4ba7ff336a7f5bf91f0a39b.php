<?php

/* base.html.twig */
class __TwigTemplate_438048dc0184fe898d44e38bc4cc8c38f3be44feb4ba7ff336a7f5bf91f0a39b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'navbar' => array($this, 'block_navbar'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"pt-br\" lang=\"pt-br\" >
    <head>
        <link rel=\"icon\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "basepath"), "html", null, true);
        echo "/img/favicon.ico\" type=\"image/x-icon\">

        <link rel=\"stylesheet\" href=\"../components/jquery-ui/themes/ui-lightness/jquery-ui.min.css\" />
        <script src=\"../components/jquery/jquery.js\"></script>
        <script src=\"../components/jquery-ui/jquery-ui-built.js\"></script>

        <!-- Latest compiled and minified CSS -->
        <link rel=\"stylesheet\" href=\"../vendor/twbs/bootstrap/dist/css/bootstrap.min.css\">

        <!-- Optional theme -->
        <link rel=\"stylesheet\" href=\"http://bootswatch.com/spacelab/bootstrap.min.css\">

        <!-- Optional theme -->
        <!--<link rel=\"stylesheet\" href=\"../vendor/twbs/bootstrap/dist/css/bootstrap-theme.min.css\">-->

        <!-- Latest compiled and minified JavaScript -->
        <script src=\"../vendor/twbs/bootstrap/dist/js/bootstrap.min.js\"></script>

        <link rel=\"stylesheet\" type=\"text/css\" href=\"../vendor/datatables/datatables/media/css/jquery.dataTables.css\">

        <script type=\"text/javascript\" language=\"javascript\" src=\"../vendor/datatables/datatables/media/js/jquery.dataTables.js\"></script>

        <link href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "basepath"), "html", null, true);
        echo "/css/main.css\" rel=\"stylesheet\" type=\"text/css\" />

        <script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "basepath"), "html", null, true);
        echo "/js/main.js\"></script>

        <script type=\"text/javascript\" charset=\"utf-8\">
        \$( document ).ready(function() {
            alert('jquery rodando!');
        });
        </script>

        <meta charset=\"utf-8\">
        <!--[if lt IE 9]>
        <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js') }}\"></script>
        <![endif]-->
        ";
        // line 40
        $this->displayBlock('title', $context, $blocks);
        // line 47
        echo "    </head>
    <body>
        ";
        // line 49
        $this->displayBlock('navbar', $context, $blocks);
        // line 52
        echo "
        <div class=\"container theme-showcase\" id=\"container-theme-showcase\">
        ";
        // line 54
        $this->displayBlock('content', $context, $blocks);
        // line 55
        echo "        </div><!-- /container -->
    </body>
</html>
";
    }

    // line 40
    public function block_title($context, array $blocks = array())
    {
        // line 41
        echo "            ";
        if (array_key_exists("title", $context)) {
            // line 42
            echo "                <title>";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</title>
            ";
        } else {
            // line 44
            echo "                <title>My Silex Application</title>
            ";
        }
        // line 46
        echo "        ";
    }

    // line 49
    public function block_navbar($context, array $blocks = array())
    {
        // line 50
        echo "        ";
        $this->env->loadTemplate("base.html.twig", "892469664")->display($context);
        // line 51
        echo "        ";
    }

    // line 54
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 54,  119 => 51,  116 => 50,  113 => 49,  109 => 46,  105 => 44,  99 => 42,  96 => 41,  93 => 40,  86 => 55,  84 => 54,  80 => 52,  78 => 49,  74 => 47,  72 => 40,  57 => 28,  52 => 26,  27 => 4,  22 => 1,  31 => 4,  28 => 3,);
    }
}


/* base.html.twig */
class __TwigTemplate_438048dc0184fe898d44e38bc4cc8c38f3be44feb4ba7ff336a7f5bf91f0a39b_892469664 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("navbar.html.twig");

        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "navbar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 54,  119 => 51,  116 => 50,  113 => 49,  109 => 46,  105 => 44,  99 => 42,  96 => 41,  93 => 40,  86 => 55,  84 => 54,  80 => 52,  78 => 49,  74 => 47,  72 => 40,  57 => 28,  52 => 26,  27 => 4,  22 => 1,  31 => 4,  28 => 3,);
    }
}
