<?php

/* index.html.twig */
class __TwigTemplate_7bd56603a450f40df9f029275215b6d1ec575e8f44d2fbb40fdbb9fd1da79770 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'link_relatorio' => array($this, 'block_link_relatorio'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        if ((twig_length_filter($this->env, (isset($context["listaTopMais"]) ? $context["listaTopMais"] : $this->getContext($context, "listaTopMais"))) > 0)) {
            // line 6
            echo "<div class=\"row\">
  <div class=\"col-md-12\">
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <i class=\"fa fa-bar-chart-o\"></i>
        <h3 class=\"panel-title\">Top 10</h3>
      </div>
      <div class=\"panel-body\">
        <div id=\"grafico_top_mais\"></div>
        <script type=\"text/javascript\">
          ";
            // line 20
            echo "        </script>
      </div>
    </div>
  </div>
</div>
";
        }
        // line 26
        echo "
<div class=\"row\">
  <div class=\"col-md-12\">
    <div class=\"panel-group\" id=\"accordion\">
      <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
          <i class=\"fa fa-bar-chart-o\"></i>
          <h4 class=\"panel-title\">
            <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">
              Relatório de atribuição de performance
            </a>
          </h4>
        </div>
        <div id=\"collapseOne\" class=\"panel-collapse collapse\">
          <div class=\"panel-body\">
            <table id=\"table_export\" >
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Rentabilidade em ";
        // line 45
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["dataReferencia"]) ? $context["dataReferencia"] : $this->getContext($context, "dataReferencia")), "m/Y"), "html", null, true);
        echo "</th>
                  <th>CNPJ</th>
                </tr>
                </thead>
              <tbody>
                ";
        // line 50
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["produtos"]) ? $context["produtos"] : $this->getContext($context, "produtos")));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["produto"]) {
            // line 51
            echo "                <tr>
                    <td>
                    ";
            // line 53
            $this->displayBlock('link_relatorio', $context, $blocks);
            // line 64
            echo "                    </td>
                    <td
                    ";
            // line 66
            if (($this->getAttribute((isset($context["produto"]) ? $context["produto"] : $this->getContext($context, "produto")), "RENT_MES") < 0)) {
                // line 67
                echo "                      class=\"valor-negativo\"
                    ";
            } else {
                // line 69
                echo "                      class=\"valor-positivo\"
                    ";
            }
            // line 71
            echo "                    >
                      ";
            // line 72
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["produto"]) ? $context["produto"] : $this->getContext($context, "produto")), "RENT_MES"), 6, ",", "."), "html", null, true);
            echo "%
                    </td>
                    <td><small>";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produto"]) ? $context["produto"] : $this->getContext($context, "produto")), "CO_PRD"), "html", null, true);
            echo "</small></td>
                </tr>
                ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 77
            echo "                    <p> Nenhum fundo foi encontrado. </p>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['produto'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "                <?php \$i++; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

";
        // line 89
        if (array_key_exists("user", $context)) {
            // line 90
            echo "<div class=\"row\">
  <div class=\"col-md-12\">
  ";
            // line 92
            echo twig_var_dump($this->env, $context, (isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")));
            echo "
  </div>
</div>
";
        }
        // line 96
        echo "
";
    }

    // line 53
    public function block_link_relatorio($context, array $blocks = array())
    {
        // line 54
        echo "                    <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("relatorio", array("id_relatorio" => 3, "co_prd" => $this->getAttribute((isset($context["produto"]) ? $context["produto"] : $this->getContext($context, "produto")), "CO_PRD"), "dt_ini" => (isset($context["dataInicio"]) ? $context["dataInicio"] : $this->getContext($context, "dataInicio")), "dt_fim" => (isset($context["dataFim"]) ? $context["dataFim"] : $this->getContext($context, "dataFim")), "dt_ref" => (isset($context["dataReferencia"]) ? $context["dataReferencia"] : $this->getContext($context, "dataReferencia")), "dt_atu" => (isset($context["dataAtualizacao"]) ? $context["dataAtualizacao"] : $this->getContext($context, "dataAtualizacao")))), "html", null, true);
        // line 61
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["produto"]) ? $context["produto"] : $this->getContext($context, "produto")), "NO_PRD"), "html", null, true);
        echo "
                    </a>
                    ";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 61,  188 => 54,  185 => 53,  180 => 96,  173 => 92,  169 => 90,  167 => 89,  155 => 79,  148 => 77,  132 => 74,  127 => 72,  124 => 71,  120 => 69,  116 => 67,  114 => 66,  110 => 64,  108 => 53,  104 => 51,  86 => 50,  78 => 45,  57 => 26,  49 => 20,  37 => 6,  35 => 5,  32 => 4,  29 => 3,);
    }
}
